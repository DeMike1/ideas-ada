//package testing.other.wsag4j;
//
//import org.junit.Test;
//
//import es.us.isa.ada.ADA;
//import es.us.isa.ada.choco.utils.ADAChocoFacade;
//import es.us.isa.ada.document.AbstractDocument;
//import es.us.isa.ada.operations.ConsistencyOperation;
//
//public class Wsag4jBasicTests {
//
//	@Test
//	public void test1(){
//		ADA ada = new ADAChocoFacade();
//		AbstractDocument doc = ada.loadDocument("Documentos WSAG4J/test-template.xml");
//		ConsistencyOperation op = 
//				(ConsistencyOperation) ada.createOperation(ADA.CONSISTENCY);
//		op.addDocument(doc);
//		ada.analyze(op);
//		boolean isConsistent = op.isConsistent();
//		System.out.println("Is consistent? "+isConsistent);
//	}
//	
//}
