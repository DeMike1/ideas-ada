package testing.choco;

import org.junit.Before;
import org.junit.Test;

import testing.facade.ADATestingFacade;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ComplianceOperation;


public class ChocoComplianceTests {

//	private AbstractDocument doc;
	
	private ADATestingFacade ada;
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}
	
	@Test
	public void test1(){
		AbstractDocument doc = ada.loadDocument("resources/Figure 1a - (Template).wsag");
		ComplianceOperation op = (ComplianceOperation) ada.createOperation("compliance");;
		op.addDocument(doc);
		ada.analyze(op);
		boolean compliant = op.isCompliant();
		System.out.println("Is the offer compliant?: "+compliant);
		assert (compliant == false);
	}
	
	@Test
	public void test2(){
		AbstractDocument doc1 = ada.loadDocument("resources/Figure 1a - (Template).wsag");
		AbstractDocument doc2 = ada.loadDocument("resources/Figure 1c - (Compliant Agreement Offer).wsag");
		ComplianceOperation op = (ComplianceOperation) ada.createOperation("compliance");;
		op.addDocument(doc1);
		op.addDocument(doc2);
		ada.analyze(op);
		boolean compliant = op.isCompliant();
		System.out.println("Is the offer compliant?: "+compliant);
		assert (compliant == true);
	}
	
	@Test
	public void test3(){
		AbstractDocument doc1 = ada.loadDocument("resources/Figure 1a - (Template).wsag");		
		AbstractDocument doc2 = ada.loadDocument("resources/Figure 1d - (Non-Compliant Agreement Offer).wsag");
		ComplianceOperation op = (ComplianceOperation) ada.createOperation("compliance");
		op.addDocument(doc1);
		op.addDocument(doc2);
		ada.analyze(op);
		boolean compliant = op.isCompliant();
		System.out.println("Is the offer compliant?: "+compliant);
		assert (compliant == false);
	}
	
}
