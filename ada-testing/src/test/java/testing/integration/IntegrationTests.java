//package testing.integration;
//
//import java.util.Collection;
//
//import org.junit.Test;
//
//import es.us.isa.ada.ADA;
//import es.us.isa.ada.choco.utils.ADAChocoFacade;
//import es.us.isa.ada.document.AbstractDocument;
//import es.us.isa.ada.operations.AlternativeDocumentsOperation;
//
//public class IntegrationTests {
//
//	@Test
//	public void test1(){
//		ADA ada = new ADAChocoFacade();
//		AbstractDocument doc = ada.loadDocument("resources/Figure 1a - (Template).wsag");
//		AlternativeDocumentsOperation op = 
//				(AlternativeDocumentsOperation) ada.createOperation("alternativeDocuments");
//		op.addDocument(doc);
//		ada.analyze(op);
//		Collection<AbstractDocument> res = op.getAlternativeDocuments();
//		System.out.println(op.getNumberOfDocuments());
//	}
//	
//}
