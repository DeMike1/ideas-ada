package testing.mmd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import es.us.isa.ada.salmon.MonitoringManagementDocument;

public class ParseMMDTest {
	
	String filePath = "mmd/MonitoringManagementDocument.wsag";
	
	@Test
	public void test(){
		String doc = readFileAsString(filePath);
		MonitoringManagementDocument mmd = new MonitoringManagementDocument(doc);
		System.out.println(mmd.toString());
	}
	
	private String readFileAsString(String filePath){
		String result = null;
		try {
			FileInputStream file = new FileInputStream(filePath);
			byte[] b = new byte[file.available ()];
			file.read(b);
			file.close ();
			result = new String (b);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return result;
	}
}
