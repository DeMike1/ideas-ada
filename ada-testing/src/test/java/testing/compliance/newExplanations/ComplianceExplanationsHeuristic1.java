package testing.compliance.newExplanations;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Test;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.choco.ChocoAnalyzer;
import es.us.isa.ada.choco.questions.ChocoQuickxplainNoComplianceOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.ConsistencyOperation;

public class ComplianceExplanationsHeuristic1 {

	
	@Test
	public void test1(){
		ADATestingFacade ada = new ADATestingFacade();
		
		AbstractDocument template = ada.loadDocument("explaining-non-compliance/sample-group-1/sample1/TEMPLATE.wsag");
		AbstractDocument offer = ada.loadDocument("explaining-non-compliance/sample-group-1/sample1/OFFER.wsag");
		
		ConsistencyOperation cons = (ConsistencyOperation) ada.createOperation("consistency");
		cons.addDocument(template);
		ada.analyze(cons);
		
		if (cons.isConsistent()){
			ChocoQuickxplainNoComplianceOp op = new ChocoQuickxplainNoComplianceOp();
			op.addDocument(template);
			op.addDocument(offer);
			
			op.execute(new ChocoAnalyzer());
//			Map<Collection<AgreementElement>,Collection<AgreementElement>> res = op.explainErrors();
//			
//			Set<Entry<Collection<AgreementElement>,Collection<AgreementElement>>> entries = res.entrySet();
//			for (Entry<Collection<AgreementElement>,Collection<AgreementElement>> e:entries){
//				Collection<AgreementElement> offerElems = e.getKey();
//				Collection<AgreementElement> templateElems = e.getValue();
//				for (AgreementElement ae:offerElems){
//					System.out.print(ae.getName()+", ");
//				}
//				System.out.println();
//				System.out.println("=>");
//				for (AgreementElement ae:templateElems){
//					System.out.print(ae.getName()+", ");
//				}
//				System.out.println();
//				System.out.println("=================");
//			}
			
		}
		else {
			System.out.println("La plantilla no es consistente");
		}
		
	}
	
	@Test
	public void test2(){
		ADATestingFacade ada = new ADATestingFacade();
		
		AbstractDocument template = ada.loadDocument("explaining-non-compliance/sample-group-1/sample2/TEMPLATE.wsag");
		AbstractDocument offer = ada.loadDocument("explaining-non-compliance/sample-group-1/sample2/OFFER.wsag");
		
		ConsistencyOperation cons = (ConsistencyOperation) ada.createOperation("consistency");
		cons.addDocument(template);
		ada.analyze(cons);
		
		if (cons.isConsistent()){
			ChocoQuickxplainNoComplianceOp op = new ChocoQuickxplainNoComplianceOp();
			op.addDocument(template);
			op.addDocument(offer);
			
			op.execute(new ChocoAnalyzer());
//			Map<Collection<AgreementElement>,Collection<AgreementElement>> res = op.explainErrors();
//			
//			Set<Entry<Collection<AgreementElement>,Collection<AgreementElement>>> entries = res.entrySet();
//			for (Entry<Collection<AgreementElement>,Collection<AgreementElement>> e:entries){
//				Collection<AgreementElement> offerElems = e.getKey();
//				Collection<AgreementElement> templateElems = e.getValue();
//				for (AgreementElement ae:offerElems){
//					System.out.print(ae.getName()+", ");
//				}
//				System.out.println();
//				System.out.println("=>");
//				for (AgreementElement ae:templateElems){
//					System.out.print(ae.getName()+", ");
//				}
//				System.out.println();
//				System.out.println("=================");
//			}
			
		}
		else {
			System.out.println("La plantilla no es consistente");
		}
		
	}
	
	@Test
	public void test3(){		
		doExplaining("explaining-non-compliance/sample-group-1");
	}
	
	@Test
	public void test4(){		
		doExplaining("explaining-non-compliance/sample-group-2");
	}

	private void doExplaining(String s) {
		ADATestingFacade ada = new ADATestingFacade();
		File directory = new File(s);
		File[] subdirs = directory.listFiles();
		int index = 1;
		
		for (File f:subdirs){
			if (f.isDirectory() && !f.getName().endsWith(".svn")){
				//each one of subdirs
				System.out.println("Case "+index++);
				String offerName = f.getPath()+"\\OFFER.wsag";
				String templateName = f.getPath()+"\\TEMPLATE.wsag";
				
				AbstractDocument template = ada.loadDocument(templateName);
				AbstractDocument offer = ada.loadDocument(offerName);
				
				ConsistencyOperation cons = (ConsistencyOperation) ada.createOperation("consistency");
				cons.addDocument(template);
				ada.analyze(cons);
				
				if (cons.isConsistent()){
					ChocoQuickxplainNoComplianceOp op = new ChocoQuickxplainNoComplianceOp();
					op.addDocument(template);
					op.addDocument(offer);
					op.setExplanationLevel(ChocoQuickxplainNoComplianceOp.REFINE_ALL);
					
					op.execute(new ChocoAnalyzer());
					Map<AgreementError,Explanation> res = op.explainErrors();
					Set<Entry<AgreementError,Explanation>> entries = res.entrySet();
					for (Entry<AgreementError,Explanation> entry:entries){
						System.out.print(entry.getKey() + " => " + entry.getValue());
						System.out.println();
					}
					System.out.println();
				}
				else {
					System.out.println("La plantilla no es consistente");
				}
			}
		}
	}
	
//	@Test
//	public void test3(){
//		ADATestingFacade ada = new ADATestingFacade();
//		
//		AbstractDocument template = ada.loadDocument("explaining-non-compliance/TEMPLATE.wsag");
//		AbstractDocument offer = ada.loadDocument("explaining-non-compliance/OFFER.wsag");
//		
//		ConsistencyOperation cons = (ConsistencyOperation) ada.createOperation("consistency");
//		cons.addDocument(template);
//		ada.analyze(cons);
//		
//		if (cons.isConsistent()){
//			ChocoComplianceOp op = new ChocoComplianceOp();
////			ChocoExplainNoComplianceOp op = new ChocoExplainNoComplianceOp();
//			op.addDocument(template);
//			op.addDocument(offer);
//			
//			op.execute(new ChocoAnalyzer());
//			boolean b = op.isCompliant();
//			if (b){
//				System.out.println("Compliant");
//			}
//			else{
//				System.out.println("NON Compliant");
//			}
//			assert (b);
//		}
//	}
	
}
