package testing.transformations;

import es.us.isa.ada.ADA;
import es.us.isa.ada.choco.ProxyChocoAnalyzer;
import es.us.isa.ada.loaders.BasicExtensionsLoader;
import es.us.isa.ada.repository.ADAAgreementRepository;
import es.us.isa.ada.subfacades.ProxyAnalyzer;
import es.us.isa.ada.users.ADASessionManager;

import java.util.Arrays;

public class OfferTests {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        BasicExtensionsLoader loader = new BasicExtensionsLoader();
        loader.setProxyAnalyzers(Arrays.asList((ProxyAnalyzer)new ProxyChocoAnalyzer()));
        loader.setAgreementRepository(new ADAAgreementRepository());
        loader.setSessionManager(new ADASessionManager());
        ADA ada = new ADA(loader);
        ada.transformTo("wsag/undeclaredSDTVar.wsag", "wsag4people/offer.wsag4people");
        ada.transformTo("wsag4people/offer.wsag4people", "recoveredwsag/offer.wsag");
    }

}
