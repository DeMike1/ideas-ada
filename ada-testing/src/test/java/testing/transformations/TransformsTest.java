package testing.transformations;

import es.us.isa.ada.ADA;
import es.us.isa.ada.choco.ProxyChocoAnalyzer;
import es.us.isa.ada.loaders.BasicExtensionsLoader;
import es.us.isa.ada.repository.ADAAgreementRepository;
import es.us.isa.ada.subfacades.ProxyAnalyzer;
import es.us.isa.ada.users.ADASessionManager;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;


public class TransformsTest {

	private ADA ada;
	
	@Before
	public void setUp(){
        BasicExtensionsLoader loader = new BasicExtensionsLoader();
        loader.setProxyAnalyzers(Arrays.asList((ProxyAnalyzer) new ProxyChocoAnalyzer()));
        loader.setAgreementRepository(new ADAAgreementRepository());
        loader.setSessionManager(new ADASessionManager());
        ada = new ADA(loader);
	}

	
	@Test
	public void testwsag2wsag4people(){
		
		String inputDir = "wsag";
		String outputDir = "wsag4people";
		
		File dir1 = new File(inputDir);
		File[] subfiles1 = dir1.listFiles();
		for (File f:subfiles1){
			if (f.getName().endsWith("wsag")){
				String path1 = f.getPath();
				String name = f.getName();
				int index = name.indexOf(".");
				name = name.substring(0,index);
				String target = outputDir+File.separator+name+".wsag4people";
				ada.transformTo(path1,target);
				File aux = new File(target);
				assert aux.exists();
			}
		}
		
	}
	
	
	@Test
	public void testwsag4people2wsag(){
		
		String inputDir = "wsag4people";
		String outputDir = "recoveredwsag";
		
		File dir1 = new File(inputDir);
		File[] subfiles1 = dir1.listFiles();
		for (File f:subfiles1){
			if (f.getName().endsWith("wsag4people")){
				String path1 = f.getPath();
				String name = f.getName();
				int index = name.indexOf(".");
				name = name.substring(0,index);
				String target = outputDir+File.separator+name+".wsag";
				ada.transformTo(path1,target);
				File aux = new File(target);
				assert aux.exists();
			}
		}
		
	}

	private String readStringFromFile(String file){
		String text = "";
		InputStream is;
		try {
			is = new FileInputStream(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String s;
			while ( (s = reader.readLine()) != null ){
				text += s + "\n";
			}
//			System.out.println(text);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return text;
	}
	
}
