package testing.scopes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.DecomposeIntoViewsOperation;
import es.us.isa.ada.wsag10.AbstractAgreementDocument;
import es.us.isa.ada.wsag10.ServiceProperties;
import es.us.isa.ada.wsag10.ServiceScope;
import es.us.isa.ada.wsag10.Term;
import es.us.isa.ada.wsag10.Variable;


@RunWith(Parameterized.class)
public class ScopesTesting {

	// private AgreementStateLoader stateLoader;
	private ADATestingFacade ada;

	private String path;
	private int result;

	public ScopesTesting(String path, int result) {
		this.path = path;
		this.result = result;
	}

	@Before
	public void setUp() {
		// stateLoader = new AgreementStateLoader();
		ada = new ADATestingFacade();
	}

	// metodo del test
	@Test
	public void test() {
		runTest(path, result);
	}

	// en este metodo obtenemos todos los archivos
	// sobre los que vamos a ejecutar los tests
	@Parameters
	public static Collection<Object[]> getParameters() {
		Collection<Object[]> res = new LinkedList<Object[]>();

		Properties props = new Properties();
		try {
			props.load(new FileInputStream(
					"scopeTests/offers/expectedResults.properties"));
			File f = new File("scopeTests/offers");
			File[] subfiles = f.listFiles();
			for (int i = 0; i < subfiles.length; i++) {
				if (subfiles[i].isDirectory()){
					String path = subfiles[i].getPath();
					if (!path.endsWith(".svn")) {
						String testName = subfiles[i].getName();
						int expected = Integer.parseInt(props.getProperty(testName));
						Object[] aux = { path, expected };
						res.add(aux);
					}
				}
				
			}

			props.load(new FileInputStream(
					"scopeTests/templates/expectedResults.properties"));
			f = new File("scopeTests/templates");
			subfiles = f.listFiles();
			for (int i = 0; i < subfiles.length; i++) {
				if (subfiles[i].isDirectory()){
					String path = subfiles[i].getPath();
					if (!path.endsWith(".svn")) {
						String testName = subfiles[i].getName();
						int expected = Integer.parseInt(props.getProperty(testName));
						Object[] aux = { path, expected };
						res.add(aux);
					}
				}
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}

	// cuerpo del test
	private void runTest(String path, int result) {
		AbstractDocument doc = ada.loadDocument(path + "/document.wsag");
		DecomposeIntoViewsOperation op = (DecomposeIntoViewsOperation) ada
				.createOperation("OpViews");
		op.addDocument(doc);
		ada.analyze(op);
//		Collection<AbstractDocument> res = op.getOperationViews();
		Set<Entry<ServiceScope, AbstractDocument>> res = op.getScopes2Views().entrySet();
		int opRes = res.size();
		System.out.println(path+": ");
		for (Entry<ServiceScope, AbstractDocument> d:res){
			AbstractAgreementDocument aux = (AbstractAgreementDocument) d.getValue();
			Collection<Term> terms = aux.getAllTerms();
			System.out.print(d.getKey().getServiceName()+": ");
			ServiceProperties props = null;
			for (Term t:terms){
				if (!(t instanceof ServiceProperties)){
					System.out.print(t.getTermName()+", ");
				}
				else{
					props = (ServiceProperties) t;
				}
			}
			Set<Variable> vars = props.getVariableSet();
			System.out.print("Variables: ");
			for (Variable v:vars){
				System.out.print(v.getName()+", ");
			}
			System.out.println();
		}
		System.out.println("=====================");
		
		assert (opRes == result);
	}

}