package testing.consistency;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;

@RunWith(Parameterized.class)
public class ConsistencyTest {

	private String path;
	private Boolean expectedResult;
	
	private ADATestingFacade ada;
	
	public ConsistencyTest(String path, Boolean expectedResult){
		this.path = path;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> param = new LinkedList<Object[]>();
		File f = new File("black-box-tests/02 ExplainInconsistencies (without temp)/3 ErrorGuess/2 Offers/");
		
		File[] subfiles = f.listFiles();
		for(File aux:subfiles){
			if(!aux.getAbsolutePath().endsWith(".svn") && !aux.getAbsolutePath().endsWith(".properties")){
				Object[] doc = new Object[] {aux.getPath(), Boolean.TRUE};
				param.add(doc);
			}
		}
		return param;
	}
	
	@Test
	public void testConsistency(){
		if(path.endsWith("07_All_inconsistencies_RES-FALSE.wsag")){
			AbstractDocument doc = ada.loadDocument(path);
			ConsistencyOperation op = (ConsistencyOperation) ada.createOperation("consistency");
			op.addDocument(doc);
			ada.analyze(op);
			Boolean result = op.isConsistent();
			System.out.println("Result: "+result+" Document: "+path);
			if(!result){
				ExplainNoConsistencyOperation expOp = (ExplainNoConsistencyOperation) ada.createOperation("explainConsistency");
				expOp.addDocument(doc);
				ada.analyze(expOp);
				System.out.println("  => Explanations: "+expOp.explainErrors());
			}
			assertEquals(expectedResult, result);
		}
	}
}
