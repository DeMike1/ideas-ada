package testing.wsag4j;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;

import testing.facade.ADATestingFacade;

public class WSAG4JDocsTest {
	
	public static void main(String[] args){
		ADATestingFacade ada = new ADATestingFacade();
		
		/*
		 * Explain Consistency test
		 */
		AbstractDocument doc1 = ada.loadDocument("wsag4j/templates/test-template.xml");
//		AbstractDocument doc1 = ada.loadDocument("files-to-test-deploy/template_term_several_errors.wsag");
		ExplainNoConsistencyOperation op1 = (ExplainNoConsistencyOperation) ada.createOperation("explainConflicts");
		op1.addDocument(doc1);
		ada.analyze(op1);
		Set<Entry<AgreementElement, Collection<AgreementElement>>> errors = op1.explainErrors().entrySet();
		System.out.println("Errores: "+errors.size());
		for(Entry<AgreementElement, Collection<AgreementElement>> e:errors){
			AgreementElement agElem = e.getKey();
			System.out.println("El elemento "+agElem.getName()+" entra en conflicto con los siguientes:\n");
			Collection<AgreementElement> c = e.getValue();
			for(AgreementElement elem:c){
				System.out.println(elem.getName());
			}
		}
		
		/*
		 * Check Consistency Test
		 */
		AbstractDocument doc2 = ada.loadDocument("wsag4j/templates/test-template.xml");
		ConsistencyOperation op2 = (ConsistencyOperation) ada.createOperation("consistency");;
		op2.addDocument(doc2);
		ada.analyze(op2);
		boolean consistent = op2.isConsistent();
		System.out.println("Is the doc consistent? "+consistent);
	}
}
