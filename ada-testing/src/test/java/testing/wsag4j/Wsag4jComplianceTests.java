package testing.wsag4j;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.ADA;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.errors.AgreementError;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ExplainNonComplianceOperation;

@RunWith(Parameterized.class)
public class Wsag4jComplianceTests {
	
private ADATestingFacade ada;
	
	private String templatePath;
	private String offerPath;
	private boolean res;
	
	private static final String templatesPath = "wsag4j/templates/";
	private static final String offersPath = "wsag4j/offers/";
	
	public Wsag4jComplianceTests(String template, String offer, boolean res){
		this.templatePath = templatesPath+template;
		this.offerPath = offersPath+offer;
		this.res = res;
	}
	
	@Before
	public void setUp(){
		ada = new TestingFacadeWsag4j();
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
		
		Object[] businessMR = new Object[]{"BusinessMR.xml", "BusinessMR_offer.xml", Boolean.TRUE};
		Object[] validComputeTest = new Object[]{"compute-test.xml", "valid_compute-test-offer.xml", Boolean.TRUE};
		Object[] invalidComputeTest = new Object[]{"compute-test.xml", "invalid_compute-test-offer.xml", Boolean.FALSE};
		Object[] simpleRestriction = new Object[]{"compute-test-simple-restriction.xml", "valid_simple_restriction_offer.xml", Boolean.TRUE};
		Object[] structuralRestriction = new Object[]{"test-structural-restriction.xml", "valid-structural-restriction-offer.xml", Boolean.TRUE};
		
		res.add(businessMR);
		res.add(validComputeTest);
		res.add(invalidComputeTest);
		res.add(simpleRestriction);
		res.add(structuralRestriction);
		return res;
	}
	
	@Test
	public void Compliance(){
		AbstractDocument template = ada.loadDocument(templatePath);
		AbstractDocument offer = ada.loadDocument(offerPath);
		
		ComplianceOperation op = (ComplianceOperation)ada.createOperation("compliance");
		op.addDocument(template);
		op.addDocument(offer);
		ada.analyze(op);
		boolean isCompliant = op.isCompliant();
		System.out.println("Vamos a comprobar "+templatePath+" contra "+offerPath+"\n     Resultado esperado "+res+" resultado obtenido "+isCompliant+"\n");
		if (!isCompliant){
			ExplainNonComplianceOperation expOp = (ExplainNonComplianceOperation) ada.createOperation("explainCompliance");
			expOp.setExplanationLevel(ExplainNonComplianceOperation.REFINE_ALL);
			expOp.addDocument(template);
			expOp.addDocument(offer);
			ada.analyze(expOp);
			Map<AgreementError,Explanation> exps = expOp.explainErrors();
			Set<Entry<AgreementError,Explanation>> entries = exps.entrySet();
			for (Entry<AgreementError,Explanation> entry:entries){
				System.out.println(entry.getKey() + " => " + entry.getValue());
			}
			
		}
		assertEquals(res, isCompliant);
	}
}
