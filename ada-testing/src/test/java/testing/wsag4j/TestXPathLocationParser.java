package testing.wsag4j;

import java.io.File;

import org.apache.xmlbeans.XmlObject;

public class TestXPathLocationParser {
	
	public static void main(String[] args) {
		
		String query = "declare namespace jsdl='http://schemas.ggf.org/jsdl/2005/11/jsdl';declare namespace wsag='http://schemas.ggf.org/graap/2007/03/ws-agreement';$this/wsag:AgreementProperties/wsag:Terms/wsag:All/wsag:ServiceDescriptionTerm[@wsag:Name='Term1']/jsdl:JobDefinition/jsdl:JobDescription/jsdl:Resources/jsdl:IndividualCPUSpeed/jsdl:Exact";
		
		XmlObject doc = null;
		try {
			doc = XmlObject.Factory.parse(new File("wsag4j/states/simple1/state0.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		XmlObject[] obj = doc.execQuery(query);
		System.out.println("Tama�o del array devuelto: "+obj.length);
		for (int i = 0; i < obj.length; i++) {
			System.out.println("Valor: "+obj[i]);
			System.out.println("Ahora: "+getTextContent(obj[i].toString()));
		}
	}
	
	public static String getTextContent(String input){
		String output = input.replaceAll("\\<.*?>", "");
		return output;
	}
}

//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathConstants;
//import javax.xml.xpath.XPathExpression;
//import javax.xml.xpath.XPathExpressionException;
//import javax.xml.xpath.XPathFactory;
//
//import org.w3c.dom.Document;
//
//public class TestXPathLocationParser {
//
//	public static void main(String[] args) {
//		
//		String xpathQuery = "//wsag:AgreementProperties/wsag:Terms/wsag:All/wsag:ServiceDescriptionTerm[@wsag:Name = 'Term1']/jsdl:JobDefinition/jsdl:JobDescription/jsdl:Resources/jsdl:IndividualCPUSpeed/jsdl:Exact";
//		
//		DocumentBuilderFactory factory;
//		DocumentBuilder builder;
//		Document doc = null;
//		
//		/*
//		 * Parsing document
//		 */
//		factory = DocumentBuilderFactory.newInstance();
//		factory.setNamespaceAware(true);
//		try {
//			builder = factory.newDocumentBuilder();
//			doc = builder.parse("wsag4j/states/simple1/state0.xml");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		/*
//		 * XPath
//		 */
//		XPathFactory xpathFactory = XPathFactory.newInstance();
//		XPath xpath = xpathFactory.newXPath();
//		xpath.setNamespaceContext(new ADANamespaceContext());
//		XPathExpression expr = null;
//		try{
//			expr = xpath.compile(xpathQuery);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		/*
//		 * Result
//		 */
//		Object result = null;
//		try {
//			result = expr.evaluate(doc, XPathConstants.NODE);
//		} catch (XPathExpressionException e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("El resultado es: "+result);
//	}
//}
