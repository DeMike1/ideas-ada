package testing.facade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import es.us.isa.ada.Analyzer;
import es.us.isa.ada.Operation;
import es.us.isa.ada.choco.ChocoAnalyzer;
import es.us.isa.ada.choco.questions.ChocoAgreementFulfilmentOp;
import es.us.isa.ada.choco.questions.ChocoComplianceOp;
import es.us.isa.ada.choco.questions.ChocoConsistencyOp;
import es.us.isa.ada.choco.questions.ChocoDeadTermsOp;
import es.us.isa.ada.choco.questions.ChocoDecomposeIntoViewsOp;
import es.us.isa.ada.choco.questions.ChocoExplainComplianceOp;
import es.us.isa.ada.choco.questions.ChocoExplainDeadTermsOp;
import es.us.isa.ada.choco.questions.ChocoExplainLudicrousTermsOp;
import es.us.isa.ada.choco.questions.ChocoExplainNoConsistencyOp;
import es.us.isa.ada.choco.questions.ChocoExplainViolationOp;
import es.us.isa.ada.choco.questions.ChocoLudicrousTermsOp;
import es.us.isa.ada.choco.questions.ChocoNumberOfAlternativeDocsOp;
import es.us.isa.ada.choco.questions.ChocoQuickxplainNoComplianceOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.io.IDocumentParser;
import es.us.isa.ada.wsag10.parsers.DefaultWSAgParser;

public class ADATestingFacade {

	
	protected IDocumentParser parser;
	
	private Map<String,Class<? extends Operation>> mapOps;
	
	private Analyzer analyzer;
	
	
	public ADATestingFacade(){
		parser = new DefaultWSAgParser();
		analyzer = new ChocoAnalyzer();
		loadOps();
	}
	
	
	private void loadOps() {
		mapOps = new HashMap<String, Class<? extends Operation>>();
		mapOps.put("consistency", ChocoConsistencyOp.class);
		mapOps.put("explainConsistency", ChocoExplainNoConsistencyOp.class);
		mapOps.put("compliance", ChocoComplianceOp.class);
		mapOps.put("explainCompliance", ChocoQuickxplainNoComplianceOp.class);
		mapOps.put("slowExplainCompliance", ChocoExplainComplianceOp.class);
		mapOps.put("Fulfilment", ChocoAgreementFulfilmentOp.class);
		mapOps.put("ViolationExps", ChocoExplainViolationOp.class);
		mapOps.put("OpViews", ChocoDecomposeIntoViewsOp.class);
		mapOps.put("deadTerms", ChocoDeadTermsOp.class);
		mapOps.put("explainDeadTerms", ChocoExplainDeadTermsOp.class);
		mapOps.put("ludicrousTerms", ChocoLudicrousTermsOp.class);
		mapOps.put("explainLudicrousTerms", ChocoExplainLudicrousTermsOp.class);
		mapOps.put("numberOfAlternatives", ChocoNumberOfAlternativeDocsOp.class);
//		mapOps.
	}


	public AbstractDocument loadDocument(String path){
		return parser.parseFile(path);
	}
	
	public Operation createOperation(String id){
		Class<? extends Operation> clazz = mapOps.get(id);
		Operation res = null;
		if (clazz != null){
			try {
				res = clazz.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	
	public void analyze(Operation op){
		analyzer.analyze(op);
	}
	
	public Collection<String> getOperationIds(){
		return mapOps.keySet();
	}
	
}
