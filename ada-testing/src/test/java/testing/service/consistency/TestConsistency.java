package testing.service.consistency;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import testing.facade.ADATestingFacade;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;

@RunWith(Parameterized.class)
public class TestConsistency {
	
	private String path;
	private Boolean expectedResult;
	
	private ADATestingFacade ada;
	
	public TestConsistency(String path, Boolean expectedResult){
		this.path = path;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> param = new LinkedList<Object[]>();
		File f = new File("frontendDefaultDocuments\\offers\\");
		
		File[] subfiles = f.listFiles();
		for(File aux:subfiles){
			if(!aux.getAbsolutePath().endsWith(".svn")){
				Object[] doc = new Object[] {aux.getPath(), Boolean.TRUE};
				param.add(doc);
			}
		}
		return param;
	}
	
	@Test
	public void testConsistency(){
		AbstractDocument doc = ada.loadDocument(path);
		ConsistencyOperation op = (ConsistencyOperation) ada.createOperation("consistency");
		op.addDocument(doc);
		ada.analyze(op);
		Boolean result = op.isConsistent();
		System.out.println("Result: "+result+" Document: "+path);
		assertEquals(expectedResult, result);
	}
}
