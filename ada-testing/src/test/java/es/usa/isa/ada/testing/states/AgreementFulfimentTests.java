//package es.usa.isa.ada.testing.states;
//
//import java.io.File;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.Parameterized;
//import org.junit.runners.Parameterized.Parameters;
//
//import testing.facade.ADATestingFacade;
//import es.us.isa.ada.document.AbstractDocument;
//import es.us.isa.ada.errors.AgreementError;
//import es.us.isa.ada.errors.Explanation;
//import es.us.isa.ada.operations.AgreementFulfilmentOperation;
//import es.us.isa.ada.operations.ExplainAgreementViolation;
//import es.us.isa.ada.wsag.values.AgreementState;
//import es.us.isa.ada.wsag10.GeneralServiceScope;
//import es.us.isa.ada.wsag10.ServiceScope;
//
//@RunWith(Parameterized.class)
//public class AgreementFulfimentTests {
//
//	private AgreementStateLoader stateLoader;
//	private ADATestingFacade ada;
//	
//	private String path;
//	private boolean result;
//	
//	public AgreementFulfimentTests(String path, Boolean result){
//		this.path = path;
//		this.result = result;
//	}
//	
//	@Before
//	public void setUp(){
//		stateLoader = new AgreementStateLoader();
//		ada = new ADATestingFacade();
//	}
//	
//	//metodo del test
//	@Test
//	public void test(){
//		runTest(path, result);
//	}
//	
//
//	//en este metodo obtenemos todos los archivos
//	//sobre los que vamos a ejecutar los tests
//	@Parameters
//	public static Collection<Object[]> getParameters(){
//		Collection<Object[]> res = new LinkedList<Object[]>();
//		
//		File f = new File("fulfilmentTests/negative");
//		File[] subfiles = f.listFiles();
//		for (int i = 0; i < subfiles.length; i++){
//			String path = subfiles[i].getPath();
//			if (!path.endsWith(".svn")){
//				Object[] aux = {path,Boolean.FALSE};
//				res.add(aux);
//			}
//		}
//		
//		f = new File("fulfilmentTests/positive");
//		subfiles = f.listFiles();
//		for (int i = 0; i < subfiles.length; i++){
//			String path = subfiles[i].getPath();
//			if (!path.endsWith(".svn")){
//				Object[] aux = {path,Boolean.TRUE};
//				res.add(aux);
//			}
//		}
//		return res;
//	}
//	
//	//cuerpo del test
//	private void runTest(String path, boolean result){
//		AgreementState state = stateLoader.loadState(path+"/state.properties");
//		Map<ServiceScope,AgreementState> param = new HashMap<ServiceScope, AgreementState>();
//		param.put(new GeneralServiceScope(), state);
//		AbstractDocument doc = ada.loadDocument(path+"/OFFER.wsag");
//		AgreementFulfilmentOperation op = (AgreementFulfilmentOperation) ada.createOperation("Fulfilment");
//		op.addDocument(doc);
//		op.setState(param);
//		ada.analyze(op);
//		boolean opRes = op.isFulfilled();
//		if (!opRes){
//			//explanations
//			ExplainAgreementViolation op2 = (ExplainAgreementViolation) ada.createOperation("ViolationExps");
//			op2.addDocument(doc);
//			op2.setState(param);
//			ada.analyze(op2);
//			Map<AgreementError,Explanation> exps = op2.explainViolation();
//			System.out.println(exps);
//		}
//		assert (opRes == result);
//	}
//	
//}
