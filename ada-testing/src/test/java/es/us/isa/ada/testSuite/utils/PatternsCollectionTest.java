package es.us.isa.ada.testSuite.utils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternsCollectionTest {

	private static final String[] TEXTOS = {"deadTerm1", "deadTerm2, deadTerm3", "deadTerm4, deadTerm5, deadTerm6"};
	
	private static final String REGEX = "\\w+(-\\w+)*";
	
	public static void main(String[] args) {
		Collection<String> res = new LinkedList<String>();
		
		Pattern pattern = Pattern.compile(REGEX);
		for(String texto: TEXTOS){
			Matcher matcher = pattern.matcher(texto);
			int i=0;
			while(matcher.find()){
				i++;
				String dead = matcher.group();
				System.out.println("DeadTerm "+i+": "+dead);
			}
		}
	}
}
