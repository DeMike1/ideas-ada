package es.us.isa.ada.testSuite;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.exceptions.BadSyntaxException;
import es.us.isa.ada.exceptions.InconsistenciesException;
import es.us.isa.ada.wsag10.Term;

public class ExplainDeadTermsTest extends ADATest{
	
	private static final String DOCS_FOLDER = "black-box-tests/04 ExplainDeadTerms (without temp)";
	
	public ExplainDeadTermsTest(String docName, String docPath){
		super(docName, docPath);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		return getParameters(DOCS_FOLDER);
	}

	@Override
	protected Object analyse(String docPath) {
//		if(numberOfTest == 28){
//			System.out.println("Ah� estamos");
//		}
		return ada.explainDeadTerms(docPath);
	}

	@Override
	protected Object parseExpectedResult(String docName) {
		Map<String, Collection<String>> result = new HashMap<String, Collection<String>>();
		
		// sacamos el resultado asociado a docName del properties
		Properties props = getPropertiesDocument();
		String stringResult = props.getProperty(docName);
		
		if(stringResult == null){
			System.err.println("El documento "+this.docPath+" no se encuentra en el documento de properties");
		}else if(stringResult.equalsIgnoreCase("es.us.isa.ada.exceptions.BadSyntaxException")){
			return new BadSyntaxException();
		}else if(stringResult.equalsIgnoreCase("es.us.isa.ada.exceptions.InconsistenciesException")){
			return new InconsistenciesException();
		}else if(stringResult.equalsIgnoreCase("NULL")){
			// no hacemos nada porque el map result debe estar vac�o en este caso
		}else if(stringResult.equalsIgnoreCase("NOT_NULL")){
			// metemos cualquier cosa para que el map result no est� vac�o
			Collection<String> aux = new LinkedList<String>();
			aux.add("Jurado");
			result.put("Antonio", aux);
		}else{
			// la cadena obtenida del properties tendr� el formato:
			// error1=[exp1, exp2, exp3], error2=[exp1, exp2]...
			String LITERAL = "\\w+(-\\w+)*";
			// expresi�n regular para obtener cada conjunto
			// de error y explanations que ser�n una entry en el map
			String EXPLANATIONS_REGEX = "("+LITERAL+")"+"=\\[("+LITERAL+"(,\\s"+LITERAL+")*)"+"\\]";
			Pattern patternExp = Pattern.compile(EXPLANATIONS_REGEX);
			Matcher matcherExp = patternExp.matcher(stringResult);
			while(matcherExp.find()){
				// obtenemos el dead term
				String deadTermString = matcherExp.group(1);
				// y sus explanations correspondientes
				String explanationsString = matcherExp.group(3);
				// expresi�n regular para sacar cada una de las explanations
				// asociadas a un error
				Pattern pattern = Pattern.compile(LITERAL);
				Matcher matcher = pattern.matcher(explanationsString);
				Collection<String> explanations = new LinkedList<String>();
				while(matcher.find()){
					// obtenemos cada una de las explanations
					explanations.add(matcher.group());
				}
				// a�adimos el error y sus explanations al map
				result.put(deadTermString, explanations);
			}
		}
		
		return result;
	}

	@Override
	protected void assertResult(Object expected, Object result) {
		Boolean iguales = null;
		if(expected == null || result == null){
			assertTrue("Expected or result is null - "+getTestInfo(), false);
		}else if(expected instanceof Exception && result instanceof Exception){
			iguales = expected.getClass().getName().equalsIgnoreCase(result.getClass().getName());
		}else if(expected instanceof Map && result instanceof Map){
			Map<String, Collection<String>> exp = (Map<String, Collection<String>>) expected;
			Map<Term, Collection<AgreementElement>> res = (Map<Term, Collection<AgreementElement>>) result;
			
			Set<Term> resultErrors = res.keySet();
			Set<String> expectedErrors = exp.keySet();
			if(resultErrors.size() != expectedErrors.size()){
				iguales = false;
			}else{
				iguales = true;
				// por cada error del resultado
				for(Term resultError: resultErrors){
					if(!expectedErrors.contains(resultError.toString())){
						// si no est� entre los errores esperados, los resultados no son iguales
						iguales = false;
					}else{
						// si est� el error, buscamos ahora las explicaciones
						Collection<String> expectedExplanations = exp.get(resultError.toString());
						Collection<AgreementElement> resultExplanations = res.get(resultError);
						if(expectedExplanations.size() != resultExplanations.size()){
							iguales = false;
						}
						// por cada explicaci�n del resultado esperado
						for(AgreementElement ae: resultExplanations){
							if(!expectedExplanations.contains(ae.toString())){
								iguales = false;
							}
						}
					}
				}
			}
		}else if(result instanceof InconsistenciesException && expectedResult instanceof Map){
			Map<Term, Collection<AgreementElement>> exp = (Map<Term, Collection<AgreementElement>>) expectedResult;
			if(exp.isEmpty()){
				// en caso de que el resultado esperado sea un
				// Map vac�o y devuelva una excepci�n por inconsistencias,
				// consideramos este caso como bueno
				iguales = true;
			}else{
				iguales = false;
			}
		}else{
			iguales = false;
		}
		if(!iguales){
			System.out.println("\n"+getTestInfo());
			System.out.println("   ==> Expected "+expected+" but was "+result);
		}
		assertTrue("Expected "+expected+" but was "+result+" - "+getTestInfo(), iguales);
	}

}
