package es.us.isa.ada.testSuite.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternsMapTest {
	
	private static final String TEXTO = "{sdt1-sdt2-error1=[sdt1-exp11, exp12], error2=[exp21, exp22, exp23], error3=[exp31]";
	
	private static final String LITERAL = "\\w+(-\\w+)*";
	
	/*
	 * Cadena para buscar patrones como 'error1=[exp1, exp2, exp3]'
	 */
	private static final String EXPLANATIONS_REGEX = "("+LITERAL+")"+"=\\[("+LITERAL+"(,\\s"+LITERAL+")*)"+"\\]";
	
	/*
	 * Patr�n para buscar palabras
	 */
	private static final String EXPLANATION_REGEX = LITERAL;
	
	public static void main(String args[]){
		Map<String, Collection<String>> res = new HashMap<String, Collection<String>>();
		
		Pattern patternExp = Pattern.compile(EXPLANATIONS_REGEX);
		Matcher matcherExp = patternExp.matcher(TEXTO);
		int j=0;
		while(matcherExp.find()){
			j++;
			String errorString = matcherExp.group(1);
			System.out.println("Error "+j+": "+errorString);
			String explanationsString = matcherExp.group(3);
			Pattern pattern = Pattern.compile(EXPLANATION_REGEX);
			Matcher matcher = pattern.matcher(explanationsString);
			int k = 0;
			Collection<String> explanations = new LinkedList<String>();
			while(matcher.find()){
				k++;
				explanations.add(matcher.group());
			}
			System.out.println("  ==> Explanations: "+explanations);
			res.put(errorString, explanations);
		}
		System.out.println("El Map resultante ser�a: "+res);
	}
	
}