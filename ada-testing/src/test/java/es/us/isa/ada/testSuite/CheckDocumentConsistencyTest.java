package es.us.isa.ada.testSuite;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Properties;

import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.exceptions.BadSyntaxException;

public class CheckDocumentConsistencyTest extends ADATest{
	
	private static final String DOCS_FOLDER = "black-box-tests/01 CheckDocumentConsistency (without temp)/";
	
	public CheckDocumentConsistencyTest(String docName, String docPath) {
		super(docName, docPath);
	}
	
	@Override
	protected Object analyse(String docPath){
		return ada.checkDocumentConsistency(docPath);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		return getParameters(DOCS_FOLDER);
	}

	@Override
	protected void assertResult(Object expected, Object result) {
		Boolean iguales = null;
		if(expected == null || result == null){
			assertTrue("Expected or result is null - "+getTestInfo(), false);
		}else if(expected instanceof Boolean && result instanceof Boolean){
			iguales = expected.equals(result);
		}else if(expected instanceof Exception && result instanceof Exception){
			iguales = expected.getClass().getName().equalsIgnoreCase(result.getClass().getName());
		}else{
			assertTrue("El resultado esperado y el obtenido no son del mismo tipo o no se conoce como compararlos", false);
		}
		
		if(!iguales){
			System.out.println(getTestInfo());
			System.out.println("   ==> Expected "+expected+" but was "+result);
			System.out.println("   ==> Explanations: "+ada.explainInconsistencies(docPath));
		}
		assertTrue("Expected "+expected+" but was "+result+" - "+getTestInfo(), iguales);
	}

	@Override
	protected Object parseExpectedResult(String docName) {
		Object result = null;
		
		Properties props = getPropertiesDocument();
		String stringResult = props.getProperty(docName);
		
//		Integer initPos = docName.lastIndexOf("RES-")+4;
//		Integer endPos = docName.lastIndexOf(".wsag");
//		String stringResult = docName.substring(initPos, endPos);
		if(stringResult == null){
			System.err.println("El documento "+this.docName+" no se encuentra en el documento de properties");
		}else if(stringResult.equalsIgnoreCase("true")){
			result = true;
		}else if(stringResult.equalsIgnoreCase("false")){
			result = false;
		}else if(stringResult.equalsIgnoreCase("es.us.isa.ada.exceptions.BadSyntaxException")){
			result = new BadSyntaxException();
		}else{
			throw new IllegalArgumentException("El resultado esperado obtenido del properties no encontr� coincidencias para ser parseado: "+stringResult);
		}
		return result;
	}
}
