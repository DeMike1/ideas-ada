/**
 * ADAServiceV2Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package es.us.isa.ada.service;

public class ADAServiceV2Locator extends org.apache.axis.client.Service implements es.us.isa.ada.service.ADAServiceV2 {

    public ADAServiceV2Locator() {
    }


    public ADAServiceV2Locator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ADAServiceV2Locator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ADAServiceV2Port
    private java.lang.String ADAServiceV2Port_address = "http://www.isa.us.es:8081/ADAService";//"http://localhost:8081/ADAService";

    public java.lang.String getADAServiceV2PortAddress() {
        return ADAServiceV2Port_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ADAServiceV2PortWSDDServiceName = "ADAServiceV2Port";

    public java.lang.String getADAServiceV2PortWSDDServiceName() {
        return ADAServiceV2PortWSDDServiceName;
    }

    public void setADAServiceV2PortWSDDServiceName(java.lang.String name) {
        ADAServiceV2PortWSDDServiceName = name;
    }

    public es.us.isa.ada.service.ADAServiceV2PortType getADAServiceV2Port() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ADAServiceV2Port_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getADAServiceV2Port(endpoint);
    }

    public es.us.isa.ada.service.ADAServiceV2PortType getADAServiceV2Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            es.us.isa.ada.service.ADAServiceV2SoapBindingStub _stub = new es.us.isa.ada.service.ADAServiceV2SoapBindingStub(portAddress, this);
            _stub.setPortName(getADAServiceV2PortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setADAServiceV2PortEndpointAddress(java.lang.String address) {
        ADAServiceV2Port_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (es.us.isa.ada.service.ADAServiceV2PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                es.us.isa.ada.service.ADAServiceV2SoapBindingStub _stub = new es.us.isa.ada.service.ADAServiceV2SoapBindingStub(new java.net.URL(ADAServiceV2Port_address), this);
                _stub.setPortName(getADAServiceV2PortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ADAServiceV2Port".equals(inputPortName)) {
            return getADAServiceV2Port();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.ada.isa.us.es/", "ADAServiceV2");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.ada.isa.us.es/", "ADAServiceV2Port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ADAServiceV2Port".equals(portName)) {
            setADAServiceV2PortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
