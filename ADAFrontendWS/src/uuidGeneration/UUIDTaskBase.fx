package uuidGeneration;

import javafx.async.JavaTaskBase;
import javafx.async.RunnableFuture;

/**
 * @author AJurado
 */
public class UUIDTaskBase extends JavaTaskBase{
    
    /**
     * Listener to communicate with javafx code
     */
    public-init var listener:UUIDListener;

	override function create():RunnableFuture{
	    return new UUIDCreator(listener);
	}

}
