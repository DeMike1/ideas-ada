package uuidGeneration;

import java.util.UUID;

public interface UUIDListener {
	
	public void returnUUID(UUID uuid);

}
