package frontend.buttons;

import frontend.Main;
import frontend.Tooltip;
import frontend.PopupWindow;
import frontend.popupWindows.ReportWindow;

import javafx.scene.Node;
import javafx.scene.Group;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.CustomNode;
import javafx.scene.paint.Stop;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Color;
import javafx.scene.layout.Stack;
import javafx.scene.shape.Polygon;
import javafx.scene.text.FontWeight;
import javafx.scene.effect.DropShadow;
import javafx.scene.text.TextAlignment;
import javafx.scene.paint.LinearGradient;
import javafx.scene.input.MouseEvent;


/**
 * @author AJurado
 */
public class ReportButton extends CustomNode {
    
    public var xPos:Number;
    
    public-init var yPos:Number;
    
    var text:Text = Text{
        content: "Send us\nfeedback"
        font: Font.font("serif", FontWeight.BOLD, 22)
        textAlignment: TextAlignment.CENTER
        fill: Color.rgb(90, 60, 7)
        rotate: -5
    }
    
    var backgroundRectFill:Paint = LinearGradient{
        startX: 0.0, startY: 0.0, endX: 0.3, endY: 1.0
        stops: [
        	Stop{ offset: 0.0 color: Color.WHITE }
        	Stop{ offset: 0.5 color: Color.rgb(253, 187, 64) }
        ]
    }
    
    var glowEffect:DropShadow = DropShadow{
        color: Color.BLACK;
    }
    
    var backgroundShape:Polygon = Polygon{
    	points: [
    		75.0, 0.0, 60.0, 10.0,
    		36.0, 5.0, 30.0, 20.0,
    		8.0, 25.0, 15.0, 40.0,
    		0.0, 50.0, 15.0, 60.0,
    		8.0, 75.0, 30.0, 80.0,
    		36.0, 95.0, 60.0, 90.0,
    		75.0, 100.0, 90.0, 90.0,
    		114.0, 95.0, 120.0, 80.0,
    		142.0, 75.0, 135.0, 60.0,
    		150.0, 50.0, 135.0, 40.0,
    		142.0, 25.0, 120.0, 20.0,
    		114.0, 5.0, 90.0, 10.0
    	]
        fill: backgroundRectFill
        effect: glowEffect
    }
    
    var tooltip:Tooltip = Tooltip{
        text: "Help us to improve"
    }
    
    var reportButton:Stack = Stack{
        layoutX: bind xPos, layoutY: yPos
        rotate: 5
        content: [
        	backgroundShape,
        	text
        ]
        cache: true
        onMouseClicked: function( e: MouseEvent ): Void {
            var reportWindow:ReportWindow = ReportWindow{
				id: "report"
				sessionId: Main.getSessionId()
            }
            PopupWindow.addPopupToScene(reportWindow, scene);
        }
        onMouseEntered: function( e: MouseEvent ): Void {
            reportButton.cursor = javafx.scene.Cursor.HAND;
            tooltip.show(e.sceneX, e.sceneY);
        }
        onMouseExited: function( e: MouseEvent ): Void {
            reportButton.cursor = javafx.scene.Cursor.DEFAULT;
            tooltip.hide();
        }
    }

    public override function create(): Node {
        return Group {
            content: [
            	reportButton
            ]
        };
    }
}
