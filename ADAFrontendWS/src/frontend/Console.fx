package frontend;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Stack;
import javafx.scene.layout.ClipView;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.effect.Glow;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Stop;
import javafx.scene.paint.LinearGradient;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.input.MouseEvent;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;
import javafx.animation.Interpolator;

/**
 * @author AJurado
 */
 public class Console extends CustomNode {
    
    /**
     * X default position.
     */
    public var x:Number;
    
    /**
     * Y default position. Final position will be Y + translateY
     */
    public var y:Number;
    
    /**
     * Width
     */
    public var width:Number;
    
    /**
     * Minimum height
     */
    public var minHeight:Number;
    
    /**
     * Scene where this console will be added
     */
    public var sceneAux:Scene;
    
    /**
     * Height of console title bar
     */
    def consoleTitleBarHeight:Number = 20;
	
	/**
	 * Background
	 */
	var background:Rectangle = Rectangle{
	    cache: true
	    layoutY: consoleTitleBarHeight
 	    width: bind width, height: minHeight
 	    fill: Color.WHITE
 	    stroke: Color.GREY
 	}
	
	/**
	 * Close button on title bar
	 */
	var closeButton:ConsoleButton;
	
	/**
	 * Minimize button en title bar
	 */
	var minimiseButton:ConsoleButton;
	
	/**
	 * Console title bar
	 */
	var consoleTitleBar:Group = Group{
	    cache: true
		content: [
			DefaultBackground{
	    	    width: bind width, height: consoleTitleBarHeight
	    	    stroke: Color.GREY
	    	}
			Text{
			    layoutX: 5, layoutY: 15
			    content: "Console"
			}
			minimiseButton = ConsoleButton{
			    layoutX: bind width-35, layoutY: 3
				ttText: "Minimise"
				buttonContent: Group{
				    content: [
				    	Line{
				    	    startX: 0, startY: 3
				    	    endX: 6, endY: 3
				    	    stroke: Color.WHITE
				    	}
				    	Line{
				    	    startX: 3, startY: 0
				    	    endX: 3, endY: 6
				    	    stroke: Color.WHITE
				    	    visible: bind minimised
				    	}
				    ]
				}
				onClick: function(){
				    minimiseConsole();
				}
			}
			closeButton = ConsoleButton{
			    layoutX: bind width-20, layoutY: 3
				ttText: "Close"
				buttonContent: Group{
				    content: [
				    	Line{
				    	    startX: 0, startY: 0
				    	    endX: 6, endY: 6
				    	    stroke: Color.WHITE
				    	}
				    	Line{
				    	    startX: 6, startY: 0
				    	    endX: 0, endY: 6
				    	    stroke: Color.WHITE
				    	}
				    ]
				}
				onClick: function(){
				    hideConsole();
				}
			}
		]
	}
	
	/**
	 * Console lines
	 */
	var lines:ConsoleLine[] = [];
		    	
 	/**
 	 * VBox that contains lines
 	 */
 	var linesVBox:VBox = VBox{
 	    translateX: 1, translateY: 5
 	    content: bind lines //Lines will be added with addLine method
 	}
 	
 	/**
 	 * Scroll bar
 	 */
 	var scrollBar:ScrollBar = ScrollBar{
 	    layoutX: bind width-11, layoutY: consoleTitleBarHeight
 	    vertical: true
 	    height: minHeight+1
 	    min: 0, max: 0
 	    unitIncrement: 10
 	    //disable: true
 	}
 	
 	/**
 	 * This variable is modified by consoleAnimation to show and hide this console
 	 */
 	var animY:Number = 0;
 	
 	/**
 	 * Animation that shows and hides this console
 	 */
	var closeAnimation:Timeline = Timeline {
	    keyFrames: [
	    	KeyFrame {
	    	    time: 0s,
	    	    canSkip: true
	    	    values: [animY => 0 tween Interpolator.EASEBOTH]
	    	}
	        KeyFrame {
	            time: 0.5s,
	            canSkip: true
	            values: [animY => -150 tween Interpolator.EASEBOTH]
	        }
	    ]
	}
	
	/**
	 * Animation that minimise this console
	 */
	var minimiseAnimation:Timeline = Timeline {
	    keyFrames: [
	    	KeyFrame {
	    	    time: 0s,
	    	    canSkip: true
	    	    values: [animY => -150 tween Interpolator.EASEBOTH]
	    	}
	    	KeyFrame {
	    	    time: 0.5s,
	    	    canSkip: true
	    	    values: [animY => -10-consoleTitleBarHeight tween Interpolator.EASEBOTH]
	    	}
	    ]
	}
 	
 	/**
 	 * Function that adds a new line to console
 	 */
 	public function addLine(text:String){
 	    var newLine:ConsoleLine = ConsoleLine{
 	        width: bind width-scrollBar.width
 	        text: text
 	    }
 	    insert newLine into lines;
		//println("\nA�adida la linea: {text}");
		linesVBox.height = sizeof lines*17;
		//println("Numero de lineas = {sizeof lines}");
		//println("lines.height = {linesVBox.height}");
		//println("background.height = {background.height}");
		var linesHeight = linesVBox.height;
		if(linesHeight >= background.height){
			//println("lines >= background");
			scrollBar.max = linesHeight-scrollBar.height;
			//scrollBar.disable = false;
		}else{
			//println("else");
			scrollBar.max = 0;
			//scrollBar.disable = true;
		}
 	}
 	
 	/**
 	 * Delete all lines in this console
 	 */
 	public function clearConsole(){
 	    delete lines;
 	}
 	
 	/**
 	 * Sets result for the last line. Example: "OK", "FAIL"...
 	 */
 	public function setResult(res:String, color:Color){
 	    (lines[sizeof lines-1] as ConsoleLine).setResultText(res, color);
 	}
 	
 	/**
 	 * Function that shows the console
 	 */
 	public function showConsole(){
 	    clearConsole(); //borramos antes de que se muestre y una vez mostrada se a�aden las lineas nuevas
 	    closeAnimation.rate = 1.0;
 	    closeAnimation.play();
 	    minimised = false;
 	}
 	
 	/**
 	 * Function that hides the console
 	 */
 	public function hideConsole(){
 	    closeAnimation.rate = -1.0;
 	    closeAnimation.play(); 
 	    //delete lines.content; Si se hace aqu�, las lineas se borran antes de que la animaci�n haya terminado de ocultar la consola
 	}
 	
 	var minimised:Boolean = false;
 	
 	function minimiseConsole(){
 	    if(minimised){
 	        minimiseAnimation.rate = -1.0;
 	        minimised = false;
 	        minimiseButton.ttText = "Minimise";
 	    }else{
 	        minimiseAnimation.rate = 1.0;
 	        minimised = true;
 	        minimiseButton.ttText = "Maximise";
 	    }
 	    minimiseAnimation.play();
 	}
 	
 	/**
 	 * Returns all consoleLines as String
 	 */
 	public function getConsoleText():String{
 	    var consoleText:String = "";
 	    
 	    for(line in lines){
 	        consoleText = "{consoleText}{line.text} {line.resultText}\n";
 	    }
 	    
 	    return consoleText;
 	}
	
    public override function create(): Node {
        var clip:ClipView;
		return Group{
		    layoutX: bind x, layoutY: bind y+10
		    translateY: bind animY
			effect: DropShadow{
		        radius: 20
		        color: Color.GREY
		    }
		    content: [
		    	consoleTitleBar,
		    	background,
		    	scrollBar,
		    	clip = ClipView{
		    	    clipY: bind javafx.util.Math.min(clip.maxClipY, scrollBar.value);
		    	    layoutY: consoleTitleBarHeight
    			    //A width se le suma 5 para que se vea la sombra
    			    width: bind width+5, height: minHeight //Altura del clip no cambia
    				node: bind linesVBox
    				pannable: false
    			}
		    ]
		}
     }
 }
 
 class ConsoleLine extends CustomNode{
     
     /**
      * Text showed on textLabel
      */
     public var text:String;
     
     /**
      * Text showed on resultLabel
      */
     public var resultText:String = "";
     
     /**
      * Width
      */
     public var width:Number;
     
     /**
      * Label showed on the left of this line
      */
     var textLabel:Label = Label{
         layoutX: 5
         text: text
     }
     
     /**
      * Label showed on the right of this line
      */
     var resultLabel:Label;
     
     /**
      * Background for this line. It will be highlighted when mouse over.
      */
     var background:Rectangle = Rectangle{
         width: bind width, height: textLabel.height
         fill: Color.TRANSPARENT
         onMouseEntered: function( e: MouseEvent ): Void {
             background.fill = Color.GAINSBORO //LIGHTSTEELBLUE ,GAINSBORO, LAVENDER
         }
         onMouseExited: function( e: MouseEvent ): Void {
             background.fill = Color.TRANSPARENT
         }
     }
     
     /**
      * Sets resultLabel text and color
      */
     public function setResultText(res:String, color:Color){
         resultText = res;
         if(color != null){
             resultLabel.textFill = color;
         }
     }
     
     public override function create(): Node {
         return Group{
     		 content: [
           	  	background,
           	  	textLabel,
           	    resultLabel = Label{
           	        layoutX: -10
           	        width: bind width
     				hpos: HPos.RIGHT
       	            text: bind resultText
       	        }
     		 ]
     	 }
     }
 }
 
 class ConsoleButton extends CustomNode{
     
     /**
      * Node to show on button. Ej. close, minimize, expand...
      */
     public var buttonContent:Node;
     
     /**
      * Function executed when this button is clicked
      */
     public var onClick:function();
     
     /**
      * Text showed on this button tooltip
      */
     public var ttText:String on replace{
         tt.text = ttText;
     }
     
     /**
      * Tooltip for this button
      */
     var tt:Tooltip = Tooltip{
         text: ttText
     }
     
     public override function create(): Node {
         return Stack{
             content: [
             	Rectangle{
             	    arcWidth: 4, arcHeight: 4
             	    width: 12, height: 12
 	    			fill: LinearGradient{
 	    				endX: 0
 	    			    stops: [
 	    			    	Stop{
 	    			    		offset: 0.0
 	    			    	    color: Color.RED
 	    			    	}
 	    			    	Stop{
 	    			    		offset: 1.0
 	    			    		color: Color.DARKRED
 	    			    	}
 	    			    ]
 	    			}
 	    			stroke: Color.DARKRED
             	}
             	buttonContent
             ]
			 onMouseEntered: function( e: MouseEvent ): Void {
			 	 this.cursor = Cursor.HAND;
			 	 tt.show(e.sceneX, e.sceneY);
			 }
			 onMouseExited: function( e: MouseEvent ): Void {
			     this.cursor = Cursor.DEFAULT;
			     tt.hide();
			 }
			 onMouseClicked: function ( e: MouseEvent ): Void {
			     onClick();
			 }
         }
     }
 }
