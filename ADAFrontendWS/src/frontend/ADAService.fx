package frontend;

import es.us.isa.ada.service.*;

/**
 * @author AJurado
 */

var servicePort:ADAServiceV2PortType = new ADAServiceV2Locator().getADAServiceV2Port();

public function explainNonCompliance(code1:String, code2:String):AgreementError2ExplanationMapEntry[]{
    var entries = servicePort.explainNonCompliance(code1.getBytes(), code2.getBytes());
    return entries;
}

public function addMetric(metric:String, metricName:String):String{
    return servicePort.addMetricFile(metric.getBytes(), metricName.getBytes());
}

public function getMetric(path:String):String{
    var metric = new String(servicePort.getMetricFile(path));
    return metric;
}

public function xmlToWSAg4People(xml:String):String{
    var bytesIN = xml.getBytes();
    return new String(servicePort.xmlToWSAg4People(bytesIN));
}

public function wsag4PeopleToXML(wsag4people:String):String{
    var bytesIN = wsag4people.getBytes();
    return new String(servicePort.wsag4PeopleToXML(bytesIN));
}