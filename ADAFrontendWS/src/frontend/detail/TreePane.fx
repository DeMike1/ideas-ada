package frontend.detail;
import javafx.ext.swing.SwingComponent;
import javax.swing.JTree;
import javax.swing.tree.*;
import javax.swing.JComponent;
import javax.swing.JScrollPane;

/**
 * @author AJurado
 */

public class TreePane extends SwingComponent{

	public var newJTree:JTree on replace{
	    tree.setModel(newJTree.getModel());
	}

	var tree:JTree;

    override function createJComponent(): JComponent {
        tree = new JTree();
        var scrollPane = new JScrollPane(tree);
        return scrollPane;
    }
}