package frontend.detail;
import frontend.ADADocument;
import frontend.DefaultBackground;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.layout.ClipView;

/**
 * @author AJurado
 */
 
public class LateralErrorBar extends CustomNode {
    
    /**
     * This document errors will be shown
     */
    public var doc:ADADocument on replace{
        //Recorrer sus errores y warnings, situarlos en que linea del texto est�n y calcular la posici�n 
        //a la que los mostraremos
    }
    
    /**
     * This height will be cliped be clipHeight
     */
    public var lateralBarHeight:Number;
    
    /**
     * Clip height
     */
	public var clipHeight:Number;
	
	/**
	 * doc BarError�s that will be shown on this LateralErrorBar. It includes warnings
	 */
	var errors:BarError[];
    
    //Ser� un clip con un rectangulo dentro del tama�o del editorPane
    
    var background:DefaultBackground = DefaultBackground{
        width: 10, height: bind lateralBarHeight
    }
	
    public override function create(): Node {
        return ClipView{
            width: 10, height: clipHeight
            node: Group{
                content: [
                	background,
                	errors
                ]
            }
        }
    }
}

class BarError extends CustomNode{
    
    /**
     * X position inside LateralErrorBar where this BarError will be positioned
     */
    var x:Number; //Siempre ser� la misma posici�n, justo en el centro de 
    
    /**
     * Y position inside LateralErrorBar where this BarError will be positioned
     */
    public var y:Number;
    
    /**
     * If true, the warning icon will be shown
     */
    public var isWarning:Boolean;
    
    override function create():Node{
        return null;
    }
}

class ScrollAdjustmentListener extends java.awt.event.AdjustmentListener{
    
    override function adjustmentValueChanged(e:java.awt.event.AdjustmentEvent){
        //�Es posible hacer esto?
    }
}