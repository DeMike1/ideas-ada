/*
 * Copyright 2006-2008 Kees de Kooter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package frontend.detail.editorkits;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;
import javax.swing.text.PlainView;
import javax.swing.text.Segment;
import javax.swing.text.Utilities;

public class WSAgView extends PlainView {
	
	private static HashMap<Pattern, Color> defaultPatterns;
    
    private static String TAG_COMMENT = "(<\\!-- .* -->)";
    private static String GENERIC_XML_NAME = "[A-Za-z]+[A-Za-z0-9\\-_]*(:[A-Za-z]+[A-Za-z0-9\\-_]+)?";
    private static String TAG_PATTERN = "(</?" + GENERIC_XML_NAME + ")\\s?>?";
    private static String TAG_END_PATTERN = "(/>)";
    private static String TAG_ATTRIBUTE_PATTERN = "(" + GENERIC_XML_NAME + ")\\w*\\=";
    private static String TAG_ATTRIBUTE_VALUE = "\\w*\\=\\w*(\"[^\"]*\")";

    static {
        // NOTE: the order is important!
    	defaultPatterns = new LinkedHashMap<Pattern, Color>();

    	defaultPatterns.put(Pattern.compile(TAG_COMMENT), Color.GRAY);
    	defaultPatterns.put(Pattern.compile(TAG_PATTERN), new Color(63, 127, 127));
    	defaultPatterns.put(Pattern.compile(TAG_ATTRIBUTE_PATTERN), new Color(127, 0, 127));
    	defaultPatterns.put(Pattern.compile(TAG_END_PATTERN), new Color(63, 127, 127));
    	defaultPatterns.put(Pattern.compile(TAG_ATTRIBUTE_VALUE), Color.BLUE);

//        patternColors.put(Pattern.compile("(<.*>$)"), java.awt.Color.GREEN);
//        patternColors.put(Pattern.compile("(\".*\")"), java.awt.Color.CYAN);
//        patternColors.put(Pattern.compile("(>.*</)"), java.awt.Color.RED);
    }

    public WSAgView(Element element) {

        super(element);

        // Set tabsize to 4 (instead of the default 8)
        getDocument().putProperty(PlainDocument.tabSizeAttribute, 4);
    }

    @Override
    protected int drawUnselectedText(Graphics graphics, int x, int y, int p0,
            int p1) throws BadLocationException {

        Document doc = getDocument();
        String text = doc.getText(p0, p1 - p0);
        
        //PRUEBA
        graphics.setPaintMode();
        //FIN_PRUEBA

        Segment segment = getLineBuffer();

        SortedMap<Integer, Integer> startMap = new TreeMap<Integer, Integer>();
        SortedMap<Integer, Color> colorMap = new TreeMap<Integer, Color>();
        
        SortedMap<Integer, Integer> commentsMap = new TreeMap<Integer, Integer>();

        // Match all regexes on this snippet, store positions
        for (Map.Entry<Pattern, Color> entry : defaultPatterns.entrySet()) {

            Matcher matcher = entry.getKey().matcher(text);

            while (matcher.find()) {
            	Color color = entry.getValue();
                startMap.put(matcher.start(1), matcher.end());
                colorMap.put(matcher.start(1), color);
                if(color.equals(Color.GRAY)){
                	commentsMap.put(matcher.start(1), matcher.end());
                }
            }
        }

        // Todo: check the map for overlapping parts
        //TODO buscar los que tienen color de comentario y todos los dem�s que est�n entre
        //su posici�n de start y end ser�n reseteados
        
        //Search for comments color
//        Iterator <Map.Entry<Integer, Color>> it = colorMap.entrySet().iterator();
//        while(it.hasNext()){
//        	Map.Entry<Integer, Color> entry = it.next();
//        	Color color = entry.getValue();
//        	if(color.equals(Color.GRAY)){
//        		Integer start = entry.getKey();
//        		
//        	}
//        }
        
        int i = 0;

        // Colour the parts
        for (Map.Entry<Integer, Integer> entry : startMap.entrySet()) {
            int start = entry.getKey();
            int end = entry.getValue();

            if (i < start) {
                graphics.setColor(Color.black);
                doc.getText(p0 + i, start - i, segment);
                x = Utilities.drawTabbedText(segment, x, y, graphics, this, i);
            }
            
            Boolean insideAComment = false;
            for(Map.Entry<Integer, Integer> comment:commentsMap.entrySet()){
            	int commentStart = comment.getKey();
            	int commentEnd = comment.getValue();
            	
            	if(commentStart < start && commentEnd > end){
            		insideAComment = true;
            	}
            }

            if(!insideAComment){
				graphics.setColor(colorMap.get(start));
				i = end;
				doc.getText(p0 + start, i - start, segment);
				x = Utilities.drawTabbedText(segment, x, y, graphics, this,
						start);
			}
        }

        // Paint possible remaining text black
        if (i < text.length()) {
            graphics.setColor(Color.black);
            doc.getText(p0 + i, text.length() - i, segment);
            x = Utilities.drawTabbedText(segment, x, y, graphics, this, i);
        }

        return x;
    }

}
