/*
 * Copyright 2006-2008 Kees de Kooter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package frontend.detail.editorkits;

import javax.swing.text.StyledEditorKit;
import javax.swing.text.ViewFactory;

public class WSAgEditorKit extends StyledEditorKit {

    private ViewFactory wsagViewFactory;

    public WSAgEditorKit() {
        wsagViewFactory = new WSAgViewFactory();
    }
    
    @Override
    public ViewFactory getViewFactory() {
        return wsagViewFactory;
    }

    @Override
    public String getContentType() {
        return "text/wsag";
    }

    
}
