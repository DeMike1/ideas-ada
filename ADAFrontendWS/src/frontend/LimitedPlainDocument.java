package frontend;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class LimitedPlainDocument extends PlainDocument{

	private int limit;
	
	public LimitedPlainDocument(int limit){
		super();
		this.limit = limit;
	}
	
	public void insertString(int offset, String str, AttributeSet a) throws BadLocationException{
		int length = str.length();
		
		if(offset + length > limit){
			length = limit - offset;
		}
		super.insertString(offset, str.substring(0, length), a);
	}
}
