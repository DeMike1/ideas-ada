package frontend;
import frontend.detail.EditorPane;
import frontend.detail.editorkits.WSAgView;
import frontend.detail.editorkits.WSAg4PeopleView;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;
import java.awt.Color;
import java.util.*;

/**
 * @author AJurado
 */
 
public class ADADocument{
    
    public var docName: String;
    
    public var docPath: String;
    
    /**
     * Document xml content
     */
    public var docContent: String;
    
    public var wsag4people: String;
    
    /**
     * True if this document is default document loaded from server
     * False if this document is loaded from client
     */
    public var docDefault: Boolean;
    
    //hay que a�adir los errores como pares error-causas
//    public function addError(pattern:String){
//        insert pattern into errors;
//    }
    
    //hay que a�adir los warnings como pares warning-causas
 //   public function addWarning(pattern:String){
 //       insert pattern into warnings;
 //   }
    
    public function addWarningOrError(set:Collection){
        insert set into errorSets;
    }
    
//    public function setErrorsAndWarnings(editor:EditorPane){
//        clearErrorsAndWarnings(editor);
//       //meter aqui un numerito
//        for(e in errors){
//            editor.highlighter.addErrorHighlight(e);
//        }
//        for(w in warnings){
//            editor.highlighter.addWarningHighlight(w);
//        }
//    }
   
     public function setErrorsAndWarnings(editor:EditorPane){
            clearErrorsAndWarnings(editor);
            i = 0;
            for(e in errorSets){
                editor.highlighter.addSetHighlight(e,i);
                i++;
            }
      }
    
    public function clearErrorsAndWarnings(editor:EditorPane){
        editor.highlighter.clearHighlights();
    }
    
    public function resetMaps(){
        delete errorSets;
   //     delete errors;
   //     delete warnings;
    }
    
    public function getErrorsAsString():String{
        var errors:String = "";
        for(eSet in errorSets){
            for(e in eSet){
                if(errors == ""){
                    errors = e as String;
                }else{
                    errors = "{errors}\n{e as String}";
                }
            }
        }
        return errors;
    }
    
 //   var errors:String[];
    
//    var warnings:String[];
    
    var errorSets:Collection[];
    
    var i:Integer;
    
}