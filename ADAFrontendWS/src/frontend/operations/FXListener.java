package frontend.operations;

public interface FXListener {
	
	public void callback(Object[] data);
	
	public void addLineToConsole(String msg);
	
	public void setResultToConsoleLine(String msg, javafx.scene.paint.Color color);

}
