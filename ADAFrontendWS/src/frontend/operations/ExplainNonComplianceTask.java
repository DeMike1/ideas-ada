package frontend.operations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.exceptions.BadSyntaxException;
import es.us.isa.ada.service.AgreementError2ExplanationMapEntry;

public class ExplainNonComplianceTask extends ADATask{
	
	private String template;
	
	private String offer;

	public ExplainNonComplianceTask(FXListener listener, String template, String offer) {
		super(listener);
		this.template = template;
		this.offer = offer;
	}

	@Override
	public void run() throws Exception {
		//el error k de offerErrors tiene como causas el elemento k de tempErrors
		Object[] result = new Object[5];
		List<String> templateErrorsNames = new ArrayList<String>();
		List<String> offerErrorsNames = new ArrayList<String>();
		
		Collection<Collection<String>> tempErrors = new LinkedList<Collection<String>>();
		Collection<Collection<String>> offerErrors = new LinkedList<Collection<String>>();
		try{
			boolean isTempConsistent;
			boolean isOfferConsistent;
			boolean isCompliant = false;
			
			//Checking Template Conflicts
			addConsoleLine("Checking template conflicts"+consoleLineExtendChars);
			
			//Checking Template Inconsistent Terms
			addConsoleLine(consoleLineFirstChars+"Checking template inconsistent terms"+consoleLineExtendChars);
			isTempConsistent = servicePort.checkDocumentConsistency(template.getBytes());
			if(isTempConsistent){
				setResultConsoleLine("Inconsistent terms-free", correctMessageColor);
			}else{
				setResultConsoleLine("Inconsistent terms", wrongMessageColor);
			}
			
			//Checking Offer Conflicts
			addConsoleLine("Checking offer conflicts"+consoleLineExtendChars);
			
			//Checking Offer Inconsistent Terms
			addConsoleLine(consoleLineFirstChars+"Checking offer inconsistent terms"+consoleLineExtendChars);
			isOfferConsistent = servicePort.checkDocumentConsistency(offer.getBytes());
			if(isOfferConsistent){
				setResultConsoleLine("Inconsistent terms-free", correctMessageColor);
			}else{
				setResultConsoleLine("Inconsistent terms", wrongMessageColor);
			}
			
			if(isTempConsistent && isOfferConsistent){
				
				//Checking Compliance Conflicts
				addConsoleLine("Checking compliance conflicts (This may take several minutes)"+consoleLineExtendChars);
				isCompliant = servicePort.isCompliant(template.getBytes(), offer.getBytes());
				if(!isCompliant){
					setResultConsoleLine("Compliance conflicts", wrongMessageColor);
					
					//Explaining Non-Compliance conflicts
			        addConsoleLine("Explaining non-compliance conflicts (This may take several minutes)"+consoleLineExtendChars);
			        AgreementError2ExplanationMapEntry[] errorsArrayMapEntry = servicePort.explainNonCompliance(template.getBytes(), offer.getBytes());
			        
			        for(AgreementError2ExplanationMapEntry e: errorsArrayMapEntry){
			        	AgreementElement[] agreementErrorElements = e.getKey().getElements();
			        	Collection<String> offerElems = new LinkedList<String>();
			        	for(AgreementElement agErrorElem: agreementErrorElements){
			        		String offerElemName = agErrorElem.getName();
			        		addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"Offer conflict: "+offerElemName);
			        		if (!offerErrorsNames.contains(offerElemName)){
				        		offerElems.add(offerElemName);
				        		offerErrorsNames.add(offerElemName);
				        	}
			        	}
			        	offerErrors.add(offerElems);
			        	
			        	AgreementElement[] explanationElements = e.getValue().getElements();
			        	Collection<String> tempElems = new LinkedList<String>();
			        	for(AgreementElement explanationElem: explanationElements){
			        		String templateElemName = explanationElem.getName();
			        		addConsoleLine(consoleLineFirstChars+consoleLineFirstChars+"  - Template cause: "+templateElemName);
			        		if (!templateErrorsNames.contains(templateElemName)){
			        			tempElems.add(templateElemName);
			        			templateErrorsNames.add(templateElemName);
			        		}
			        	}
			        	tempErrors.add(tempElems);
			        }
//			        addConsoleLine(consoleLineFirstChars+"Template errors"+consoleLineExtendChars);
//					if(templateErrorsNames.size() == 1){
//					    setResultConsoleLine("1 Error", wrongMessageColor);
//					}else{
//					    setResultConsoleLine(templateErrorsNames.size()+" Errors", wrongMessageColor);
//					}
//					addConsoleLine(consoleLineFirstChars+"Offer errors"+consoleLineExtendChars);
//					if(offerErrors.size() == 1){
//					    setResultConsoleLine("1 Error", wrongMessageColor);
//					}else{
//					    setResultConsoleLine(offerErrors.size()+" Errors", wrongMessageColor);
//					}
				}else{
					setResultConsoleLine("Compliance conflicts-free", correctMessageColor);
				}
			}
			result[0] = isTempConsistent;
			result[1] = isOfferConsistent;
			result[2] = isCompliant;
			result[3] = tempErrors;
			result[4] = offerErrors;
		}catch(BadSyntaxException e){
			addConsoleLine("There was an error: It may be due to a syntax error, please check the document syntax.");
			result = null;
		}
		returnResult(result);
	}
}
