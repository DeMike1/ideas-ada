package frontend.operations;

import es.us.isa.ada.service.*;

import com.sun.javafx.functions.Function0;
import javafx.async.RunnableFuture;

public abstract class ADATask implements RunnableFuture{
	
	private FXListener listener;
	
	protected ADAServiceV2PortType servicePort;
	
	protected final String consoleLineFirstChars = "   ";
	
	protected final String consoleLineExtendChars = "..........";
	
	protected final javafx.scene.paint.Color correctMessageColor = javafx.scene.paint.Color.$GREEN;
	
	protected final javafx.scene.paint.Color wrongMessageColor = javafx.scene.paint.Color.$RED;
	
	public ADATask(FXListener listener){
		this.listener = listener;
		try{
//			Esta linea daba error porque no hay ninguna clase ADAServiceV2PortTypeProxy
//			servicePort = new ADAServiceV2PortTypeProxy("http://150.214.188.147:8081/ADAService");
			servicePort = new ADAServiceV2Locator().getADAServiceV2Port();
		}catch(Exception e){
			System.out.println("No se ha podido conectar con el servicio");
			//e.printStackTrace();
		}
	}
	
//	@Override
	public abstract void run() throws Exception;
	
	protected void addConsoleLine(final String msg){
		javafx.lang.FX.deferAction(new Function0<Void>(){
//			@Override
			public Void invoke() {
				listener.addLineToConsole(msg);
				return null;
			}
		});
	}
	
	protected void setResultConsoleLine(final String msg, final javafx.scene.paint.Color color){
		javafx.lang.FX.deferAction(new Function0<Void>() {
//			@Override
			public Void invoke() {
				listener.setResultToConsoleLine(msg, color);
				return null;
			}
		});
	}
	
	protected void returnResult(final Object[] result){
		javafx.lang.FX.deferAction(new Function0<Void>() {
//            @Override
            public Void invoke() {
                listener.callback(result);
                return null;
            }
        });
	}
}
