package frontend.operations;
import frontend.PopupWindow;
import frontend.popupWindows.MessageWindow;
import frontend.popupWindows.OperationWindow;
import frontend.ADADocument;
import frontend.detail.TabbedMenu;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.control.ListView;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/**
 * @author AJurado
 */
 
public class OperationsList extends CustomNode {
    
    public var operations:Operation[];
    public var x:Integer;
    public var y:Integer;
    public var isVisible: Boolean = false;
    public var openedTemps: ADADocument[];
    public var openedOffers: ADADocument[];
    
    public var tabbedMenu:TabbedMenu;
    
    var listView: ListView;

    public override function create(): Node {
        return Group {
            layoutX: x, layoutY: y
            content: [
            	listView = ListView{
            	    items: for(o in operations){
            	        o.opName;
            	    }
            	    width: 350, height: sizeof operations*20
            	}
            	Button{
            	    layoutX: listView.layoutBounds.minX+10, layoutY: listView.layoutBounds.maxY+3
            	    text: "Accept"
            	    action: function(){
            	        if(tabbedMenu.continueIfUnsavedChanges()){
            	            var s = scene;
                			delete s.lookup("opList") from s.content;
                			isVisible = false;
                			if(listView.selectedItem == null){
                			    var pw = MessageWindow{
                			        id: "error"
                			        message: "Error: No operation selected"
                			    }
                			    PopupWindow.addPopupToScene(pw, s);
                			}else{
                			    for(o in operations){
                    	            if(o.opName == listView.selectedItem){
                    	                //Si hay un documento seleccionado y la operaci�n es de un 
                    	                //s�lo documento se deber� lanzar la operaci�n del tir�n
                    	                if(sizeof tabbedMenu.tabs > 0 and o.nDocs == 1){
                    	                    var docs:ADADocument[] = [];
                    	                    insert tabbedMenu.tabs[tabbedMenu.indexActiveTab].doc into docs;
                    	                    o.docs = docs;
                    	                    o.console.showConsole();
                    	                    o.execute(o, o.docs);
                    	                }else{
                    	                    var pw = OperationWindow{
                        	                    id: "op"
                        	                    operation: o
                        	                    temps: openedTemps
                        	                    offers: openedOffers
                        	                }
                        	                PopupWindow.addPopupToScene(pw, s);
                    	                }
                    	                break;
                    	            }
                    	        }
                			}
            	        }
            	    }
            	}
            	Button{
            	    layoutX: listView.layoutBounds.minX+80, layoutY: listView.layoutBounds.maxY+3
            	    text: "Cancel"
            	    action: function(){
            	        delete scene.lookup("opList") from scene.content;
            	        isVisible = false;
            	    }
            	}
            ]
        };
    }
}