package frontend.operations;

import frontend.ADADocument;
import javafx.async.JavaTaskBase;
import javafx.async.RunnableFuture;

/**
 * @author AJurado
 */
public class ADATaskBase extends JavaTaskBase{
    
    /**
     * Listener
     */
    public-init var listener:FXListener;
    
    /**
     * Document
     */
    public-init var docs:ADADocument[];
    
    /**
     * Creates a RunnableFuture
     */
    public-init var createTask:function(listener:FXListener, docs:ADADocument[]):RunnableFuture;
    
    override function create():RunnableFuture{
        createTask(listener, docs);
    }
}