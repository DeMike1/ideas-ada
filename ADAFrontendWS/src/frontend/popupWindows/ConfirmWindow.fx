package frontend.popupWindows;
import frontend.PopupWindow;
import frontend.detail.TabbedMenu;
import javafx.scene.Scene;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.control.Button;

/**
 * @author AJurado
 */
 
public class ConfirmWindow extends CustomNode {
    
    public-init var message:String;
    
    /**
     * Function executed when user clicks "yes"
     */
    public-init var yesAction:function();
    
    /**
     * Function executed when user clicks "no"
     */
    public-init var noAction:function();
    
    /**
     * ImageView to show icon
     */
    var imageView = ImageView{
        image: Image{
            url: "{__DIR__}icons/info.png"
        }
        cache: true
    }
    
    /**
     * Text that shows the message
     */
    var text:Text = Text{
        layoutX: 75, layoutY: 25
        wrappingWidth: 400
        font: Font{
            size: 13
        }
        content: message
    }
    
    var acceptButton:Button = Button{
        layoutX: 100, layoutY: 60
        text: "Yes"
        action: function(){
            yesAction();
            window.closeWindow();
        }
    }
    
    var cancelButton:Button = Button{
        layoutX: 180, layoutY: 60
        text: "No"
        action: function(){
            noAction();
            window.closeWindow();
        }
    }
    
    var content:Node;
    
    var window:PopupWindow;
 
    public override function create(): Node {
        return Group {
			content: [
				window = PopupWindow{
            	    id: id //Important, this id makes posible to close the PopupWindow
            	    width: bind content.boundsInLocal.width+50, height: bind content.boundsInLocal.height+50
				}
				content = Group{
            	    layoutX: bind window.boundsInLocal.minX+25, layoutY: bind window.boundsInLocal.minY+40
            	    content: [
            	    	imageView,
            	    	text,
            	    	acceptButton,
            	    	cancelButton
            	    ]
            	}
			]
		};
	}
}
