package frontend.popupWindows;
import frontend.ADADocument;
import frontend.operations.Operation;
import frontend.PopupWindow;
import frontend.popupWindows.MessageWindow;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.Cursor;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.text.Text;
import javafx.scene.text.Font;

/**
 * @author AJurado
 */
 
public class OperationWindow extends CustomNode {

	public var operation:Operation;
	
    public var temps:ADADocument[];
    
    public var offers:ADADocument[];
    
    public var content:Node;
    
    def contentPosX:Integer = 75;
    def contentPosY:Integer = 25;
    
    var listViewsWidth:Integer = 300;
    var listViewsHeight:Integer = 150;
    
    var execButton:Button = Button{
        layoutInfo: LayoutInfo{
            hpos: HPos.CENTER
        }
        text: "Execute"
        //action: must be defined depending on operation
    }
    
    var window:PopupWindow = PopupWindow{
        id: id //Important, this id makes posible to close the PopupWindow
        width: bind content.boundsInLocal.width+50, height: bind content.boundsInLocal.height+50
    }
    
    /**
     * Search a document by its name on temps or offers sequence depending on "type"
     */
    function searchDocumentByName(name:String, type:String):ADADocument{
        var document:ADADocument;
        if(type == "template"){
            for(t in temps){
                if(t.docName == name){
                    document = t;
                    break;
                }
            }
        }
        if(type == "offer"){
            for(o in offers){
                if(o.docName == name){
                    document = o;
                    break;
                }
            }
        }
        return document;
    }
    
    /**
     * Search a document by its name on temps and offers
     */
    function searchDocumentByName(name:String):ADADocument{
        var document:ADADocument;
        var found:Boolean;
        for(t in temps){
            if(t.docName == name){
                document = t;
                found = true;
                break;
            }
        }
        if(not found){
            for(o in offers){
                if(o.docName == name){
                    document = o;
                    found = true;
                    break;
                }
            }
        }
        return document;
    }

    public override function create(): Node {
        if(operation.nDocs == 1){
            var leftLV:ListView = ListView{
	    	    layoutInfo: LayoutInfo{
	    	        width: listViewsWidth
	    	        height: listViewsHeight
	    	    }
	    	    items: [
	    			for(t in temps){
	    			    t.docName;
	    			}
	    			for(o in offers){
	    			    o.docName;
	    			}
	    	    ]
	    	}
            var rightLV:ListView = ListView{
	    	    layoutInfo: LayoutInfo{
	    	        width: listViewsWidth
	    	        height: listViewsHeight
	    	    }
	    	    items: []
	    	}
    	    var addDocButton:Button = Button{
    	        text: "Add =>"
    	        action: function(){
    	            var puw:MessageWindow;
	    	        if(leftLV.selectedItem == null){
	    	            puw = MessageWindow{
	    	                id: "error"
	    	            	message: "Error: No document selected\nPlease select a document and click add button"
	    	            }
	    	            PopupWindow.addPopupToScene(puw, scene);
	    	        }else if(sizeof rightLV.items > 0){
	    	            puw = MessageWindow{
	    	                id: "error"
	    	            	message: "Error: Only one document is needed and a document has been already added"
	    	            }
	    	            PopupWindow.addPopupToScene(puw, scene);
	    	        }else{
	    	            insert leftLV.selectedItem into rightLV.items
	    	        }
    	        }
    	    }
    	    
    	    var delDocButton:Button = Button{
    	        text: "<= Del"
    	        action: function(){
    	        	delete rightLV.selectedItem from rightLV.items
    	        }
    	    }
    	    
            execButton.action = function(){
                if(sizeof rightLV.items < 1){
                    var puw:MessageWindow = MessageWindow{
                        id: "error"
                        message: "This operation needs one document.\nPlease, select a document and click add button before executing operation"
                    }
                    PopupWindow.addPopupToScene(puw, scene);
                }else{
					var docs:ADADocument[] = [];
					insert searchDocumentByName(rightLV.items[0].toString()) into docs;
					operation.docs = docs;
					operation.console.showConsole();
					operation.execute(operation, operation.docs);
					window.closeWindow();
                }
            }
            content = VBox{
                layoutX: bind window.boundsInLocal.minX+25, layoutY: bind window.boundsInLocal.minY+40
                hpos: HPos.CENTER
                vpos: VPos.CENTER
                spacing: 20
                content: [
                	Text{
                	    content: "This operation works with one document.\nPlease, select only one from the list."
                	    font: Font{
                	        size: 13
                	    }
                	}
                	HBox{
                	    spacing: 10
                	    content: [
                	    	leftLV,
                	    	VBox{
                	    	    spacing: 10
                	    		content: [
                	    			addDocButton,
    	                	    	delDocButton
                	    		]
                	    	}
                	    	rightLV
                	    ]
                	}
                	execButton
                ]
            }
        }else if(operation.nDocs > 1){
            if(operation.nTemps == 1 and operation.nOffers == 1){
                var availableTemps:ListView = ListView{
                    layoutInfo: LayoutInfo{
                        width: listViewsWidth
                        height: listViewsHeight
                    }
                    items: [
                    	for(t in temps){
                    	    t.docName;
                    	}
                    ]
                }
                var availableOffers:ListView = ListView{
                    layoutInfo: LayoutInfo{
                        width: listViewsWidth
                        height: listViewsHeight
                    }
                    items: [
                    	for(o in offers){
                    	    o.docName;
                    	}
                    ]
                }
                var addedTemps:ListView = ListView{
    	    	    layoutInfo: LayoutInfo{
    	    	        width: listViewsWidth
    	    	        height: listViewsHeight
    	    	    }
    	    	    items: []
    	    	}
                var addedOffers:ListView = ListView{
    	    	    layoutInfo: LayoutInfo{
    	    	        width: listViewsWidth
    	    	        height: listViewsHeight
    	    	    }
    	    	    items: []
    	    	}
    	    	
    	    	var addTempButton:Button = Button{
                    text: "Add Temp =>"
                    action: function(){
                        var puw:MessageWindow;
                        if(availableTemps.selectedItem == null){
                            puw = MessageWindow{
                                id: "error"
                                message: "Error: No document selected\nPlease select a document and click add button"
                            }
                            PopupWindow.addPopupToScene(puw, scene);
                        }else if(sizeof addedTemps.items > 0){
                            if(operation.nTemps == 1){
                                puw = MessageWindow{
        	    	                id: "error"
        	    	            	message: "Error: Only one document is needed and a document has been already added"
        	    	            }
        	    	            PopupWindow.addPopupToScene(puw, scene);
                            }
                        }else{
                            insert availableTemps.selectedItem into addedTemps.items;
                        }
                    }
                }
                var addOfferButton:Button = Button{
                    text: "Add Offer =>"
                    action: function(){
                        var puw:MessageWindow;
                        if(availableOffers.selectedItem == null){
                            puw = MessageWindow{
                                id: "error"
                                message: "Error: No document selected\nPlease select a document and click add button"
                            }
                            PopupWindow.addPopupToScene(puw, scene);
                        }else if(sizeof addedOffers.items > 0){
                            if(operation.nOffers == 1){
                                puw = MessageWindow{
        	    	                id: "error"
        	    	            	message: "Error: Only one document is needed and a document has been already added"
        	    	            }
        	    	            PopupWindow.addPopupToScene(puw, scene);
                            }
                        }else{
                            insert availableOffers.selectedItem into addedOffers.items;
                        }
                    }
    	    	}
                
                var delTempButton:Button = Button{
                    text: "<= Del Temp"
                    action: function(){
                        delete addedTemps.selectedItem from addedTemps.items;
                    }
                }
                
                var delOfferButton:Button = Button{
                    text: "<= Del Offer"
                    action: function(){
                        delete addedOffers.selectedItem from addedOffers.items;
                    }
                }
                
                execButton.action = function(){
                    if(sizeof addedTemps.items < 1 or sizeof addedOffers.items < 1){
                        var puw:MessageWindow = MessageWindow{
                            id: "error"
                            message: "This operation needs one template and one offer.\nPlease, add this documents before executing operation"
                        }
                        PopupWindow.addPopupToScene(puw, scene);
                    }else{
                        var docs:ADADocument[] = [];
                        insert searchDocumentByName(addedTemps.items[0].toString(), "template") into docs;
                        insert searchDocumentByName(addedOffers.items[0].toString(), "offer") into docs;
                        operation.docs = docs;
    					operation.console.showConsole();
    					operation.execute(operation, operation.docs);
    					window.closeWindow();
                    }
                }
                
                content = VBox{
                    layoutX: bind window.boundsInLocal.minX+25, layoutY: bind window.boundsInLocal.minY+40
                    hpos: HPos.CENTER
                    vpos: VPos.CENTER
                    spacing: 20
                    content: [
                    	Text{
                    	    content: "This operation works with several documents. Please, select documents to analyse from the list."
                    	    font: Font{
                    	        size: 13
                    	    }
                    	}
                    	HBox{
                    	    spacing: 10
                    	    content: [
                    	    	availableTemps,
                    	    	VBox{
                    	    	    spacing: 10
                    	    		content: [
                    	    			addTempButton,
        	                	    	delTempButton
                    	    		]
                    	    	}
                    	    	addedTemps
                    	    ]
                    	}
                    	HBox{
                    	    spacing: 10
                    	    content: [
                    	    	availableOffers,
                    	    	VBox{
                    	    	    spacing: 10
                    	    	    content: [
                    	    	    	addOfferButton,
                    	    	    	delOfferButton
                    	    	    ]
                    	    	}
                    	    	addedOffers
                    	    ]
                    	}
                    	execButton
                    ]
                }
            }else{
                var puw = MessageWindow{
                    id: "error"
                    message: "Error: Incorrect operation declaration"
                }
                PopupWindow.addPopupToScene(puw, scene);
            }
        }
        
        return Group {
            content: [
            	window,
            	content
            ]
        };
    }
}