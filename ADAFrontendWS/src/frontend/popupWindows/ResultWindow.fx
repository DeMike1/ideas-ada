package frontend.popupWindows;
import frontend.PopupWindow;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;

/**
 * @author AJurado
 */

public class ResultWindow extends CustomNode {

	override var id on replace{
	    if(id == "consistentWithWarnings"){
	        message = "The document is consistent!. However, it includes dead or ludicrous terms.\nYou may use �Explain Conflicts (single-document)� operation to see the origin for such dead or ludicrous terms.";
	        icon = Image{
	            url: "{__DIR__}icons/ok.png"
	        }
	    }else if(id == "consistentWithNoWarnings"){
	        message = "Congratulations, the document is consistent!.\nMoreover, it includes neither dead nor ludicrous terms.";
	        icon = Image{
	            url: "{__DIR__}icons/ok.png"
	        }
	    }else if(id == "inconsistent"){
	        message = "The document is inconsistent!.\nYou may use �Explain Conflicts (single-document)� operation to see the origin for such inconsistent terms.";
	        icon = Image{
	            url: "{__DIR__}icons/nook.png"
	        }
	    }else if(id == "compliant"){
	        message = "Congratulations, the agreement offer is compliant with the template.";
	    	icon = Image{
	    	    url: "{__DIR__}icons/ok.png"
	    	}
	    }else if(id == "nonCompliant"){
	        message = "The agreement offer is NOT compliant with the template.\nYou may use �Explain Non-Compliance Conflicts (template-offer)� operation to see the origin for such non-compliance.";
	        icon = Image{
	            url: "{__DIR__}icons/nook.png"
	        }
	    }else if(id == "inconsistentOnComplianceOp"){
	        message = "Some of this documents are inconsistent.\nPlease use �Check Conflicts (single-document)� before checking compliance.";
	    	icon = Image{
	    	    url: "{__DIR__}icons/nook.png"
	    	}
	    }else{
	        println("Invalid id on ResultWindow");
	    }
	}
	
	var message:String;
	
	var text:Text = Text{
	    layoutX: 75, layoutY: 25
        wrappingWidth: 400
        font: Font{
            size: 13
        }
        content: message
	}

    /**
     * Image showed on imageView
     */
    var icon:Image;
    
    /**
     * ImageView to show icon
     */
    var imageView = ImageView{
        image: icon
        cache: true
    }
    
    var showInconsistenciesButton:Button = Button{
        layoutX: 80, layoutY: 40
        text: "Explain Conflicts (single-document)"
        action: function(){
            window.closeWindow();
        }
    }

	var content:Group = Group{
	    layoutX: bind window.boundsInLocal.minX+25, layoutY: bind window.boundsInLocal.minY+40
	    content: [
	    	imageView,
	    	text
	    ]
	}

	var window:PopupWindow = PopupWindow{
	    id: id //Important, this id makes posible to close the PopupWindow
	    width: bind content.boundsInLocal.width+50, height: bind content.boundsInLocal.height+50
	}

    public override function create(): Node {
        //Para esto deber�a llamar desde aqui a explainInconsistencies y 
        //pasarle el documento como parametro pero no hay acceso desde aqui
        /*if(id == "inconsistent"){
            insert showInconsistenciesButton into content.content;
        }*/
        return Group {
            content: [
            	window,
            	content
            ]
        };
    }
}