package frontend.popupWindows;
import frontend.PopupWindow;
import frontend.LimitedPlainDocument;
import frontend.log.AddReportTaskBase;
import frontend.log.AddReport;

import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextBox;
import javafx.ext.swing.SwingComponent;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Tile;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Dimension;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.layout.LayoutInfo;
import javafx.geometry.HPos;
import javafx.scene.paint.Color;

/**
 * @author AJurado
 */
 public class ReportWindow extends CustomNode {
    
    def maxCharsNameInput:Integer = 30;
    def maxCharsOrgInput:Integer = 45;
    def maxCharsEmailInput:Integer = 45;
    def maxCharsReportInput:Integer = 3000;
    
    def defaultFont:Font = Font{
        size: 13;
    }
    
    public-init var sessionId:String;
	
    var nameLabel:Label = Label{
        text: "* Your name:"
        font: defaultFont
    }
    
    var nameInput:TextBox = TextBox{
        columns: 10
        onKeyTyped: function( e: KeyEvent ): Void {
            nameInput.commit();
            if(nameInput.text.length() > maxCharsNameInput){
                nameInput.text = nameInput.text.substring(0, maxCharsNameInput);
                nameInput.end();
            }
        }
    }
    
    var orgLabel:Label = Label{
        text: "Your organization:"
        font: defaultFont
    }
    
    var orgInput:TextBox = TextBox{
        columns: 19
        onKeyTyped: function( e: KeyEvent ): Void {
            orgInput.commit();
            if(orgInput.text.length() > maxCharsOrgInput){
                orgInput.text = orgInput.text.substring(0, maxCharsOrgInput);
                orgInput.end();
            }
        }
    }
    
    var emailLabel:Label = Label{
        text: "Your email:"
        font: defaultFont
    }
    
    var emailInput:TextBox = TextBox{
        columns: 19
        onKeyTyped: function( e: KeyEvent ): Void {
            emailInput.commit();
            if(emailInput.text.length() > maxCharsEmailInput){
                emailInput.text = emailInput.text.substring(0, maxCharsEmailInput);
                emailInput.end();
            }
        }
    }
    
    var reportLabel:Label = Label{
        text: "Report text:"
        font: defaultFont
    }
    
    var reportInput:JTextArea = new JTextArea(new LimitedPlainDocument(maxCharsReportInput));
    
    function initJTextArea():SwingComponent{
        reportInput.setLineWrap(true);
        reportInput.setWrapStyleWord(true);
        
        var sp:JScrollPane = new JScrollPane(reportInput);
        sp.setPreferredSize(new Dimension(350, 100));
        
        var sc:SwingComponent = SwingComponent.wrap(sp);
        
        return sc;
    }
    
    var acceptButton:Button = Button{
    	text: "Send"
    	action: function(){
    		if(nameInput.text != ""){
    		    AddReportTaskBase{
    	            text: reportInput.getText()
    	            username: nameInput.text
    	            userOrganization: orgInput.text
    	            userEmail: emailInput.text
    	            sessionId: sessionId
    	            createTask: function(report, username, org, email, sessionId){
    	                return new AddReport(sessionId, report, username, org, email);
    	            }
    	        }.start();
    		    window.closeWindow();
    		}
    	}
    }
    
    var improveMessage:Text = Text{
        content: "Help us to improve"
        font: Font{
            size: 18
        }
        layoutInfo: LayoutInfo{
            hpos: HPos.CENTER
        }
    }
    
    var requiredFieldsMessage:Text = Text{
        content: "* = Required field"
        fill: Color.RED;
        layoutInfo: LayoutInfo{
            hpos: HPos.RIGHT;
        }
    }
    
    var content:Node = VBox{
        layoutX: bind window.boundsInLocal.minX+25, layoutY: bind window.boundsInLocal.minY+40
        spacing: 10
        content: [
        	improveMessage,
        	Tile{
        	    rows: 4, columns: 2
        	    hgap: 5, vgap: 3
        	    content: [
        	    	nameLabel, nameInput,
        	    	orgLabel, orgInput,
        	    	emailLabel, emailInput,
        	    	reportLabel
        	    ]
        	}
        	initJTextArea(),
        	Tile{
        	    rows: 1, columns: 2
        	    hgap: 5, vgap: 3
        	    content: [
        	    	acceptButton,
        	    	requiredFieldsMessage
        	    ]
        	}
        ]
    }
    
    var window:PopupWindow = PopupWindow{
        id: id //Important, this id makes posible to close the PopupWindow
        width: bind content.boundsInLocal.width+50, height: bind content.boundsInLocal.height+50
    }
 
	public override function create(): Node {
		return Group {
			content: [
				window, content
			]
		};
	}
}
