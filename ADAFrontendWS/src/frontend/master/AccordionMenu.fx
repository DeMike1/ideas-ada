package frontend.master;
import frontend.DefaultBackground;
import frontend.PopupWindow;
import frontend.popupWindows.MessageWindow;
import frontend.Tooltip;
import java.lang.Exception;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.layout.Stack;
import javafx.scene.layout.VBox;
import javafx.scene.layout.ClipView;
import javafx.geometry.VPos;
import javafx.geometry.HPos;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.scene.input.MouseEvent;
import javafx.scene.Cursor;
import javafx.scene.paint.Color;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;
import javafx.animation.Interpolator;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.control.ScrollBar;
import javafx.util.Math;


/**
 * @author AJurado
 */
 
public class AccordionMenu extends CustomNode {
    
    def defaultWidth = 200.0;
    public var width: Number = defaultWidth;
    public var templates: AccordionItem[];
    public var offers: AccordionItem[];
    var titles = [
    	AccordionTitle{
    	    titleName: "Templates"
    	    items: bind templates
    	    //scaleX: bind scaleText
    	}
    	AccordionTitle{
    	    titleName: "Offers"
    	    items: bind offers
    	    //scaleX: bind scaleText
    	}
    ];
    var isVisible: Boolean = true on replace{
        if(isVisible){
            t.rate = -1;
            arrow.points = arrowClosePoints;
            tt.text = "Close Menu"
        }else{
            t.rate = 1;
            arrow.points = arrowOpenPoints;
            tt.text = "Open Menu"
        }
    }

	//var scaleText = 1.0;
    var t:Timeline = Timeline {
        keyFrames: [
        	KeyFrame{
        	    time: 0s
        	    canSkip: true
        	    values: [width=>defaultWidth]
        	}
            KeyFrame{
                time: 0.1s
                canSkip: true
                values: [width=>defaultWidth+25 tween Interpolator.EASEOUT]
            }
            //Daba problemas con el tama�o del texto de los mensajes cuando el men� estaba oculto
            /*KeyFrame{
                time: 0.45s
                canSkip: true
                action: function(){
                    if(isVisible){
                        scaleText = 1;
                    }else{
                        scaleText = 0.01;
                    }
                }
            }*/
            KeyFrame{
                time: 0.5s
                canSkip: true
                values: [width=>0 tween Interpolator.EASEIN]
            }
        ]
    }
   
    var arrow:Polygon;
    var arrowClosePoints = [
		0.0, 5.0,
		5.0, 0.0,
		5.0, 10.0
    ];
    var arrowOpenPoints = [
    	0.0, 0.0,
    	0.0, 10.0,
    	5.0, 5.0
    ];
    var tt:Tooltip = Tooltip{
        text: "Close Menu"
    }
    
    public function addTemplate(item:AccordionItem){
        var found:Boolean = false;
        for(t in templates){
            if(t.doc.docName == item.doc.docName){
                found = true;
                var puw = MessageWindow{
                    id: "error"
                	message: "File already opened";
                }
                PopupWindow.addPopupToScene(puw, scene);
                break;
            }
        }
        if(not found){
            insert item into templates;
        }
    }
    
    public function addOffer(item:AccordionItem){
        var found:Boolean = false;
        for(o in offers){
            if(o.doc.docName == item.doc.docName){
                found = true;
                var puw = MessageWindow{
                    id: "error"
                	message: "File already opened";                	
                }
                PopupWindow.addPopupToScene(puw, scene);
                break;
            }
        }
        if(not found){
            insert item into offers;
        }
    }
    
    public function searchAccordionItemByName(name:String):AccordionItem{
        var returnItem:AccordionItem;
        var found:Boolean = false;
        for(t in templates){
            if(t.doc.docName == name){
                returnItem = t;
                found = true;
                break;
            }
        }
        if(not found){
            for(o in offers){
                if(o.doc.docName == name){
                    returnItem = o;
                    found = true;
                    break;
                }
            }
        }
        return returnItem;
    }

    public override function create(): Node {
        var vBox:VBox;
        return Group {
            clip: Rectangle{
                width: bind width+10, height: bind vBox.boundsInLocal.height
            }
            content: [
            	vBox = VBox {
            	    width: bind width
            	    content: titles
            	}
            	Stack{
            	    layoutX: bind vBox.layoutBounds.width, layoutY: bind vBox.boundsInLocal.height/2-15
            		content: [
            			DefaultBackground{
		            	    width: 10, height: 30
		            	    onMouseEntered: function( e: MouseEvent ): Void {
		            	        cursor = Cursor.HAND;
		            	        tt.show(e.sceneX, e.sceneY);
		            	    }
		            	    onMouseExited: function( e: MouseEvent ): Void {
		            	        cursor = Cursor.DEFAULT;
		            	        tt.hide();
		            	    }
		            	    onMouseClicked: function( e: MouseEvent ): Void {
		            	        t.play();
		            			if(isVisible){
		            			    isVisible = false;
		            			}else{
		            			    isVisible = true;
		            			}
		            	    }
		            	}
		            	arrow = Polygon{
		            	    points: arrowClosePoints
		            	}
            		]
            	}
            ]
        };
    }
}

class AccordionTitle extends CustomNode{
    public var titleName: String;
    public var items: AccordionItem[];
    var height:Number = 200.0;
    var background: Node;
    var isExpanded:Boolean = true;
    
    function expand(){
        isExpanded = true;
        t.rate = -1;
        t.play();
    }
    
    function collapse(){
        isExpanded = false;
        t.rate = 1;
        t.play();
    }
    
    var t = Timeline{
        keyFrames: [
            KeyFrame{
                time: 0.5s,
                canSkip: true
                values: [
                	height=>0 tween Interpolator.EASEBOTH
                ]
            }
        ]
    }
    
    var accItems:VBox = VBox{
	    spacing: 2
	    nodeHPos: HPos.CENTER
	    hpos: HPos.CENTER
	    width: bind width
		content: bind items
	}
	
	var scrollBar:ScrollBar = ScrollBar{
 	    layoutX: bind width-13, layoutY: 20
 	    layoutInfo: LayoutInfo{
 	        managed: false
 	    }
 	    vertical: true
 	    height: bind height
 	    visible: bind height>20
 	    min: 0, max: bind Math.max((accItems.boundsInLocal.height - height), 0)
 	    unitIncrement: 10
 	    //disable: true
 	}
    
    public override function create(): Node{
        return VBox{
            content: [
        		Stack{
            		onMouseEntered: function( e: MouseEvent ): Void {
            			background.cursor = Cursor.HAND;
            		}
            		onMouseExited: function( e: MouseEvent ): Void {
            		    background.cursor = Cursor.DEFAULT;
            		}
            		onMouseClicked: function( e: MouseEvent ): Void {
            		    if(isExpanded){
            		        collapse();
            		    }else{
            		        expand();
            		    }
            		}
            		content: [
            			background = DefaultBackground{
               			 	width: bind width, height: 20
						}
               	 		Text{
               	 	    	content: titleName
               	 	    	wrappingWidth: width
               	 	    	//scaleX: bind scaleText
						}
					]
        		}
        		ClipView{
        		    clip: Rectangle{
	                    width: bind width, height: bind height
	                }
		            width: bind width, height: bind javafx.util.Math.max(height, 20);
		            clipY: bind scrollBar.value
		            node: accItems
		            pannable: false
		        }
	    		scrollBar
        	]
        }
    }
}