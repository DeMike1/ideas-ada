package frontend.master;
import frontend.ADADocument;
import frontend.Tooltip;
import frontend.PopupWindow;
import frontend.popupWindows.WaitWindow;
import javafx.scene.CustomNode;
import javafx.scene.Node;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Circle;
import javafx.scene.layout.Stack;
import javafx.scene.layout.LayoutInfo;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.geometry.HPos;


/**
 * @author AJurado
 */
 
public class AccordionItem extends CustomNode {
    
    /**
     * Document opened.
     */
    public var doc: ADADocument;
    
    /**
     * String type, template or offer
     */
    public var type:String;
    
    /**
     * 
     */
    public var openItem: function();
    
    /**
     * 
     */
    public var closeItem: function();
    
    /**
     * Width of this Item.
     */
    public var width:Number;
    
    /**
     * Item tooltip
     */
    var tt:Tooltip = Tooltip{}
    
    /**
	 * InnerShadow color on close tab button is binded to it. This creates a pressed
	 * button effect.
	 */    
	var shadowColor = Color.TRANSPARENT;
	
	var loadingWin = WaitWindow{
        id: "wait"
        message: "Loading, please wait"
        indicator: false
    }

    public override function create(): Node {
        var l:Label;
        var bg:Rectangle;
        var closeB:Circle;
        return Group{
        	content: [
        		Stack {
        		    nodeHPos: HPos.LEFT
		            content: [
		            	bg = Rectangle{
		            	    width: bind width, height: 20
		            	    fill: Color.TRANSPARENT
		            	    onMouseEntered: function( e: MouseEvent ): Void {
		            	        tt.text = doc.docName;
		            	        tt.show(e.sceneX+150, e.sceneY+30);
		            	    }
		            	    onMouseExited: function( e: MouseEvent ): Void {
		            	        tt.hide();
		            	    }
		            	    /*onMouseClicked: function( e: MouseEvent ): Void {
		            	        tt.hide();
		            	        openItem();
		            	    }*/
		            	    onMousePressed: function( e: MouseEvent ): Void {
		            	        tt.hide();
		            	        PopupWindow.addPopupToScene(loadingWin, scene);
		            	    }
		            	    onMouseReleased: function( e: MouseEvent ): Void {
		            	        PopupWindow.closeWindow("wait", scene);
		            	        openItem();
		            	    }
		            	}
		            	l = Label{
		            	    text: bind doc.docName
		            	    textOverrun: OverrunStyle.ELLIPSES;
		            	    width: bind width-30
		            	}
		            	closeB = Circle {
		            	    translateX: bind width-30
    	    				cache: true
    	    			    radius: 4
    	    			    fill: Color.RED
    	    			    onMouseEntered: function( e: MouseEvent ): Void {
    	    			    	closeB.cursor = Cursor.HAND;
    	    			    }
    	    			    onMousePressed: function( e: MouseEvent ): Void {
    	    					shadowColor = Color.BLACK;
    	    			    }
    	    			    onMouseReleased: function( e: MouseEvent ): Void {
    	    			    	shadowColor = Color.TRANSPARENT;
    	    			    }
    	    			    onMouseClicked: function( e: MouseEvent ): Void {
    	    			        tt.hide();
    	    			        closeItem();
    	    			    }
    	    			}
		            ]
		        }
        	]
        }
    }
}