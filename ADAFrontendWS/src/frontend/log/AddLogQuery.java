package frontend.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class AddLogQuery extends DBConnection{
	
	/**
	 * Operation
	 */
	private String op;
	
	/**
	 * First document name
	 */
	private String doc1Name;
	
	/**
	 * First document content
	 */
	private String doc1Content;
	
	/**
	 * First document is a default document
	 */
	private Boolean doc1Default;
	
	/**
	 * Second document name
	 */
	private String doc2Name;
	
	/**
	 * Second document content
	 */
	private String doc2Content;
	
	/**
	 * Second document is a default document
	 */
	private Boolean doc2Default;
	
	/**
	 * Console log after execute the operation
	 */
	private String consoleLog;
	
	/**
	 * ADA result after execute the operation
	 */
	private String resultADA;
	
	/**
	 * This session identifier
	 */
	private String sessionId;
	
	/**
	 * Info about the browser, operating system...
	 */
	private String browserInfo;

	public AddLogQuery(String op, Object[] doc1, Object[] doc2, String consoleLog, String resultADA, String sessionId, String browserInfo) {
		super();
		this.op = op;
		this.doc1Name = (String) doc1[0];
		this.doc1Content = (String) doc1[1];
		this.doc1Default = (Boolean) doc1[2];
		this.doc2Name = (String) doc2[0];
		this.doc2Content = (String) doc2[1];
		this.doc2Default = (Boolean) doc2[2];
		this.consoleLog = consoleLog;
		this.resultADA = resultADA;
		this.sessionId = sessionId;
		this.browserInfo = browserInfo;
	}

	protected void dbQuery(Connection c) throws SQLException {
		
		//Create PreparedStatement and ResultSet to get operation id
		PreparedStatement psGetOpId = c.prepareStatement("SELECT idOperaciones FROM operaciones WHERE nombre=?");
		ResultSet rsGetOpId;
		
		//Get operation id
		int operationId = 0;
		psGetOpId.setString(1, op);
		rsGetOpId = psGetOpId.executeQuery();
		
		if(rsGetOpId.next()){
			operationId = rsGetOpId.getInt("idOperaciones");
		}
		
		//Create PreparedStatement and ResultSet to insert documents. 1 = nombre, 2 = contenido
		PreparedStatement psInsertDocs = c.prepareStatement("INSERT INTO documentos VALUES (null, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
		ResultSet rsInsertDocs;
		
		//Create PreparedStatement and ResultSet to select documents id
		PreparedStatement psSelectDocsId = c.prepareStatement("SELECT idDocumentos FROM documentos WHERE nombre=?");
		ResultSet rsSelectDocsId;
		
		//Insert documents if necessary
		int doc1Id = 0;
		if(doc1Default){
			//Select first document id
			psSelectDocsId.setString(1, doc1Name);
			
			rsSelectDocsId = psSelectDocsId.executeQuery();
			
			if(rsSelectDocsId.next()){
				doc1Id = rsSelectDocsId.getInt(1);
			}
		}else{
			//Prepare and insert document1
			psInsertDocs.setString(1, doc1Name);
			psInsertDocs.setString(2, doc1Content);
			psInsertDocs.setInt(3, 1);
			
			//Execute insert
			psInsertDocs.executeUpdate();
			
			//Get inserted document id
			rsInsertDocs = psInsertDocs.getGeneratedKeys();
			if(rsInsertDocs.next()){
				doc1Id = rsInsertDocs.getInt(1);
			}
		}
		
		int doc2Id = 0;
		if(doc2Name != ""){
			if(doc2Default){
				//Select second document id
				psSelectDocsId.setString(1, doc2Name);
				
				rsSelectDocsId = psSelectDocsId.executeQuery();
				
				if(rsSelectDocsId.next()){
					doc2Id = rsSelectDocsId.getInt(1);
				}
			}else{
				//Prepare and insert document2
				psInsertDocs.setString(1, doc2Name);
				psInsertDocs.setString(2, doc2Content);
				psInsertDocs.setInt(3, 1);
				
				//Execute insert
				psInsertDocs.executeUpdate();
				
				//Get inserted document id
				rsInsertDocs = psInsertDocs.getGeneratedKeys();
				if(rsInsertDocs.next()){
					doc2Id = rsInsertDocs.getInt(1);
				}
			}
		}
		
		//Create PreparedStatement to insert log. 1=logConsola, 2=resultadoADA, 3=sessionId, 4=operacionId, 5=templateId, 6=offerId, 7=browserInfo
		PreparedStatement psInsertLog = c.prepareStatement("INSERT INTO logs VALUES (null, null, ?, ?, ?, ?, ?, ?, ?)");
		
		//Prepare and insert log
		psInsertLog.setString(1, consoleLog);
		psInsertLog.setString(2, resultADA);
		psInsertLog.setString(3, sessionId);
		psInsertLog.setInt(4, operationId);
		psInsertLog.setInt(5, doc1Id);
		if(doc2Id != 0){
			psInsertLog.setInt(6, doc2Id);
		}else{
			psInsertLog.setNull(6, Types.INTEGER);
		}
		psInsertLog.setString(7, browserInfo);
		
		psInsertLog.executeUpdate();		
	}
}
