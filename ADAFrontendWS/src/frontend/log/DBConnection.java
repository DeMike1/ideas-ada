package frontend.log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javafx.async.RunnableFuture;

public abstract class DBConnection implements RunnableFuture{
	
	/**
	 * Database user
	 */
	private String user = "ada.user";
	
	/**
	 * Database pass
	 */
	private String pass = "ada.pass.1";
	
	/**
	 * Database URL
	 */
	private String jdbcURL = "jdbc:mysql://193.147.175.250:3306/ada?autoReconnect=true";
	
	/**
	 * Database driver class name
	 */
	private String driverClassName = "com.mysql.jdbc.Driver";
	
	/**
	 * Connection
	 */
	private Connection conn = null;
	
	public DBConnection(){}
	
	/**
	 * This query will be executed
	 * @param c
	 * @throws SQLException
	 */
	protected abstract void dbQuery(Connection c) throws SQLException;

	/**
	 * Asynchronous task, it runs in a different thread
	 */
	public void run() throws Exception{
		
		try{
			//Load driver
			Class.forName(driverClassName);
			
			//Get connection
			conn = DriverManager.getConnection(jdbcURL, user, pass);
			
			//Execute query
			dbQuery(conn);
			
			
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			if(conn != null){
				//Close connection
				conn.close();
			}
		}
		
	}
}
