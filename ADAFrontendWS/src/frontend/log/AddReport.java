package frontend.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;

public class AddReport extends DBConnection{

	/**
	 * report text
	 */
	private String text;
	
	/**
	 * Username
	 */
	private String username;
	
//	/**
//	 * User organization
//	 */
//	private String org;
	
	/**
	 * User email
	 */
	private String userEmail;
	
	/**
	 * sessionId
	 */
	private String sessionId;
	
	public AddReport(String sessionId, String text, String username, String org, String userEmail){
		super();
		this.sessionId = sessionId;
		this.text = text;
		this.username = username;
//		this.org = org;
		this.userEmail = userEmail;
	}
	
	protected void dbQuery(Connection c) throws SQLException {
		
		//Create PreparedStatement and ResultSet to select users id. 1=nombre, 2=email
//		PreparedStatement psSelectUser = c.prepareStatement("SELECT idUsuario FROM usuarios WHERE Nombre=? AND Email=?");
//		ResultSet rsSelectUser;
		
		//Create PreparedStatement and ResultSet to insert users. 1=nombre, 2=organizacion, 3=email
//		PreparedStatement psInsertUser = c.prepareStatement("INSERT INTO usuarios VALUES (null, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
//		ResultSet rsInsertUser;
		
		//Create PreparedStatement to insert reports. 1=sessionId, 2=nombre de usuario, 3=email, 4=reporte
		PreparedStatement psInsertReport = c.prepareStatement("INSERT INTO reportes VALUES (null, ?, ?, ?)");
		
		//Check if user already exists
//		int userId = 0;
//		psSelectUser.setString(1, username);
//		psSelectUser.setString(2, userEmail);
//		rsSelectUser = psSelectUser.executeQuery();
//		if(rsSelectUser.next()){
//			userId = rsSelectUser.getInt(1);
//		}
		
		//If exists
//		if(userId != 0){
			//Insert report with existing user information
			psInsertReport.setString(1, sessionId);
			psInsertReport.setString(2, username);
			psInsertReport.setString(3, userEmail);
			psInsertReport.setString(4, text);
			
			psInsertReport.executeUpdate();
//		}else{//if not exist
//			//Insert user information and get inserted user id
//			psInsertUser.setString(1, username);
//			psInsertUser.setString(2, org);
//			psInsertUser.setString(3, userEmail);
//			
//			psInsertUser.executeUpdate();
//			
//			rsInsertUser = psInsertUser.getGeneratedKeys();
//			
//			if(rsInsertUser.next()){
//				userId = rsInsertUser.getInt(1);
//			}
//			
//			//Insert report
//			psInsertReport.setString(1, sessionId);
//			psInsertReport.setInt(2, userId);
//			psInsertReport.setString(3, text);
//			
//			psInsertReport.executeUpdate();
//		}
	}
}
