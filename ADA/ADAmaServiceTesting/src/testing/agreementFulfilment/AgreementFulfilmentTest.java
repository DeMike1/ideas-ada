package testing.agreementFulfilment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;

import javax.xml.rpc.ServiceException;

import es.us.isa.ada.service.ADAManagerServiceLocator;
import es.us.isa.ada.service.ADAManagerServicePortType;
import es.us.isa.ada.service.ADAServiceWithRepoLocator;
import es.us.isa.ada.service.ADAServiceWithRepoPortType;

public class AgreementFulfilmentTest {

	private static ADAManagerServicePortType adama;
	
	private static ADAServiceWithRepoPortType adarepo;
	
	private static final String FILE_PATH = "resources/agreementFixedValue.wsag";
	
	public static void main(String[] args){
		try{
			//Creamos las instancias para comunicar con los servicios
			adama = new ADAManagerServiceLocator().getADAManagerServicePort();
			adarepo = new ADAServiceWithRepoLocator().getADAServiceWithRepoPort();
			
			//Abrimos sesi�n en ADAma con usuario y password
			String session = adama.login("upc.user", "upc.pass");
			
			//Leemos en contenido del documento y lo guardamos en el repositorio
			//No se pueden guardar documentos cuyo nombre coincida con uno existente
			String docContent = readFileAsString(FILE_PATH);
			Integer documentId = adama.storeAgreement("NombreDelDocumento.wsag", docContent, session);
			
			//Obtenemos el MMD (sin medidas) del documento WS-Ag
			String mmd = adama.getMMD(documentId, session);
			
			//TODO Modificamos el MMD para a�adir medidas
			//Seg�n el documento WS-Ag de prueba, si a�adimos una medida
			//de DemandedTranslationTime entre 1 y 55 la operaci�n devolver� true,
			//si est� fuera de ese rango devolver� false
			
			//Ejecutamos la operaci�n AgreementFulfilment con el MMD
			Boolean isFulfiled = adarepo.isFulfilled(documentId, mmd.toString().getBytes());
			
			System.out.println("�Se est� cumpliendo el acuerdo? "+isFulfiled);
			
			//Si queremos volver a probar con este c�digo, borramos el documento
			//del repositorio para que al volver a guardar no de problemas
			adama.deleteAgreement(documentId, session);
			adama.logout(session);
		}catch(ServiceException e){
			e.printStackTrace();
		}catch(RemoteException e){
			e.printStackTrace();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	/*
	 * Usamos este m�todo auxiliar para leer el documento como String
	 */
	private static String readFileAsString(String filePath){
		String result = null;
		try {
			FileInputStream file = new FileInputStream(filePath);
			byte[] b = new byte[file.available ()];
			file.read(b);
			file.close ();
			result = new String (b);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return result;
	}
}
