package testing.loginAndLogout;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.us.isa.ada.service.ADAManagerServiceLocator;
import es.us.isa.ada.service.ADAManagerServicePortType;
import es.us.isa.ada.service.ADAManagerServiceSoapBindingStub;

public class LoginTest {
	
	private ADAManagerServicePortType ada;

	@Before
	public void setUp(){
		try {
			ada = new ADAManagerServiceLocator().getADAManagerServicePort(new URL("http://www.isa.us.es:8084/ADAma"));
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test(){
		try {
			String session = ada.login("Antonio", "ajurado");
			System.out.println(session);
			ada.logout(session);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown(){
		
	}
}
