/**
 * ADAManagerServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package es.us.isa.ada.service;

public interface ADAManagerServicePortType extends java.rmi.Remote {
    public int storeAgreement(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException, java.sql.SQLException;
    public es.us.isa.ada.service.Int2DocumentProxyMapEntry[] getAgreementsByUser(java.lang.String arg0) throws java.rmi.RemoteException, java.sql.SQLException;
    public int getUserId(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean logout(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean updateAgreement(int arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException, java.sql.SQLException;
    public byte[] documentId2String(int arg0) throws java.rmi.RemoteException, java.sql.SQLException;
    public java.lang.String getMMD(int arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean deleteAgreement(int arg0, java.lang.String arg1) throws java.rmi.RemoteException, java.sql.SQLException;
    public java.lang.String login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public es.us.isa.ada.service.Int2DocumentProxyMapEntry[] getPublicAgreements() throws java.rmi.RemoteException, java.sql.SQLException;
}
