/**
 * ADAManagerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package es.us.isa.ada.service;

public class ADAManagerServiceLocator extends org.apache.axis.client.Service implements es.us.isa.ada.service.ADAManagerService {

    public ADAManagerServiceLocator() {
    }


    public ADAManagerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ADAManagerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ADAManagerServicePort
    private java.lang.String ADAManagerServicePort_address = "http://localhost:8082/ADAma";

    public java.lang.String getADAManagerServicePortAddress() {
        return ADAManagerServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ADAManagerServicePortWSDDServiceName = "ADAManagerServicePort";

    public java.lang.String getADAManagerServicePortWSDDServiceName() {
        return ADAManagerServicePortWSDDServiceName;
    }

    public void setADAManagerServicePortWSDDServiceName(java.lang.String name) {
        ADAManagerServicePortWSDDServiceName = name;
    }

    public es.us.isa.ada.service.ADAManagerServicePortType getADAManagerServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ADAManagerServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getADAManagerServicePort(endpoint);
    }

    public es.us.isa.ada.service.ADAManagerServicePortType getADAManagerServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            es.us.isa.ada.service.ADAManagerServiceSoapBindingStub _stub = new es.us.isa.ada.service.ADAManagerServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getADAManagerServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setADAManagerServicePortEndpointAddress(java.lang.String address) {
        ADAManagerServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (es.us.isa.ada.service.ADAManagerServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                es.us.isa.ada.service.ADAManagerServiceSoapBindingStub _stub = new es.us.isa.ada.service.ADAManagerServiceSoapBindingStub(new java.net.URL(ADAManagerServicePort_address), this);
                _stub.setPortName(getADAManagerServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ADAManagerServicePort".equals(inputPortName)) {
            return getADAManagerServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.ada.isa.us.es/", "ADAManagerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.ada.isa.us.es/", "ADAManagerServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ADAManagerServicePort".equals(portName)) {
            setADAManagerServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
