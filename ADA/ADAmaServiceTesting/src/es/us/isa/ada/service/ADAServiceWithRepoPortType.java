/**
 * ADAServiceWithRepoPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package es.us.isa.ada.service;

public interface ADAServiceWithRepoPortType extends java.rmi.Remote {
    public es.us.isa.ada.service.Term2ArrayOfAgreementElementMapEntry[] explainLudicrousTerms(int arg0, es.us.isa.ada.wsag10.Term[] arg1) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public es.us.isa.ada.wsag10.Term[] getLudicrousTerms(int arg0) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public es.us.isa.ada.service.AgreementElement2ArrayOfAgreementElementMapEntry[] explainInconsistencies(int arg0) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public es.us.isa.ada.wsag10.Term[] getDeadTerms(int arg0) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public boolean checkDocumentConsistency(int arg0) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public es.us.isa.ada.service.AgreementError2ExplanationMapEntry[] explainNonCompliance(int arg0, int arg1) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public byte[] wsag4PeopleToXML(byte[] arg0) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public byte[] getMetricFile(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean isFulfilled(int arg0, byte[] arg1) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public boolean isCompliant(int arg0, int arg1) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public java.lang.String addMetricFile(byte[] arg0) throws java.rmi.RemoteException;
    public es.us.isa.ada.service.Term2ArrayOfAgreementElementMapEntry[] explainDeadTerms(int arg0, es.us.isa.ada.wsag10.Term[] arg1) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public es.us.isa.ada.service.AgreementError2ExplanationMapEntry[] explainAgreementViolation(int arg0, byte[] arg1) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
    public byte[] xmlToWSAg4People(byte[] arg0) throws java.rmi.RemoteException, es.us.isa.ada.exceptions.BadSyntaxException;
}
