/**
 * ADAManagerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package es.us.isa.ada.service;

public interface ADAManagerService extends javax.xml.rpc.Service {
    public java.lang.String getADAManagerServicePortAddress();

    public es.us.isa.ada.service.ADAManagerServicePortType getADAManagerServicePort() throws javax.xml.rpc.ServiceException;

    public es.us.isa.ada.service.ADAManagerServicePortType getADAManagerServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
