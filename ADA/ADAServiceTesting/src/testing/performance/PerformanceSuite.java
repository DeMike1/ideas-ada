package testing.performance;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import testing.performance.operations.MeasurableCompliance;
import testing.performance.operations.MeasurableConsistency;
import testing.performance.operations.MeasurableDeadTerms;
import testing.performance.operations.MeasurableExplainCompliance;
import testing.performance.operations.MeasurableExplainConsistency;
import testing.performance.operations.MeasurableExplainDeadTerms;
import testing.performance.operations.MeasurableExplainLudicrousTerms;
import testing.performance.operations.MeasurableGetMetric;
import testing.performance.operations.MeasurableLudicrousTerms;
import testing.performance.operations.MeasurableOperation;
import testing.performance.operations.MeasurableTransformationTo4People;
import testing.performance.operations.MeasurableTransformationToXml;

public class PerformanceSuite {
	
	private static final String templatesPath = "frontendDefaultDocuments/templates/";
	private static final String offersPath = "frontendDefaultDocuments/offers/";
	
	private static Map<String, String> templates = new HashMap<String, String>();
	private static Map<String, String> offers = new HashMap<String, String>();
	
	public static void main(String[] args){
		Boolean offersAnalised = false;
		loadDocuments();
		
//		getMetric("metrics/metric2575304161886105.xml", "");
		//Por cada plantilla llamar a las operaciones de un documento
		for(Entry<String, String> t:templates.entrySet()){
//			consistency(t.getKey(), t.getValue());
//			explainConsistency(t.getKey(), t.getValue());
//			deadTerms(t.getKey(), t.getValue());
//			ludicrousTerms(t.getKey(), t.getValue());
//			explainDeadTerms(t.getKey(), t.getValue());
//			explainLudicrousTerms(t.getKey(), t.getValue());
//			fulfillment(t.getKey(), t.getValue());
//			violationExplanation(t.getKey(), t.getValue());
//			decomposeIntoViews(t.getKey(), t.getValue());
//			xmlToWsag4People(t.getKey(), t.getValue());
//			wsag4PeopleToXml(t.getKey(), t.getValue());
			
//		    Por cada oferta llamar a las operaciones de dos documentos
			for(Entry<String, String> o:offers.entrySet()){
//			    Si offersAnalised == false -> llamar a las de un documento
				if(!offersAnalised){
//					consistency(o.getKey(), o.getValue());
//					explainConsistency(o.getKey(), o.getValue());
//					deadTerms(o.getKey(), o.getValue());
//					ludicrousTerms(o.getKey(), o.getValue());
//					explainDeadTerms(o.getKey(), o.getValue());
//					explainLudicrousTerms(o.getKey(), o.getValue());
//					fulfillment(o.getKey(), o.getValue());
//					violationExplanation(o.getKey(), o.getValue());
//					decomposeIntoViews(o.getKey(), o.getValue());
//					xmlToWsag4People(o.getKey(), o.getValue());
//					wsag4PeopleToXml(o.getKey(), o.getValue());
				}// Sino nada
//				compliance(t.getKey(), t.getValue(), o.getKey(), o.getValue());
//				explainCompliance(t.getKey(), t.getValue(), o.getKey(), o.getValue());
			}
			offersAnalised = true;
		}
	}

	private static void loadDocuments(){
		File f;
		File[] subfiles;
		
		f = new File(templatesPath);
		subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			String name = subfiles[i].getName();
			if (!path.endsWith(".svn")){
				templates.put(name, path);
			}
		}
		
		f = new File(offersPath);
		subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			String name = subfiles[i].getName();
			if (!path.endsWith(".svn")){
				offers.put(name, path);
			}
		}
	}
	
	private static void consistency(String docName, String docPath){
		MeasurableOperation op = new MeasurableConsistency(docName, docPath, "CheckConsistency");
		System.out.println("Vamos a comprobar consistencia de: "+docName);
		op.execute();
	}
	
	private static void compliance(String templateName, String templatePath, String offerName, String offerPath){
//		for(Entry<String, String> t:templates.entrySet()){
//			for(Entry<String, String> o:offers.entrySet()){
//				if(isNotErrorCase(t.getKey(), o.getKey())){
					MeasurableOperation op = new MeasurableCompliance(templateName, templatePath, offerName, offerPath, "CheckCompliance");
					System.out.println("Vamos a comprobar conformidad entre: "+templateName+" y "+offerName);
					op.execute();
//				}else{
//					System.out.println("Saltando caso: "+t.getKey()+" y "+o.getKey());
//				}
//			}
//		}
	}
	
	private static void explainConsistency(String documentName, String documentPath) {
		MeasurableOperation op = new MeasurableExplainConsistency(documentName, documentPath, "ExplainInconsistencies");
		System.out.println("Vamos a explicar inconsistencias de: "+documentName);
		op.execute();
	}
	
	private static void explainCompliance(String templateName, String templatePath, String offerName, String offerPath) {
		MeasurableOperation op = new MeasurableExplainCompliance(templateName, templatePath, offerName, offerPath, "ExplainNonCompliance");
		System.out.println("Vamos a explicar la disconformidad entre: "+templateName+" y "+offerName);
		op.execute();
	}
	
	private static void deadTerms(String docName, String docPath){
		MeasurableOperation op = new MeasurableDeadTerms(docName, docPath, "DeadTerms");
		System.out.println("Vamos a comprobar DeadTerms de: "+docName);
		op.execute();
	}
	
	private static void ludicrousTerms(String docName, String docPath){
		MeasurableOperation op = new MeasurableLudicrousTerms(docName, docPath, "LudicrousTerms");
		System.out.println("Vamos a comprobar LudicrousTerms de: "+docName);
		op.execute();
	}
	
	private static void explainDeadTerms(String docName, String docPath){
		MeasurableOperation op = new MeasurableExplainDeadTerms(docName, docPath, "ExplainDeadTerms");
		System.out.println("Vamos a explicar los DeadTerms: "+docName);
		op.execute();
	}
	
	private static void explainLudicrousTerms(String docName, String docPath){
		MeasurableOperation op = new MeasurableExplainLudicrousTerms(docName, docPath, "ExplainLudicrousTerms");
		System.out.println("Vamos a explicar los LudicrousTerms: "+docName);
		op.execute();
	}
	
	private static void xmlToWsag4People(String documentName, String documentPath){
		MeasurableOperation op = new MeasurableTransformationTo4People(documentName, documentPath, "XmlToWsag4People");
		System.out.println("Vamos a transformar a 4People: "+documentName);
		op.execute();
	}
	
	private static void wsag4PeopleToXml(String documentName, String documentPath){
		MeasurableOperation op = new MeasurableTransformationToXml(documentName, documentPath, "Wsag4PeopleToXml");
		System.out.println("Vamos a transformar a Xml: "+documentName);
		op.execute();
	}
	
	private static void getMetric(String documentName, String documentPath){
		MeasurableOperation op = new MeasurableGetMetric(documentName, documentPath, "GetMetric()");
		System.out.println("Vamos a hacer getMetric()");
		op.execute();
	}
	
//	private static Boolean isNotErrorCase(String doc){
//		Boolean result = false;
//		if(!doc.equalsIgnoreCase("DeadTerm(Xor).wsag")){
//			result = true;
//		}
//		return result;
//	}
//	
//	private static Boolean isNotErrorCase(String template, String offer){
//		Boolean result = false;
//		
//		if(!template.equalsIgnoreCase("Template(term_errors_by_several_GTs_and_CCs).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag")){
//			if(!template.equalsIgnoreCase("Template(term_errors_by_several_GTs_and_CCs).wsag") || !offer.equalsIgnoreCase("AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag")){
//				if(!template.equalsIgnoreCase("Template(term_errors_by_SDT_and_CC_with_themselves).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag")){
//					if(!template.equalsIgnoreCase("ConsistentTemplate.wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag")){
//						if(!template.equalsIgnoreCase("Template(term_errors_by_several_GTs_and_CCs).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag")){
//							if (!template.equalsIgnoreCase("Template(term_errors_by_SDT_and_CC_with_themselves).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag")) {
//								if (!template.equalsIgnoreCase("ConsistentTemplate.wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag")) {
//									if (!template.equalsIgnoreCase("Template(warning_by_SDT_CC).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag")) {
//										if (!template.equalsIgnoreCase("Template(warning_by_SDT_CC).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag")) {
//											if (!template.equalsIgnoreCase("Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag") || !offer.equalsIgnoreCase("AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag")) {
//												if (!template.equalsIgnoreCase("Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag")) {
//													if (!template.equalsIgnoreCase("Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag") || !offer.equalsIgnoreCase("ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag")) {
//														result = true;
//													}
//												}
//											}
//										}
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		return result;
//	}
}
