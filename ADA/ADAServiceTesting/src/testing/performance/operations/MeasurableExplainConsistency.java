package testing.performance.operations;

import java.rmi.RemoteException;

public class MeasurableExplainConsistency extends MeasurableOneDocumentOperation{

	public MeasurableExplainConsistency(String documentName, String documentPath, String operationName) {
		super(documentName, documentPath, operationName);
	}

	protected void operationToBeMeasured() {
		try{
			ada.explainInconsistencies(super.getDocumentContent().getBytes());
		}catch(RemoteException e){
			System.out.println("Error en la explicación de la consistencia");
			e.printStackTrace();
		}
	}

}
