package testing.performance.operations;

import java.rmi.RemoteException;

import es.us.isa.ada.wsag10.Term;

public class MeasurableExplainLudicrousTerms extends MeasurableOneDocumentOperation{
	
	private Term[] terms;

	public MeasurableExplainLudicrousTerms(String documentName, String documentPath, String operationName) {
		super(documentName, documentPath, operationName);
	}
	
	protected void initialize(){
		super.initialize();
		try{
			terms = ada.getLudicrousTerms(super.getDocumentContent().getBytes());
		}catch(RemoteException e){
			e.printStackTrace();
		}
	}

	protected void operationToBeMeasured() {
		try{
			ada.explainLudicrousTerms(super.getDocumentContent().getBytes(), terms);
		}catch(RemoteException e){
			System.out.println("Error en la explicación de LudicrousTerms");
			e.printStackTrace();
		}
	}
}
