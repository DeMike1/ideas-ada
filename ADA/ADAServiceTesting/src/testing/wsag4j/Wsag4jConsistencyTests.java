package testing.wsag4j;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.rpc.ServiceException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;

@RunWith(Parameterized.class)
public class Wsag4jConsistencyTests {

	private ADAServiceV2PortType ada;
	
	private String path;
	private Boolean res;
	
	public Wsag4jConsistencyTests(String path, Boolean res){
		this.path = path;
		this.res = res;
	}
	
	@Before
	public void setUp(){
		try{
			ada = new ADAServiceV2Locator().getADAServiceV2Port(new URL("http://localhost:8081/ADAService"));
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void test1(){
		runConsistency(path, res);
	}
	
	private void runConsistency(String path, Boolean res){
		Boolean isConsistent = null;
		try {
			String doc = FileUtils.readFileToString(new File(path));
			isConsistent = ada.checkDocumentConsistency(doc.getBytes());
		} catch (RemoteException e) {
			System.out.println("Error en "+path);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(res, isConsistent);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
		
		File f = new File("Documents/wsag4j/offers");
		File[] subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			if (!path.endsWith(".svn")){
				Object[] aux = {path, Boolean.TRUE};
				res.add(aux);
			}
		}
		
		f = new File("Documents/wsag4j/templates");
		subfiles = f.listFiles();
		for (int i = 0; i < subfiles.length; i++){
			String path = subfiles[i].getPath();
			if (!path.endsWith(".svn")){
				Object[] aux = {path, Boolean.TRUE};
				res.add(aux);
			}
		}
		
		return res;
	}
	
}
