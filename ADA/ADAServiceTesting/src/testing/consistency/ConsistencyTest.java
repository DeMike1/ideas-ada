package testing.consistency;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.rpc.ServiceException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;

@RunWith(Parameterized.class)
public class ConsistencyTest {
	
	private ADAServiceV2PortType ada;
	
	private String document;
	private String docName;
	private Boolean expectedResult;
	
	public ConsistencyTest(String document, String docName, Boolean expectedResult){
		this.document = document;
		this.docName = docName;
		this.expectedResult = expectedResult;
		try{
			ada = new ADAServiceV2Locator().getADAServiceV2Port(new URL("http://localhost:8081/ADAService"));
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch(ServiceException e){
			e.printStackTrace();
		}
	}
	
	@Before
	public void setUp(){
		
	}

	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> params = new LinkedList<Object[]>();
		
		File trueTestsPath = new File("Documents\\Consistency\\True\\");
		File[] trueTests = trueTestsPath.listFiles();
		for(File f:trueTests){
			String path = f.getPath();
			if(!path.endsWith(".svn")){
				try{
					String doc = FileUtils.readFileToString(f);
					Object[] o = new Object[]{doc, path, Boolean.TRUE};
					params.add(o);
				}catch (IOException e){
					e.printStackTrace();
				}
			}
		}
		
		File falseTestsPath = new File("Documents\\Consistency\\False\\");
		File[] falseTests = falseTestsPath.listFiles();
		for(File f:falseTests){
			String path = f.getPath();
			if(!path.endsWith(".svn")){
				try{
					String doc = FileUtils.readFileToString(f);
					Object[] o = new Object[]{doc, path, Boolean.FALSE};
					params.add(o);
				}catch (IOException e){
					e.printStackTrace();
				}
			}
		}
		
		return params;
	}
	
	@Test
	public void test(){
		Boolean result = null;
		try {
			result = ada.checkDocumentConsistency(document.getBytes());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		System.out.println("Result: "+result+", Expected: "+expectedResult+" - Document: "+docName);
		assertEquals(expectedResult, result);
	}
}
