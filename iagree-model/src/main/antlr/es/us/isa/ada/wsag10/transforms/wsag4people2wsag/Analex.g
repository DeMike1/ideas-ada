header{
	package es.us.isa.ada.wsag10.transforms.wsag4people2wsag;	
}
class Analex extends Lexer;

options{
	// Importacin del vocabulario de tokens desde el analizador
	// sintctico (Anasint.g)
 	importVocab=Anasint;
 	
	// Por defecto no se activa la comprobacin de literales
	// declarados en la seccin tokens
	testLiterals=false;
	
	//2 smbolos de anticipacin para tomar decisiones
	//(los tokens DP y ASIG justifican su necesidad)
	k=3;
}

tokens{	
	// Palabras reservas (unidad de programa)
	MODULO="modulo";

	
	
	//Tokens de AGrement
	TEMPLATE = "Template";
	AGREEMENTOFFER = "AgreementOffer";
	INITIATOR = "Initiator";
	RESPONDER = "Responder";
	SERVICEPROVIDER = "ServiceProvider";
	EXPIRATIONTIME = "ExpirationTime";
	TEMPLATEID = "TemplateId";
	TEMPLATENAME = "TemplateName";
	CREATION = "Creation";
	CONSTRAINT = "Constraints";
	ITEMS = "Items";
	VALUE = "Value";
	OF = "Of";
	IS = "is";
	INTEGER = "integer";
	FLOAT = "float";
	ENUM = "enum";
	IF = "If";
	GUARANTEE = "Guarantee";
	GUARANTEES = "Guarantees";
	SERVICE = "Service";
	PROPERTIES = "Properties";
	REFERENCES = "References";
	REFERENCE = "Reference";
	DESCRIPTION = "Description";
	TERMS = "Terms";
	BY = "by";
	//SERVICEPROVIDER ="ServiceProvider";
	SERVICECONSUMER = "ServiceConsumer";
	TERM = "Term";
	MEASURED = "measured";
	RELATED = "related";
	TO = "to";
	ALL = "All";
	EXACTLY = "Exactly";
	ONE = "One";
	BETWEEN = "Between";
	OR = "Or";
	MORE = "More";
	//SERVICEPROPERTIES ="ServiceProperties";
	SCOPES = "Scopes";
	QUALIFYING = "Qualifying";
	CONDITION = "Condition";
	SLO = "SLO";
	BUSINESS ="Business";
	RELATIVE = "Relative";
	IMPORTANCE = "importance";
	BETWEENMINUS = "between";
	GUARANTEESMINUS = "guarantees";
	PENALTY = "Penalty";
	REWARD = "Reward";
	UTILITY = "Utility";
	IFMINUS = "if";
	SELECTED = "selected";
	CUSTOM = "Custom";
	KPI = "KPI";
	ASSESSED = "Assessed";
	IN = "in";
	INVOCATION = "invocation";
	XS = "xs";
	
	
	

}



	

	////////////////////
	//ATTR options {testLiterals=true;}: (LETRA|DIGITO)*;
	//ATTR : ((('a'..'z')|('A'..'Z'))|('0'..'9')|('.'))+;
	
	
	
	
	
	
	
	///////////////////

	//Tokens intiles para el anlisis sintctico
	//(B)lancos y (T)abuladores
	//BT : (' '|'\t') {$setType(Token.SKIP);} ;
	//(S)altos de (L)inea
	//SL : ("\r\n" | "\n"){newline();$setType(Token.SKIP);} ;
	protected SALTO: ("\r\n" | '\n') {newline();};
	BLANCO: (SALTO | ' ' | '\t') {$setType(Token.SKIP);};
	
	// Comentario de lnea
	COMENT_LIN: "//" ( ~('\n'))* ('\n') {newline();$setType(Token.SKIP);} ;
	//COMENT_LIN: "//" (('\r')+ ~('\n') | ~('\r') )* "\r\n" {newline();$setType(Token.SKIP);} ;

	// Signos de puntuacin
	DOS_PUNTOS : ":"; //(D)os (P)untos
	PARENTESIS_ABIERTO : '('; // (P)arentesis (A)bierto
	PARENTESIS_CERRADO : ')'; // (P)arentesis (C)errado
	LLAVE_ABIERTA: "{"; // (LL)ave (A)bierta
	LLAVE_CERRADA: "}"; // (LL)ave (C)errada
	CORCHETE_ABIERTO: "["; // (COR)chete (A)bierto
	CORCHETE_CERRADO: "]"; // (COR)chete (C)errado
	COMA: ","; // (CO)ma
	PUNTO_Y_COMA: ";"; // (PU)nto y (C)oma
	PUNTO:"."; // (PU)nto
	//TODO: ~(COMA|PUNTO|PUNTO_Y_COMA);
	IMPLICACION:"->";
	
	// Operadores aritmticos
	MAS: '+';
	MENOS: '-';
	POR: "*";
	DIVISION: "/";
	DIVISION2: "\\";
	ALMOHADILLA: "#";
	

	// Operadores relacionales
	MENOR:"<";
	MENOR_IGUAL:"<=";
	MAYOR:">";
	MAYOR_IGUAL:">=";
	IGUAL: "=";
	EXCLAMACION: "!";
	DISTINTO: "!=";
	MENOR_BARRA:"</";
	DTD: "dtd";

	// Asignacin
	ASIGNACION : ":=" ;

	// Lexemas auxiliares
	protected DIGITO: ('0'..'9');
	protected LETRA: ('a'..'z'|'A'..'Z');

	//SECCION: (NUMERO PUNTO)+ ' ';

	// Literales Enteros y Reales
	NUMERO : ((DIGITO)+ PUNTO) => (DIGITO)+ PUNTO (DIGITO)+ {$setType(LIT_REAL);}
		| ((DIGITO)+) => (DIGITO)+ {$setType(LIT_ENTERO);}
		;
		

	
	//Literales Carcter
	//LIT_CAR: '\''! (~('\''|'\n'|'\r'|'\t')) '\''!;

	// Lexema IDENT (Identificadores)
	// Se activa la comprobacin de palabras reservadas.
	// Las palabras reservadas tienen preferencia a cualquier otro identificador.
	IDENT options {testLiterals=true;}: ('_'|LETRA)('_'|LETRA|DIGITO)* ;
	
		DIRECCION: (("/") IDENT)+;
	//ATTR options {testLiterals=true;}: (LETRA)*; 
	