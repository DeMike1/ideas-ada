header{
	package es.us.isa.ada.wsag10.transforms.wsag4people2wsag;
	import java.util.*;
	import antlr.*;
	
}

class Anasint extends Parser;

options{
	buildAST=true;
	k=3;
}

tokens{
	LIT_ENTERO;
	LIT_REAL;
	PROGRAMA;
	VISIBLE;
	OCULTO;
	NO_INST;
	ATRIBUTO;
	METODO;
	PROTOTIPO;
	PARAMETRO;
	PARAMETROS;
	EXPRESIONES;
	RESULTADO;
	DEFINICION;
	VACIO;
    VARIABLE_LOCAL;
	VARIABLES_LOCALES;
	INSTRUCCION;
	INSTRUCCIONES;
	MENOSUNARIO;
	LLAMADA;
	ACCESO_TABLA;
	ACCESO_OBJETO;
	ACCESO_SIMPLE;
	ACCESO_PREDEFINIDO;
	LISTA_ENTEROS;
	
}

{
	
//Variables globales
	String resultado = "";
	String cabecera = "";
	String pie = "";
	int numTab = 3;
	String numero = "";
	LinkedList<Item> listaItems = new LinkedList<Item>();
	Item itemActual = new Item();
	String contenido = "";
	String nombre = "";
	String prefijo = "";
	String tipoConstraint = "";
	String acumuladorDeTexto = "";
	String acumulador1 = "";
	String acumulador2 = "";
	String acumulador3 = "";
	
	
	public String eliminaMayorYMenor(String texto){
		String stringAdaptado ="";
		for(int i = 0; i <texto.length();i++){
			if(texto.charAt(i) == '>'){
				stringAdaptado+= "&gt;";
			}else if(texto.charAt(i) == '<'){
				stringAdaptado+="&lt;";
				
			}else
				stringAdaptado+=texto.charAt(i);
			
		}
		return stringAdaptado;
	}
	
	public String eliminaEspaciosTexto(String texto){
	String resultado ="";
	
	for(int i = 0; i <texto.length();i++){
		if(texto.charAt(i) != ' ')
			resultado+=texto.charAt(i);
		
	}
	return resultado;	
		
		
	}
	
}
 

declaracion_modulo! returns [boolean error = false] : 
	{
		 resultado += "<?xml version=\"1.0\" encoding = \"UTF-8\"?>\n";
		 
	

	}
   d:documentowsag EOF
   {
   	
   	//System.out.println("RESULTADO:\n"+resultado);
   	WSAgForPeopleToXML.setResultado(resultado);
   }
   ;
 

nombre_modulo: MODULO! IDENT;


 	
documentowsag: context;

context:   inicio 
{
	resultado+=cabecera;
}
contextItems terms  (creationConstraints)?
		
{
	resultado+=pie;	
}
;

inicio:
{
	acumulador1="";
	acumulador2="";
}   
   k:tipoInicio (texao:texto_ao_ac {acumulador1+=#texao+" ";})+ MENOS (tex:texto_ao_ac{acumulador2+=#tex+" ";})+ DOS_PUNTOS
{AST s = #k;

String t = s.getText();
	if(t.equalsIgnoreCase("Template")){
		cabecera+= "<wsag:Template wsag:TemplateId=\""+ acumulador1+" \" \n xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  \n xmlns:wsag=\"http://schemas.ggf.org/graap/2007/03/ws-agreement\" \n xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" \n xsi:schemaLocation=\"http://schemas.ggf.org/graap/2007/03/wsagreement\" \n agreement_types.xsd=\"http://www.w3.org/2001/XMLSchema XMLSchema.xsd\">";
		pie="\n</wsag:Template>";
		
	}else{
		cabecera+= "<wsag:AgreementOffer wsag:AgreementId=\""+acumulador1+"\" \n xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n xmlns:wsag=\"http://schemas.ggf.org/graap/2007/03/ws-agreement\" \n xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" \n xsi:schemaLocation=\"http://schemas.ggf.org/graap/2007/03/wsagreement\" \n agreement_types.xsd=\"http://www.w3.org/2001/XMLSchema XMLSchema.xsd\">";
		pie="\n</wsag:AgreementOffer>";
	}

	cabecera+="\n   <wsag:Name>\n     "+ acumulador2+" \n   </wsag:Name>";
}

;

tipoInicio:

  AGREEMENTOFFER|TEMPLATE

;


//texto_dos_puntos:(~(MENOS|DOS_PUNTOS))+;
//texto_ao: (~(MENOS|COMA|PUNTO_Y_COMA|DOS_PUNTOS))+;
//texto_cierre_frase: (~(COMA|PUNTO_Y_COMA))+;
//texto_menos: (~(MENOS))+;
texto_menos_ac: (~(MENOS));
texto_cierre_frase_ac:(~(COMA|PUNTO_Y_COMA));
texto_ao_ac:(~(MENOS|COMA|PUNTO_Y_COMA|DOS_PUNTOS));

contextItems: 
{
resultado +="\n   <wsag:Context>";	
}
(contextItem)+ 

{
resultado +="\n   </wsag:Context>";	
}
;

contextItem: (LIT_ENTERO|LIT_REAL|PUNTO)+
{
acumulador1="";	
}

 i:itemType DOS_PUNTOS (t:texto_contextItem
 {
 	acumulador1+=#t+" ";	
 }
 )+ cierre_frase
{	
	AST s = #i;
	String context = s.getText();
		
	if(context.equals("Initiator")||context.equals("Responder")){
		resultado+="\n     <wsag:Agreement"+#i+">\n       "+eliminaMayorYMenor(acumulador1)+"\n     </wsag:Agreement"+#i+">   ";
	}else if(context.equals("ExpirationTime")){
		resultado+="\n     <wsag:"+#i+">\n       "+eliminaMayorYMenor(eliminaEspaciosTexto(acumulador1))+"\n     </wsag:"+#i+">   ";
	}else{
		resultado+="\n     <wsag:"+#i+">\n       "+eliminaMayorYMenor(acumulador1)+"\n     </wsag:"+#i+">   ";
	}
	
}

;

texto_contextItem:
   (~(COMA|PUNTO_Y_COMA))
;

itemType:INITIATOR|RESPONDER|SERVICEPROVIDER|EXPIRATIONTIME|TEMPLATEID|TEMPLATENAME

;


terms:
	TERMS MENOS i:IDENT DOS_PUNTOS  

	
	c:conjunto_term

	{
		/*
		System.out.println("items");	
		
		for(int i = 0; i < listaItems.size();i++){
			System.out.println("nombre:"+listaItems.get(i).getNombre()+"\n");
			for(int j  = 0; j < listaItems.get(i).getListaNumeros().size();j++){
				System.out.println(listaItems.get(i).getListaNumeros().get(j).intValue());
				
			}	
			//System.out.println("tamao:"+listaItems.get(i).getListaNumeros().size()+"\n");
			//System.out.println("contenido:"+listaItems.get(i).getContenido()+"\n");
			//System.out.println("prefijo:"+listaItems.get(i).getPrefijo()+"\n\n"); 
		}*/
		resultado += "\n   <wsag:Terms wsag:Name=\""+#i+"\">";
		resultado += "\n     <wsag:All>" +UtilidadesItems.imprimeItems(listaItems)+"\n     </wsag:All>";
		resultado += "\n   </wsag:Terms>";	

		
		
	}
	;
	
conjunto_term: (term)+

	
;

term:

	 i:lista_numeros termino 
{
	itemActual.setContenido(contenido);
	itemActual.setNombre(nombre);
	listaItems.add(itemActual);
	itemActual = new Item();
	contenido = "";
	nombre = "";
}
;

lista_numeros:
{
	numero = "";	
}	
	(i:LIT_REAL^{numero+=#i;}
	|j:PUNTO^{numero+=#j;}
	|k:LIT_ENTERO^{numero+=#k;}
	
	)+
	

	{
	
	Item it = new Item();
	itemActual.agregaNumerosALista(numero);
	itemActual.setPrefijo(numero);
	
	}
;

termino: simple|agrupados|recursivos


;

simple:
	serviceReferences|serviceProperties|serviceDescriptionTerm|guarantee
;

agrupados:
   
   servicePropertiesAgrupado|serviceReferencesAgrupado|serviceDescriptionTermAgrupado|guaranteeAgrupado
;

guaranteeAgrupado:
 	GUARANTEES DOS_PUNTOS (guaranteesAnidados)+

;

texto_almohadilla: (~(MENOS|ALMOHADILLA))+;

guaranteesAnidados:
 ALMOHADILLA texto_almohadilla MENOS id1:IDENT PARENTESIS_ABIERTO BY p:proveedor PARENTESIS_CERRADO DOS_PUNTOS
{
   nombre = "Gurantee";	
   contenido+="\n<wsag:GuaranteeTerm wsag:Name=\""+#id1+"\" wsag:Obligated=\""+#p+"\" >";
   
} 
elementos_guarantee


{
	contenido+="\n</wsag:GuaranteeTerm>";	
}
;


guarantee:
GUARANTEE MENOS id1:IDENT PARENTESIS_ABIERTO BY p:proveedor PARENTESIS_CERRADO DOS_PUNTOS 
{
   nombre = "Gurantee";	
   contenido+="\n<wsag:GuaranteeTerm wsag:Name=\""+#id1+"\" wsag:Obligated=\""+#p+"\" >";
   
}

elementos_guarantee

{
	contenido+="\n</wsag:GuaranteeTerm>";	
}
;


proveedor:(SERVICECONSUMER|SERVICEPROVIDER);

elementos_guarantee:
	(serviceScope)? (qualifyingCondition)?  (slo|kpi)   (businesValueList)?

;

businesValueList:

	businessValue utility custom
;

utility:
	UTILITY IFMINUS SELECTED DOS_PUNTOS
	{
		contenido+="\n   "+"<wsag:Preference>"; ;	
	}
	

	i:IDENT IGUAL MAYOR l:LIT_REAL
	{
		contenido+="\n     <wsag:ServiceTermReference>\n"+#i+"\n     </wsag:ServiceTermReference>\n"+
		 "     <wsag:Utility>\n"+#l+"\n     </wsag:Utility>";	
		
	}
	(COMA i2:IDENT IGUAL MAYOR l2:LIT_REAL
	{
		contenido+="\n     <wsag:ServiceTermReference>\n"+#i2+"\n     </wsag:ServiceTermReference>\n"+
		 "     <wsag:Utility>\n"+#l2+"\n     </wsag:Utility>";	
		
	}
	)* 
	PUNTO_Y_COMA
	
	{
		contenido+="\n   "+"</wsag:Preference>"; ;	
	}
;

custom:
{
	acumulador1 = "";	
}

	CUSTOM BUSINESS VALUE DOS_PUNTOS (t:texto_ao_ac {acumulador1+=#t + " ";})+ PUNTO_Y_COMA
	{
		contenido+="\n   <CustomBusinessValue>\n"+eliminaMayorYMenor(acumulador1)+"\n   </CustomBusinessValue>";
	}
;




businessValue:
  BUSINESS VALUE DOS_PUNTOS RELATIVE IMPORTANCE BETWEENMINUS GUARANTEESMINUS DOS_PUNTOS l:LIT_ENTERO cierre_frase
  {
  	contenido+="\n   <wsag:BusinessValueList>\n     <wsag:Importance>"+#l+"</wsag:Importance>";
  }
  (penalty)*
  
  (reward)*
  
 {
 	contenido+="\n   </wsag:BusinessValueList>";
 }
  
;

reward:
{
	acumulador1="";
	acumulador2="";
	acumulador3="";
}


 REWARD DOS_PUNTOS (t:texto_ai  {acumulador1+=#t+" ";})+
 COMA (un:texto_ai {acumulador2+=#un+" ";})+ PUNTO ASSESSED IN
 (t2:texto_ai {acumulador3+=#t2+" ";})+ (i:INVOCATION)? cierre_frase
 {
 	contenido+="\n   <wsag:Reward>";
 	contenido+="\n     <wsag:AssesmentInterval>";
 if(i == null)
    contenido+="\n       <wsag:TimeInterval>\n       "+eliminaMayorYMenor(acumulador3)+"\n       </wsag:TimeInterval>";
 else 	
    contenido+="\n       <wsag:Count>\n       "+eliminaMayorYMenor(acumulador3)+"\n       </wsag:Count>";
   
    contenido+="\n     </wsag:AssesmentInterval>";     	
    contenido+="\n     <wsag:ValueUnit>\n     "+eliminaMayorYMenor(acumulador2)+"\n     </wsag:ValueUnit>\n     <wsag:ValueExpr>\n     "+eliminaMayorYMenor(acumulador1)+"\n     </wsag:ValueExpr>\n   </wsag:Reward>";
 }
;
penalty:
{
	acumulador1="";
	acumulador2="";
	acumulador3="";
}

PENALTY DOS_PUNTOS (t:texto_ai  {acumulador1+=#t+" ";} )+
COMA (un:texto_ai {acumulador2+=#un+" ";})+ PUNTO ASSESSED IN
 (t2:texto_ai {acumulador3+=#t2+" ";})+ (i:INVOCATION)? cierre_frase
 
 
 {
 	contenido+="\n   <wsag:Penalty>";
 	contenido+="\n     <wsag:AssesmentInterval>";
 if(i == null)
    contenido+="\n       <wsag:TimeInterval>\n       "+eliminaMayorYMenor(acumulador3)+"\n       </wsag:TimeInterval>";
 else 	
    contenido+="\n       <wsag:Count>\n       "+eliminaMayorYMenor(acumulador3)+"\n       </wsag:Count>";
     
    contenido+="\n     </wsag:AssesmentInterval>";     	
    contenido+="\n     <wsag:ValueUnit>\n     "+eliminaMayorYMenor(acumulador2)+"\n     </wsag:ValueUnit>\n     <wsag:ValueExpr>\n     "+eliminaMayorYMenor(acumulador1)+"\n     </wsag:ValueExpr>\n   </wsag:Penalty>";
 }
 
;


texto_ai:

 (~(PUNTO|COMA|PUNTO_Y_COMA|INVOCATION))

 
 

;

contenido_slo:
//(~(PUNTO_Y_COMA|DOS_PUNTOS))
(LIT_ENTERO|MAS|MENOS|DIVISION|POR|IGUAL|IDENT|MAYOR|MENOR|DISTINTO|MENOR_IGUAL|MAYOR_IGUAL)
;

slo:
 SLO DOS_PUNTOS 
 {
 	
 	contenido+="\n   <wsag:ServiceLevelObjective>\n     <wsag:CustomServiceLevel>"+
 	          "\n       ";	
 }
 (t:contenido_slo
 {
 	AST arbol = #t;
 	
 	contenido+=""+eliminaMayorYMenor(arbol.getText())+" ";
 	
 }
 )+
 {
 	contenido+="\n     </wsag:CustomServiceLevel>\n   </wsag:ServiceLevelObjective>";
 	
 }
 PUNTO_Y_COMA
;


kpi:
{
	acumulador1="";
	acumulador2="";	
}
 KPI DOS_PUNTOS (t:texto_ao_ac{acumulador1+=#t + " ";})+ COMA (t2:texto_ao_ac{acumulador2+=#t2 + " ";})+ cierre_frase
 
  {
 	
 	contenido+="\n   <wsag:ServiceLevelObjective>\n     <wsag:KPITarget>\n       <wsag:KPIName>"+
 	          "\n         "+eliminaMayorYMenor(acumulador1)+"\n       </wsag:KPIName>\n       <wsag:Target>\n         "+ eliminaMayorYMenor(acumulador2) +"\n       </wsag:Target>\n     </wsag:KPITarget>\n   <wsag:ServiceLevelObjective>\n"; 	
 }
;

qualifyingCondition:
{
	acumulador1 ="";	
}

 QUALIFYING CONDITION DOS_PUNTOS (t1:texto_ao_ac
 {
 	
 	acumulador1+=#t1+"";
 }
 )+  PUNTO_Y_COMA
 
 {
 	
 contenido+="\n   <wsag:QualifyingCondition>\n     "+eliminaMayorYMenor(acumulador1)+"\n   </wsag:QualifyingCondition>";	
 }
;
condicion:
	MAYOR|MENOR|IGUAL|MAYOR_IGUAL|MENOR_IGUAL
;

serviceScope:
	SCOPES  (elementosScopes) PUNTO_Y_COMA
;

elementosScopes:
{
	acumulador1="";
	acumulador2="";	
}
DOS_PUNTOS (id1:texto_ao_ac{acumulador1+=#id1 + " ";})+ MENOS (id2:texto_ao_ac {acumulador2+=#id2 + " ";})+
{
	contenido+="\n   <wsag:ServiceScope wsag:ServiceName=\""+eliminaMayorYMenor(acumulador1)+"\" >\n     "+eliminaMayorYMenor(acumulador2)+"\n   </wsag:ServiceScope>";
			
}
(
{
	acumulador1="";
	acumulador2="";	
}

COMA (id3:texto_ao_ac{acumulador1+=#id3 + " ";})+ MENOS (id4:texto_ao_ac{acumulador2+=#id4 + " ";})+

{
	contenido+="\n   <wsag:ServiceScope wsag:ServiceName=\""+eliminaMayorYMenor(acumulador1)+"\" >\n     "+eliminaMayorYMenor(acumulador2)+"\n   </wsag:ServiceScope>";	
}
)* 

;







serviceDescriptionTerm:
SERVICE DESCRIPTION TERM MENOS id:IDENT MENOS id2:IDENT DOS_PUNTOS 
{
nombre = "ServiceDescriptionTerm";
contenido+="\n<wsag:ServiceDescriptionTerm wsag:Name=\""+#id+"\" \n wsag:ServiceName=\""+#id2+"\">";
}
elementosServiceDescriptionTerm

{
contenido+="\n</wsag:ServiceDescriptionTerm>";
}

;

elementosServiceDescriptionTerm:
	mas_variables (COMA mas_variables)*  cierre_frase
;

//TODO cuidado que es posible que no este bien esta regla
mas_variables{String value = "";String metric = "";}:
(i2:IDENT (IGUAL l2:LIT_ENTERO {value=#l2.getText();})? 
(MENOS MEASURED BY {metric = "wsag:Metric=\"";}(d1:texto_cierre_frase_ac {metric+=#d1.getText();} )+ 
{metric+="\"";})? 
{
	
	contenido += "\n <OfferItem name=\""+#i2+"\"";
	if (!metric.isEmpty()){
		contenido += " "+metric;
	}
	
	if (!value.isEmpty()){
		contenido += ">"+value+"</OfferItem>";
	}
	else{
	   contenido += "/>";	
	}
	//contenido+=	"\n <OfferItem name=\""+#i2+"\""+metric+" >"+value+"</OfferItem>"; 
}
)
;


serviceDescriptionTermAgrupado:
	SERVICE DESCRIPTION TERMS DOS_PUNTOS (serviceDescriptionTermnAnidados)+
;

serviceDescriptionTermnAnidados:
	LIT_ENTERO id:IDENT MENOS id2:IDENT DOS_PUNTOS
	
	{
nombre = "ServiceDescriptionTerm";
contenido+="\n<wsag:ServiceDescriptionTerm wsag:Name=\""+#id+"\" \n wsag:ServiceName=\""+#id2+"\">";
}
 elementosServiceDescriptionTerm
 {
contenido+="\n</wsag:ServiceDescriptionTerm>";
}
	
;




serviceReferences:
{
	acumulador1="";	
}
SERVICE REFERENCE  MENOS i1:IDENT MENOS i2:IDENT DOS_PUNTOS (t:texto_ao_ac{acumulador1+=#t + " ";})+ cierre_frase
{
	nombre+="ServiceReferences";	
	contenido+= "\n<wsag:ServiceReferences wsag:Name=\""+#i1+"\" \n wsag:ServiceName=\""+#i2+"\"> \n "+eliminaMayorYMenor(acumulador1)+"\n";
	contenido+= "\n</wsag:ServiceReferences>";	
}
;
serviceReferencesAgrupado:
	SERVICE REFERENCES DOS_PUNTOS (serviceReferencesAnidados)+
;

serviceReferencesAnidados:

{
	acumulador1="";	
}
	LIT_ENTERO i1:IDENT MENOS i2:IDENT DOS_PUNTOS (t:texto_ao_ac{acumulador1+=#t + " ";})+ cierre_frase
	{
	contenido+= "\n<wsag:ServiceReferences wsag:Name=\""+#i1+"\" \n wsag:ServiceName=\""+#i2+"\"> \n"+eliminaMayorYMenor(acumulador1)+"\n";
	contenido+= "\n </wsag:ServiceReferences>";	
	}
	
;


texto_dos_puntos_ac:(~(MENOS|DOS_PUNTOS));

serviceProperties:
{
acumulador1="";
acumulador2="";
}


SERVICE PROPERTIES  MENOS (i1:texto_menos_ac
{
	acumulador1+=#i1+" ";
}
)+ 

MENOS (i2:texto_dos_puntos_ac
{
	
	acumulador2+=#i2+" ";	
}

)+  DOS_PUNTOS
{
		nombre+="ServiceProperties";
		contenido+= "\n<wsag:ServiceProperties wsag:Name=\""+#i1+"\" \n wsag:ServiceName=\""+#i2+"\"> \n  <wsag:VariableSet> ";
}

(elementosServicePropertiesTerm)+
{
		contenido+= "\n  </wsag:VariableSet> \n</wsag:ServiceProperties>";	
}

;

elementosServicePropertiesTerm:

{
	acumulador1="";
	acumulador2="";
	
}
LIT_ENTERO i:IDENT MENOS MEASURED BY (d1:texto_menos_ac 
{
	acumulador1+=#d1+" ";	
	
}
)+


MENOS RELATED TO (d2:texto_cierre_frase_ac
{
	acumulador2+=#d2+"";
}	
)+ cierre_frase
{
		contenido+="\n    <wsag:Variable wsag:Name=\""+#i+"\" wsag:Metric=\""+eliminaMayorYMenor(eliminaEspaciosTexto(acumulador1))+"\"> \n     <wsag:Location> \n       "+eliminaMayorYMenor(acumulador2) +"\n     </wsag:Location>\n    </wsag:Variable>";
             
}
;
servicePropertiesAgrupado:
	SERVICE PROPERTIES DOS_PUNTOS (servicePropertiesAnidados)+
;

servicePropertiesAnidados:
	 i1:IDENT MENOS i2:IDENT  DOS_PUNTOS
	{
		nombre ="ServiceProperties";
		contenido+= "\n<wsag:ServiceProperties wsag:Name=\""+#i1+"\" \n wsag:ServiceName=\""+#i2+"\"> \n  <wsag:VariableSet> \n";
	}
	
	(elementosServicePropertiesTerm)+
	
	{
		contenido+= "\n  </wsag:VariableSet> \n</wsag:ServiceProperties>";	
	}
;







recursivos:
   (all|exactlyOne|oneOrMoreBetween) 
;

all:
{
	nombre ="all";
	contenido += "\n<wsag:all>";	
}
 	ALL DOS_PUNTOS
;

exactlyOne:
{
	nombre ="eob";
	contenido += "\n<wsag:exactlyOne>";	
}


 	EXACTLY ONE BETWEENMINUS DOS_PUNTOS 
	
 	
 	
   

;
oneOrMoreBetween:
	
{
	nombre ="omb";
	contenido += "\n<wsag:OneOrMore>";	
}
 	ONE OR MORE BETWEENMINUS DOS_PUNTOS 
	
 
;

creationConstraints:

CREATION CONSTRAINT
{
	resultado +="\n   <wsag:CreationConstraints>";	
}
 DOS_PUNTOS (conjuntoitems)? (conjuntoconstraints)?
 
{
	resultado +="\n   </wsag:CreationConstraints>";	
} 
 
;

conjuntoitems:
	ITEMS DOS_PUNTOS items
;

items: (item)+
;

item: 
LIT_ENTERO id:IDENT DOS_PUNTOS VALUE OF d:DIRECCION IS 

{
	resultado += "\n      <wsag:Item wsag:Name=\""+#id+"\">"
	          +  "\n         <wsag:Location>\n          "+#d+"\n         </wsag:Location>"
	          +  "\n         <wsag:ItemConstraint>";

}

  tipo_datos
{
	
	resultado += "\n         </wsag:ItemConstraint>"
	          +  "\n      </wsag:Item>";	
}

;
 	
 	
tipo_datos:
   (basico|enumerado)
;

basico: td:tipo_dato_exacto ab:abertura min:LIT_ENTERO COMA max:LIT_ENTERO c:cerratura cierre_frase


{
	
	resultado+= "\n              <xs:restriction base = \""+tipoConstraint+"\" >";
	          
	          
	AST s = #ab;
	
	char abertura = s.getText().charAt(0);
	
	if(abertura == '('){
	resultado+=" \n                 <xs:minExclusive value=\""+#min+"\" />";
	}else{
	resultado+=" \n                 <xs:minInclusive value=\""+#min+"\" />";
	}
	;
	
	s = #c;
	
	char cerratura = s.getText().charAt(0);
	
	if(cerratura == ')'){
	resultado+=" \n                 <xs:maxExclusive value=\""+#max+"\" />";
	}else{
	resultado+=" \n                 <xs:maxInclusive value=\""+#max+"\" />";
	}
	;          
	          

	
	
	resultado+= "\n              </xs:restriction>";
	
}


;

abertura:
	(CORCHETE_ABIERTO|PARENTESIS_ABIERTO)
;

cerratura:
    (CORCHETE_CERRADO|PARENTESIS_CERRADO) 
;

tipo_dato_exacto:
	(INTEGER
	{tipoConstraint="integer";}
	|FLOAT
	{tipoConstraint="float";}
	|(XS DOS_PUNTOS INTEGER)
	{tipoConstraint="xs:integer";}
	|(XS DOS_PUNTOS FLOAT){tipoConstraint="xs:float";})
 	
 	;
 	
enumerado: i:IDENT ENUM LLAVE_ABIERTA id:IDENT 



{
	resultado+= "\n              <xs:simpleType name=\""+#i+"\">"
	          + "\n                 <xs:restriction base = \""+tipoConstraint+"\" >"
	          + "\n                    <xs:enumeration value=\""+#id+"\" />";
	          
}

mas_elementos_enum LLAVE_CERRADA cierre_frase
{
	
	resultado+= "\n                 </xs:restriction>"
	          + "\n              </xs:simpleType>";	
	
	
}
;
mas_elementos_enum:

(COMA id:IDENT

{
	resultado+="\n                    <xs:enumeration value=\""+#id+"\" />";
	
}
)*


;

cierre_frase:
	(COMA|PUNTO_Y_COMA)
; 	

conjuntoconstraints:
	CONSTRAINT DOS_PUNTOS constraints



;
 	
constraints:
    (constraint)+
; 

constraint:
  (LIT_ENTERO|LIT_REAL|PUNTO)+  id:IDENT DOS_PUNTOS 
  
  {
	
	resultado += "\n      <wsag:Constraint>"
	          +  "\n          <Name>\n            "+#id;	
 
  	
  	
	resultado += "\n          </Name>";
	resultado += "\n          <Content>\n            ";
 }
   
   (t:contenidoConstraint
   
   {
   	AST arbol = #t;
   	
   	resultado += eliminaMayorYMenor(arbol.getText())+" ";
   }
   )+ 
   
   
   
   (COMA|PUNTO_Y_COMA)
 {
	
	resultado += "\n          </Content>";
	resultado += "\n      </wsag:Constraint>";	

  }

  
;
contenidoConstraint:
~(COMA|PUNTO_Y_COMA)

;



condicionada:
 	IF id1:IDENT IGUAL id2:IDENT IMPLICACION id3:IDENT IGUAL id4:IDENT COMA
 	

 	
;


no_se_usan:EXCLAMACION;

