/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.temporalAlgorithms;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class InverseTemporalSequence {
	protected TemporalInterval validityPeriod;
	protected ArrayList<InverseTemporalPair> pairs;

	/**
	 * @param validityPeriod
	 * @param pairs
	 */
	public InverseTemporalSequence(TemporalInterval validityPeriod,
			ArrayList<InverseTemporalPair> pairs) {
		this.validityPeriod = validityPeriod;
		this.pairs = pairs;
	}
	
	public InverseTemporalSequence(TemporalInterval validityPeriod){
		this.validityPeriod = validityPeriod;
		this.pairs = new ArrayList<InverseTemporalPair>();
	}
	
	public int search(LinkedHashSet<Object> data){
		int index = 0;
		for (InverseTemporalPair itp: pairs){
			if (itp.getData().equals(data)){
				return index; 
			}	
			index++;
		}
		return -1;
	}

	public void add(InverseTemporalPair itp){
		pairs.add(itp);
	}
	
	public ArrayList<InverseTemporalPair> getPairs() {
		return pairs;
	}

	public TemporalInterval getValidityPeriod() {
		return validityPeriod;
	}
}
