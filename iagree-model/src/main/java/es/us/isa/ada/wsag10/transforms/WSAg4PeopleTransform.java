/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.wsag10.transforms;


import es.us.isa.ada.wsag10.transforms.wsag2wsag4people.XmlToWSAg4People;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;


/**
 * @author  cmuller
 */
public class WSAg4PeopleTransform implements ITransform{

	private final static String INPUT_FORMAT = "wsag";
	private final static String OUTPUT_FORMAT = "wsag4people";
	
	private XmlToWSAg4People impl;
	
	public WSAg4PeopleTransform(){
		impl = new XmlToWSAg4People();
	}
	
	@Override
	public String getInputFormat() {
		return INPUT_FORMAT;
	}

	@Override
	public String getOutputFormat() {
		return OUTPUT_FORMAT;
	}

	@Override
	public boolean transform(String source, String destination) {
		Document docXML ;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder builder;
		String content = "";
		
			try {
				builder = factory.newDocumentBuilder();
				docXML = builder.parse(source);
				content = impl.convertXMLToWSAg4People(docXML);
			} catch (ParserConfigurationException e1) {
				// TODO Auto-generated catch block
				System.err.println("Error while opening the agreement file");
				e1.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				System.err.println("Error while opening the agreement file");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.err.println("Error while opening the agreement file");
				e.printStackTrace();
			}
			
		
		
		boolean res = (content != null);

		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(destination);
			pw = new PrintWriter(fichero);
			pw.println(content);
		} catch (IOException e) {
			res = false;
			e.printStackTrace();
		} finally {
			try {
				fichero.close();
			} catch (IOException e) {
				res = false;
				e.printStackTrace();
			}

		}

		return res;
	}

	@Override
	public String transform(String source) {
		Reader r = new StringReader(source);
		InputSource is = new InputSource(r);
		Document docXML;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		String res = "";

		try {
			builder = factory.newDocumentBuilder();
			docXML = builder.parse(is);
			res = impl.convertXMLToWSAg4People(docXML); 
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
}
	
		


