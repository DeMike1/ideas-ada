package es.us.isa.ada.xyz.osgi;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import es.us.isa.ada.ADA;
import es.us.isa.ada.subfacades.ProxyAnalyzer;
import es.us.isa.ada.xyz.XYZAnalyzer;
import es.us.isa.ada.xyz.operations.XYZComplianceOperation;
import es.us.isa.ada.xyz.operations.XYZConsistencyOperation;

public class Activator implements BundleActivator {

	private Collection<ServiceRegistration> registrations;
	
	public void start(BundleContext bc) throws Exception {
		// TODO Auto-generated method stub
		registrations = new LinkedList<ServiceRegistration>();
		ServiceRegistration sr;
		Hashtable<String,String> table = new Hashtable<String,String>();
		XYZAnalyzer xyz = new XYZAnalyzer();
		//XXX analyser's id here
		String id = "xyz";
		table.put("type", "ADA");
		table.put("id", id);
		
		ProxyAnalyzer proxy = new ProxyAnalyzer(id, xyz);
		//XXX here add operations
		proxy.addOperation(ADA.CONSISTENCY, XYZConsistencyOperation.class);
		proxy.addOperation(ADA.COMPLIANCE, XYZComplianceOperation.class);
		//XXX etc
		sr = bc.registerService(ProxyAnalyzer.class.getCanonicalName(), proxy, table);
		registrations.add(sr);
	}

	public void stop(BundleContext bc) throws Exception {
		Iterator<ServiceRegistration> it = registrations.iterator();
		while (it.hasNext()){
			ServiceRegistration sr = it.next();
			sr.unregister();
		}
	}

}
