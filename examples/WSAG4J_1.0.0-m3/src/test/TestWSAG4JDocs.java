package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;
import es.us.isa.ada.service.ADAServiceV2Locator;
import es.us.isa.ada.service.ADAServiceV2PortType;

public class TestWSAG4JDocs {
	
	private static final String baseURL = "files/";
	
	private static ADAServiceV2PortType servicePort = null;
	
	public static void main(String[] args) {
		
		//A�adimos la direcci�n de los documentos que vamos a analizar
		//String[] docs = {baseURL+"test-template.xml", baseURL+"template-1.xml.vm", baseURL+"template-2.xml.vm", baseURL+"template-3.xml.vm", baseURL+"template-4.xml.vm", baseURL+"template-5.xml.vm"};
		String[] docs = {baseURL+"compute-test_InconsistentMod.xml"};
		
		//Analizamos los documentos
		for(int i=0; i<docs.length; i++){
			System.out.println("\n\nEl documento que vamos a analizar a continuaci�n es:");
			System.out.println("  -> "+docs[i]);
			
			//Cargamos un documento
			InputStream is = TestWSAG4JDocs.class.getResourceAsStream(docs[0]);
			String docXML = null;
			try {
				docXML = inputStreamToString(is);
				System.out.println(docXML);
			} catch (IOException e) {
				System.out.println("Error al pasar de InputStream a String");
				e.printStackTrace();
			}
			
			//Llamamos a ADA para analizar el documento
			try {
				servicePort = new ADAServiceV2Locator().getADAServiceV2Port();
			} catch (ServiceException e) {
				System.out.println("Error al crear la conexion con el servidor");
				e.printStackTrace();
			}
			
			Boolean consistente = null;
			try {
				consistente = servicePort.checkDocumentConsistency(docXML.getBytes());
			} catch (RemoteException e) {
				System.out.println("Error al analizar el documento");
				e.printStackTrace();
			}
			
			//Imprimimos mensaje con el resultado
			if (consistente) {
				System.out.println("El documento es consistente :D");
			}else{
				System.out.println("El documento es inconsistente :(");
			}
		}
	}
	
	private static String inputStreamToString(InputStream is) throws IOException{
		BufferedReader BR = new BufferedReader(new InputStreamReader(is));
		StringBuilder SB = new StringBuilder();
		String line1 = null;

		while ((line1 = BR.readLine()) != null) {
		SB.append(line1 + "\n");
		}

		BR.close();
		return SB.toString();
	}
}
