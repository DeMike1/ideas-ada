package client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.security.auth.login.LoginContext;

import static org.junit.Assert.*;

import org.ogf.graap.wsag.api.AgreementOffer;
import org.ogf.graap.wsag.api.client.AgreementClient;
import org.ogf.graap.wsag.api.client.AgreementFactoryClient;
import org.ogf.graap.wsag.api.client.AgreementFactoryRegistryClient;
import org.ogf.graap.wsag.api.exceptions.AgreementFactoryException;
import org.ogf.graap.wsag.api.types.AgreementOfferType;
import org.ogf.graap.wsag.client.AgreementFactoryRegistryLocator;
import org.ogf.graap.wsag.security.core.KeystoreProperties;
import org.ogf.graap.wsag.security.core.keystore.KeystoreLoginContext;
import org.ogf.schemas.graap.wsAgreement.AgreementTemplateType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

public class Main {

	public static void main(String[] args) throws Exception {
		
		/**
		 * Number of installed factories
		 */
		final int EXPECTED_FACTORIES = 2;
		
		/**
		 * Number of templates that returns factory_1
		 */
		final int EXPECTED_TEMPLATES_FACTORY_1 = 2;
		
		
		
		//Provide credentials and do login
		KeystoreProperties properties = new KeystoreProperties();
		properties.setKeyStoreAlias("wsag4j-user");
        properties.setPrivateKeyPassword("user@wsag4j");
        
        properties.setKeyStoreType("JKS");
        properties.setKeystoreFilename("/wsag4j-client-keystore.jks");
        properties.setKeystorePassword("user@wsag4j");

        properties.setTruststoreType("JKS");
        properties.setTruststoreFilename("/wsag4j-client-keystore.jks");
        properties.setTruststorePassword("user@wsag4j");

//        try {
//			LoginContext loginContext = new KeystoreLoginContext(properties);
//			loginContext.login();
//		} catch (LoginException e) {
//			e.printStackTrace();
//		}
        LoginContext loginContext = new KeystoreLoginContext(properties);
        loginContext.login();
		
		//Create agreement factory client and retrieve a template from it
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue("http://localhost:8080/wsag4j-agreement-factory-1.0.0-m3/");
		
		AgreementFactoryRegistryClient registry = AgreementFactoryRegistryLocator.getFactoryRegistry(epr, loginContext);
		AgreementFactoryClient[] factories = registry.listAgreementFactories();
		assertEquals("�Se devuelve el mismo numero de factorias que las esperadas?", EXPECTED_FACTORIES, factories.length);
		
		/*
		 * We have 2 factories installed. One returns 2 templates, the other 3.
		 */
		
		AgreementFactoryClient factory = null;
//		System.out.println("El tama�o de factories es: "+factories.length);
		for(int i=0; i<factories.length; i++){
			AgreementTemplateType[] templates = factories[i].getTemplates();
//			System.out.println("En la factor�a n�mero "+i+", hay "+templates.length+" templates");
			
			assertNotNull(templates);
			
			if(templates.length == EXPECTED_TEMPLATES_FACTORY_1){
//				System.out.println("Nos quedamos con la factory n�mero "+i+" porque tiene "+EXPECTED_TEMPLATES_FACTORY_1+" templates que es el n�mero que esperamos");
				factory = factories[i];
			}
		}
		
		assertNotNull(factory);
		
		//trace true, if the web service client should traces incoming/outgoing messages
		//factory.setTrace(true);
		
		AgreementTemplateType[] templates = factory.getTemplates();
		
		//Imprimir el texto XML de las plantillas
		for(int i=0; i<templates.length; i++){
			System.out.println("\nTemplate "+i+": "+templates[i].xmlText());
		}
		
		//Create a valid agreement offer using this template. 
		//Create a new service level agreement under the conditions stated in the offer.
		AgreementOffer offer = new AgreementOfferType(templates[0]);
		
		AgreementClient agreement = null;
		try{
			agreement = factory.createAgreement(offer);
		}catch(AgreementFactoryException e){
			e.printStackTrace();
		}

		//Imprimir nombre y id del acuerdo
		System.out.println("\nAgreement name: "+agreement.getName()+"\nAgreement id: "+agreement.getAgreementId());
	}
}
