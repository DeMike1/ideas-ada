package pruebas;

import java.io.File;
import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;

public class pruebasComplianceTrue {

	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	boolean compliance = true;
	File carpeta = new File("pruebasCompliance(true)");
	File[] lista = carpeta.listFiles();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
		try {
			File fichero = new File("CSV/compliance-TRUE-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");	//columna A
			writercsv.write("Tipo de Operación");	//columna B
			writercsv.write("Resultado Esperado");	//columna C
			writercsv.write("Resultado Obtenido");	//columna D
			writercsv.write("¿Excepcion?");			//columna E
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		/*---------------------------------------------------------------------------------*/
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].isDirectory()) {
				if(!(lista[i].getName().startsWith("."))){
				File subcarpeta = new File(carpeta.getName() + "/"+ lista[i].getName());	
				String ruta = subcarpeta.getPath();
					AbstractDocument doc = ada.loadDocument(ruta+"/"+"TEMPLATE.wsag");
					AbstractDocument doc1 = ada.loadDocument(ruta+"/"+"OFFER.wsag");
					ComplianceOperation op = (ComplianceOperation) ada.createOperation("compliance");
					op.addDocument(doc);
					op.addDocument(doc1);
					boolean aux;
					boolean excepcion = false;
					try {
						ada.analyze(op);
						aux = op.isCompliant();
					} catch (Exception e) {
						aux = false;
						excepcion = true;
						e.printStackTrace();
					}

					compliance = compliance && aux;
					System.out.println("Is the offer compliant?: " + aux);
					/*
					 * BLOQUE TRY-CATCH PARA ESCRIBIR EL CONTENIDO DEL ARCHIVO
					 * .CSV
					 */
					try {
						writercsv.write(lista[i].getName());
						writercsv.write("Compliance");
						writercsv.write("true");
						writercsv.write("" + aux);
						writercsv.write("" + excepcion);
						writercsv.endRecord();
						writercsv.endRecord();

					} catch (Exception e) {
						throw e;
					}

				}
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (compliance == true);
	}
}