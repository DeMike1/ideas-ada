package pruebas;

import java.io.File;
import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;

public class pruebasConsistencyTrue {

	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	File carpeta = new File("pruebasConsistency(true)");
	String[] lista = carpeta.list();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
		boolean consistent = true;
		try {
			File fichero = new File("CSV/consistency-TRUE-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Resultado Esperado");
			writercsv.write("Resultado Obtenido");
			writercsv.write("¿Warnings?");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		/*---------------------------------------------------------------------------------*/
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
				AbstractDocument doc = ada.loadDocument(carpeta.getName() + "/"+ lista[i]);
				ConsistencyOperation op = (ConsistencyOperation) ada.createOperation("consistency");
				op.addDocument(doc);
				ada.analyze(op);
				boolean aux = op.isConsistent();
				consistent = consistent && aux;
				boolean warning = false;
				System.out.println("Is the offer consistent?: " + aux);
				/* BLOQUE TRY-CATCH PARA ESCRIBIR EL CONTENIDO DEL ARCHIVO .CSV */
				try {
					writercsv.write(lista[i]);
					writercsv.write("Consistencia");
					writercsv.write("true");
					writercsv.write("" + aux);
					writercsv.write("" + warning);
					writercsv.endRecord();
					writercsv.endRecord();

				} catch (Exception e) {
					throw e;
				}
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (consistent == true);
	}
}