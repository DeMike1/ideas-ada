package pruebas;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;

/*
 * TODO corregir el error al no encontrar valor a asignar en los OfferItem
 * TODO ver por que tarda tanto la consistencia con los nuevos OfferITem
 * TODO ver por que falla el compliance con los nuevos OfferItem
 */
public class PruebasNewOfferItems {

	private ADATestingFacade ada;
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}
	
	@Test
	public void testCompliance1(){
		String template = "SDTsNewTests/distinctOfferTemplateVar/TEMPLATE.wsag";
		String offer = "SDTsNewTests/distinctOfferTemplateVar/OFFER.wsag";
		boolean res = doComplianceOp(template, offer);
		assert !res;
	}
	
	@Test
	public void testCompliance2(){
		String template = "SDTsNewTests/sameOfferTemplateVar/TEMPLATE.wsag";
		String offer = "SDTsNewTests/sameOfferTemplateVar/OFFER.wsag";
		boolean res = doComplianceOp(template, offer);
		assert !res;
	}
	
	@Test
	public void testConsistency1(){
		boolean res = doConsistencyOp("SDTsNewTests/undeclaredSDTVar.wsag"); 
		assert res;
	}
	
	@Test
	public void testConsistency2(){
		boolean res = doConsistencyOp("SDTsNewTests/validSDTVar.wsag"); 
		assert res;
	}
	
	private boolean doConsistencyOp(String file){
		AbstractDocument doc = ada.loadDocument(file);
		ConsistencyOperation op = (ConsistencyOperation) ada.createOperation("consistency");
		op.addDocument(doc);
		ada.analyze(op);
		boolean res = op.isConsistent();
		return res;
	}
	
	private boolean doComplianceOp(String template, String offer){
		AbstractDocument doc = ada.loadDocument(template);
		AbstractDocument doc1 = ada.loadDocument(offer);
		ComplianceOperation op = (ComplianceOperation) ada
				.createOperation("compliance");
		op.addDocument(doc);
		op.addDocument(doc1);	
		ada.analyze(op);
		boolean aux = op.isCompliant();
		return aux;
	}
	
}
