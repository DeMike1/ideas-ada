package pruebas;

import java.io.File;
import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;

public class pruebasConsistencyFalse {

	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	boolean consistent = false;
	File carpeta = new File("pruebasConsistency(false)(Entero)");
	String[] lista = carpeta.list();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
		try {
			File fichero = new File("CSV/consistency-FALSE-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Resultado Esperado");
			writercsv.write("Resultado Obtenido");
			writercsv.write("¿Excepcion?");
			writercsv.write("¿Warnings?");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		/*---------------------------------------------------------------------------------*/
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
				String s = carpeta.getName() + "/" + lista[i];
				AbstractDocument doc = ada.loadDocument(s);
				ConsistencyOperation op = (ConsistencyOperation) ada
						.createOperation("consistency");
				op.addDocument(doc);
				boolean aux;
				boolean excepcion = false;
				boolean warning = false;
				try {
					ada.analyze(op);
					aux = op.isConsistent();
					
				} catch (Exception e) {
					e.printStackTrace();
					aux = false;
					excepcion = true;
				}

				consistent = consistent || aux;
				System.out.println("Is the offer consistent?: " + aux);
				/* BLOQUE TRY-CATCH PARA ESCRIBIR EL CONTENIDO DEL ARCHIVO .CSV */
				try {
					writercsv.write(lista[i]);
					writercsv.write("Consistencia");
					writercsv.write("false");
					writercsv.write("" + aux);
					writercsv.write("" + excepcion);
					writercsv.write("" + warning);
					writercsv.endRecord();
					writercsv.endRecord();

				} catch (Exception e) {
					throw e;
				}
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (consistent == false);
	}
}