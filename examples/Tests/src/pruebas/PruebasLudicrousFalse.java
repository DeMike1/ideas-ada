package pruebas;

import java.io.File;
import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.LudicrousTermsOperation;

public class PruebasLudicrousFalse {


	boolean resultado = false;
	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	File carpeta = new File("ludicrousTerms(false)");
	String[] lista = carpeta.list();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		try {
			File fichero = new File("CSV/ludicrous-false-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Resultado Esperado");
			writercsv.write("Resultado Obtenido");
			writercsv.write("¿Excepcion?");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
				String s = carpeta.getName() + "/" + lista[i];
				AbstractDocument doc = ada.loadDocument(s);
				LudicrousTermsOperation op = (LudicrousTermsOperation) ada.createOperation("ludicrous");
//				ConsistencyOperation op = (ConsistencyOperation) ada
//						.createOperation("consistency");
				op.addDocument(doc);
//				boolean aux;
				boolean excepcion = false;
				boolean warning = false;
				try {
					ada.analyze(op);
//					aux = op.isConsistent();
					warning = op.hasLudicrousTerms();
					
				} catch (Exception e) {
					e.printStackTrace();
//					aux = false;
					excepcion = true;
				}

//				consistent = consistent || aux;
				resultado = resultado || warning;
				System.out.println(s);
				System.out.println("Has the document ludicrous terms?: " + warning);
				/* BLOQUE TRY-CATCH PARA ESCRIBIR EL CONTENIDO DEL ARCHIVO .CSV */
				try {
					writercsv.write(lista[i]);
					writercsv.write("Ludicrous Terms");
					writercsv.write("false");
//					writercsv.write("" + aux);
					writercsv.write("" + warning);
					writercsv.write("" + excepcion);
					writercsv.endRecord();
					writercsv.endRecord();

				} catch (Exception e) {
					throw e;
				}
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (resultado == false);
	}
	
}
