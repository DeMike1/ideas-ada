package pruebas.frontend;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.DeadTermsOperation;
import es.us.isa.ada.operations.ExplainDeadTerms;
import es.us.isa.ada.operations.ExplainLudicrousTerms;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;
import es.us.isa.ada.operations.LudicrousTermsOperation;
import es.us.isa.ada.wsag10.AgreementDocument;
import es.us.isa.ada.wsag10.Term;

public class PruebasDocumentosFrontEnd {

	private ADATestingFacade ada;
	
	@Before
	public void setUp(){
		ada = new ADATestingFacade();
	}
	
	@Test
	public void testOffer1(){
		AgreementDocument d = loadDocument("front-end-documents\\AgreementOffer(guarantee_term_errors_by_QC_SLO_QC-SLO).wsag");
		testConsistency(d, false);
		
	}
	
	@Test
	public void testOffer2(){
		AgreementDocument d = loadDocument("front-end-documents\\AgreementOffer(warning_by_SDT).wsag");
		testConsistency(d, true);
		testDeadTerms(d, true);
		testLudicrousTerms(d, false);
	}
	
	@Test
	public void testOffer3(){
		AgreementDocument d = loadDocument("front-end-documents\\ConsistentAgreementOfferCompliantWithConsistentTemplate.wsag");
		testConsistency(d, true);
		testDeadTerms(d, false);
		testLudicrousTerms(d, false);
	}
	
	@Test
	public void testOffer4(){
		AgreementDocument d = loadDocument("front-end-documents\\ConsistentAgreementOfferNonCompliantWithConsistentTemplate(by_SDTs_and_GT).wsag");
		testConsistency(d, true);
		testDeadTerms(d, false);
		testLudicrousTerms(d, false);
	}
	
	@Test
	public void testTemplate1(){
		System.out.println("=====================================");
		System.out.println("front-end-documents\\ConsistentTemplate(with_deadTerm_GT_CC).wsag");
		System.out.println("=====================================");
		AgreementDocument d = loadDocument("front-end-documents\\ConsistentTemplate(with_deadTerm_GT_CC).wsag");
		testConsistency(d, true);
		testDeadTerms(d, true);
		testLudicrousTerms(d, false);
	}
	
	@Test
	public void testTemplate2(){
		System.out.println("=====================================");
		System.out.println("front-end-documents\\Template(term_errors_by_SDT_and_CC_with_themselves).wsag");
		System.out.println("=====================================");
		AgreementDocument d = loadDocument("front-end-documents\\Template(term_errors_by_SDT_and_CC_with_themselves).wsag");
		testConsistency(d, false);
	}
	
	@Test
	public void testTemplate3(){
		System.out.println("=====================================");
		System.out.println("front-end-documents\\Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag");
		System.out.println("=====================================");
		AgreementDocument d = loadDocument("front-end-documents\\Template(term_errors_by_SDT-CC_SDT-GT_and_GT-CC).wsag");
		testConsistency(d, false);
	}
	
	@Test
	public void testTemplate4(){
		System.out.println("=====================================");
		System.out.println("front-end-documents\\Template(term_errors_by_several_GTs_and_CCs).wsag");
		System.out.println("=====================================");
		AgreementDocument d = loadDocument("front-end-documents\\Template(term_errors_by_several_GTs_and_CCs).wsag");
		testConsistency(d, false);
	}
	
	@Test
	public void testTemplate5(){
		System.out.println("=====================================");
		System.out.println("front-end-documents\\Template(warning_by_SDT_CC).wsag");
		System.out.println("=====================================");
		AgreementDocument d = loadDocument("front-end-documents\\Template(warning_by_SDT_CC).wsag");
		testConsistency(d, true);
		//TODO que warning debe dar aqui?????
	}
	
	private AgreementDocument loadDocument(String path){
		AgreementDocument res = (AgreementDocument) ada.loadDocument(path);
		return res;
	}
	
	private void testConsistency(AgreementDocument d, boolean result){
		ConsistencyOperation op = (ConsistencyOperation) ada.createOperation("consistency");
		op.addDocument(d);
		ada.analyze(op);
		boolean b = op.isConsistent();
		if (!b){
			ExplainNoConsistencyOperation op2 = (ExplainNoConsistencyOperation) ada.createOperation("explainInconsistencies");
			op2.addDocument(d);
			ada.analyze(op2);
			Map<AgreementElement,Collection<AgreementElement>> res = op2.explainErrors();
			Set<Entry<AgreementElement,Collection<AgreementElement>>> entries = res.entrySet();
			for (Entry<AgreementElement,Collection<AgreementElement>> entry:entries){
				System.out.print(entry.getKey().getName() + " => ");
				Collection<AgreementElement> col = entry.getValue();
				for (AgreementElement elem:col){
					System.out.print(elem.getName() + " ");
				}
				System.out.println();
			}
		}
		assert (b == result);
	}
	
	
	
	private void testDeadTerms(AgreementDocument d, boolean result){
		DeadTermsOperation op = (DeadTermsOperation) ada.createOperation("dead");
		op.addDocument(d);
		ada.analyze(op);
		boolean b = op.hasDeadTerms();
		if (b){
			Collection<Term> deadTerms = op.getDeadTerms();
			ExplainDeadTerms op2 = (ExplainDeadTerms) ada.createOperation("explainDead");
			op2.addDocument(d);
			op2.setDeadTerms(deadTerms);
			ada.analyze(op2);
			Map<Term,Collection<AgreementElement>> res = op2.explainDeadTerms();
			Set<Entry<Term,Collection<AgreementElement>>> entries = res.entrySet();
			for (Entry<Term,Collection<AgreementElement>> entry:entries){
				System.out.print(entry.getKey().getName() + " => ");
				Collection<AgreementElement> col = entry.getValue();
				for (AgreementElement elem:col){
					System.out.print(elem.getName() + " ");
				}
				System.out.println();
			}
		}
		assert (b == result);
	}
	
	
	
	private void testLudicrousTerms(AgreementDocument d, boolean result){
		LudicrousTermsOperation op = (LudicrousTermsOperation) ada.createOperation("ludicrous");
		op.addDocument(d);
		ada.analyze(op);
		boolean b = op.hasLudicrousTerms();
		if (b){
			Collection<Term> ludicrousTerms = op.getLudicrousTerms();
			ExplainLudicrousTerms op2 = (ExplainLudicrousTerms) ada.createOperation("explainLudicrous");
			op2.addDocument(d);
			op2.setLudicrousTerms(ludicrousTerms);
			ada.analyze(op2);
			Map<Term,Collection<AgreementElement>> res = op2.explainLudicrousTerms();
			Set<Entry<Term,Collection<AgreementElement>>> entries = res.entrySet();
			for (Entry<Term,Collection<AgreementElement>> entry:entries){
				System.out.print(entry.getKey().getName() + " => ");
				Collection<AgreementElement> col = entry.getValue();
				for (AgreementElement elem:col){
					System.out.print(elem.getName() + " ");
				}
				System.out.println();
			}
		}
		assert (b == result);
	}
	
}
