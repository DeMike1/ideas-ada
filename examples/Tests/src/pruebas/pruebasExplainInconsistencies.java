package pruebas;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;

public class pruebasExplainInconsistencies {

	boolean errores = true;
	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	// File carpeta = new File("pruebasExplainInconsistencies");
	File carpeta = new File("moreExplainingConsistency");
	String[] lista = carpeta.list();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
		try {
			File fichero = new File("CSV/explainInconsistencies-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Contiene Errores");
			// writercsv.write("Error");
			// writercsv.write("Causas");
			// writercsv.write("¿Excepcion?");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		/*---------------------------------------------------------------------------------*/
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
				AbstractDocument doc = ada.loadDocument(carpeta.getName() + "/"
						+ lista[i]);
				ExplainNoConsistencyOperation op = (ExplainNoConsistencyOperation) ada
						.createOperation("explainInconsistencies");
				op.addDocument(doc);
				boolean aux = true;
				// Collection<Explanation> col;
				Map<AgreementElement, Collection<AgreementElement>> map;
				Entry<AgreementElement, Collection<AgreementElement>> ex = null;
				boolean excepcion = false;
				try {
					ada.analyze(op);
					map = op.explainErrors();
					Iterator<Entry<AgreementElement, Collection<AgreementElement>>> it = map
							.entrySet().iterator();

					if (map.isEmpty()) {
						aux = false;
					}
					writercsv.write(lista[i]);
					writercsv.write("ExplainInconsistencies");
					writercsv.write("" + aux);
					while (it.hasNext()) {
						ex = it.next();

						String res = ex.getKey().getName() + " ->";
						for (AgreementElement elem : ex.getValue()) {
							res += " " + elem.getName();
						}
						writercsv.write(res);
						// writercsv.write("" + ex);
						// writercsv.write("" + excepcion);

						// writercsv.endRecord();

					}
					writercsv.endRecord();
				} catch (Exception e) {
					aux = false;
					excepcion = true;
					e.printStackTrace();
				}

				errores = errores && aux;
				// System.out.println("Does the document contains errors?: " +
				// aux);
				// try {
				// writercsv.write(lista[i]);
				// writercsv.write("ExplainInconsistencies");
				// writercsv.write("" + aux);
				// writercsv.write("" + ex);
				// writercsv.write("" + excepcion);
				// writercsv.endRecord();
				// writercsv.endRecord();
				//
				// } catch (Exception e) {
				// throw e;
				// }

			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (errores == true);
	}
}
