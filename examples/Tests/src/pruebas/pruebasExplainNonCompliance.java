package pruebas;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.choco.questions.ChocoExplainNoComplianceOp;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.errors.Explanation;
import es.us.isa.ada.operations.AlternativeDocumentsOperation;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.operations.ExplainNoConsistencyOperation;
import es.us.isa.ada.operations.ExplainNonComplianceOperation;

public class pruebasExplainNonCompliance {

	boolean todas = true;
	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	File carpeta = new File("pruebasExplainNonCompliance");
	File[] lista = carpeta.listFiles();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
		try {
			File fichero = new File("CSV/explainNonCompliance-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Contiene Errores");
			writercsv.write("¿Excepcion?");
			writercsv.write("Elemento(OFFER) -> Elemento(TEMPLATE)");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		/*---------------------------------------------------------------------------------*/
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].isDirectory()) {
				if (!(lista[i].getName().startsWith("."))) {
					String folderName = lista[i].getName();
					File subcarpeta = new File(carpeta.getName()
							+ File.separator + lista[i].getName());
					System.out.println(lista[i].getName());
					String ruta = subcarpeta.getPath();
					AbstractDocument doc = ada.loadDocument(ruta
							+ File.separator + "TEMPLATE.wsag");
					AbstractDocument doc1 = ada.loadDocument(ruta
							+ File.separator + "OFFER.wsag");
					ChocoExplainNoComplianceOp op = (ChocoExplainNoComplianceOp) ada
							.createOperation("explainNonCompliance");
					op.addDocument(doc);
					op.addDocument(doc1);
					boolean aux;
					boolean excepcion = false;
					int tam = 0;
					Set<AgreementElement> claves;
					AgreementElement elem;
					AgreementElement elem2;
					Iterator<AgreementElement> it;
					Iterator<AgreementElement> it2;
					Collection<AgreementElement> col;
					Map<AgreementElement, Collection<AgreementElement>> map;
					try {
						ada.analyze(op);
						map = op.explainErrors();
						tam = map.size();
						if (tam == 0) {
							aux = false;
						} else {
							aux = true;
						}

						claves = map.keySet();
						it = claves.iterator();
						writercsv.write(lista[i].getName());
						writercsv.write("ExplainNonCompliance");
						writercsv.write("" + aux);
						writercsv.write("" + excepcion);
						while (it.hasNext()) {
							elem = it.next();// clave
							col = map.get(elem);
							String s = elem.getName() + "->";
							it2 = col.iterator();
							while (it2.hasNext()) {
								elem2 = it2.next();
								s = s + elem2.getName() + " ";
							}
							writercsv.write(s);
						}
						writercsv.endRecord();
						writercsv.endRecord();

					} catch (Exception e) {
						aux = false;
						excepcion = true;
						e.printStackTrace();
					}

					todas = todas && aux;
					System.out.println("Does the document contains errors?: "
							+ aux);
				}
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (todas == true);
	}
}
