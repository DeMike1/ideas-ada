package pruebas;

import java.io.File;
import java.io.FileWriter;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.AlternativeDocumentsOperation;
import es.us.isa.ada.operations.ComplianceOperation;
import es.us.isa.ada.operations.ConsistencyOperation;

public class pruebasAlternativeDocumentsTrue {

	boolean alternative = true;
	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	File carpeta = new File("pruebasAlternativeDocuments");
	String[] lista = carpeta.list();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		/* BLOQUE TRY-CATCH PARA ESCRIBIR LA CABECERA DEL ARCHIVO .CSV */
		try {
			File fichero = new File("CSV/alternativeDocuments-TRUE-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Resultado Esperado");
			writercsv.write("Resultado Obtenido");
			writercsv.write("Numero de AlternativeDocuments");
			writercsv.write("¿Excepcion?");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		/*---------------------------------------------------------------------------------*/
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
					AbstractDocument doc = ada.loadDocument(carpeta.getName() + "/"+ lista[i]);
					AlternativeDocumentsOperation op = (AlternativeDocumentsOperation) ada.createOperation("alternativeDocuments");
					op.addDocument(doc);
					boolean aux=true;
					int num=0;
					boolean excepcion = false;
					try {
						ada.analyze(op);
						num = op.getNumberOfDocuments();
					} catch (Exception e) {
						aux = false;
						excepcion = true;
						e.printStackTrace();
					}
					
					alternative = alternative && aux;
					System.out.println("How many altenative documenst?: " + num);
						try {
						writercsv.write(lista[i]);
						writercsv.write("Alternative Documents");
						writercsv.write("true");
						writercsv.write("" + aux);
						writercsv.write("" + num);
						writercsv.write("" + excepcion);
						writercsv.endRecord();
						writercsv.endRecord();

					} catch (Exception e) {
						throw e;
					}			
					
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (alternative == true);
	}
}
