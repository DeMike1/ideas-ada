package pruebas;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

import utils.ADATestingFacade;

import com.csvreader.CsvWriter;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.operations.ExplainLudicrousTerms;
import es.us.isa.ada.operations.LudicrousTermsOperation;
import es.us.isa.ada.wsag10.AgreementDocument;
import es.us.isa.ada.wsag10.Term;

public class pruebasLudicrousTrue {


	boolean resultado = true;
	private ADATestingFacade ada;
	CsvWriter writercsv = null;
	File carpeta = new File("ludicrousTerms(true)");
	String[] lista = carpeta.list();

	@Before
	public void setUp() {
		ada = new ADATestingFacade();
	}

	@Test
	public void test1() throws Exception {
		try {
			File fichero = new File("CSV/ludicrous-JUNIT.csv");
			FileWriter fwriter = new FileWriter(fichero);
			writercsv = new CsvWriter(fwriter, ';');
			writercsv.write("Nombre del Archivo");
			writercsv.write("Tipo de Operación");
			writercsv.write("Resultado Esperado");
			writercsv.write("Resultado Obtenido");
			writercsv.write("¿Excepcion?");
			writercsv.write("Terms");
			writercsv.write("Explaining");
			writercsv.endRecord();
		} catch (Exception e) {
			throw e;
		}
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].endsWith(".wsag")) {
				String s = carpeta.getName() + "/" + lista[i];
				AbstractDocument doc = ada.loadDocument(s);
				LudicrousTermsOperation op = (LudicrousTermsOperation) ada.createOperation("ludicrous");
				op.addDocument(doc);
				boolean excepcion = false;
				boolean warning = false;
				Collection<Term> ludicrousTerms = new LinkedList<Term>();
				Map<Term,Collection<AgreementElement>> res = new HashMap<Term, Collection<AgreementElement>>();
				try {
					ada.analyze(op);
					warning = op.hasLudicrousTerms();
					ludicrousTerms = op.getLudicrousTerms();
					ExplainLudicrousTerms op2 = 
						(ExplainLudicrousTerms) ada.createOperation("explainLudicrous");
					op2.addDocument(doc);
					op2.setLudicrousTerms(ludicrousTerms);
					ada.analyze(op2);
					res = op2.explainLudicrousTerms();
				} catch (Exception e) {
					e.printStackTrace();
					excepcion = true;
				}

				resultado = resultado && warning;
				System.out.println(s);
				System.out.println("Has the document ludicrous terms?: " + warning);
				/* BLOQUE TRY-CATCH PARA ESCRIBIR EL CONTENIDO DEL ARCHIVO .CSV */
				try {
					writercsv.write(lista[i]);
					writercsv.write("Ludicrous Terms");
					writercsv.write("true");
					writercsv.write("" + warning);
					writercsv.write("" + excepcion);
					String s1 = "";
					for (Term t: ludicrousTerms){
						s1 += t.getName()+",";
					}
					writercsv.write(s1);
					String s2 = "";
					Set<Entry<Term,Collection<AgreementElement>>> entries = res.entrySet();
					for (Entry<Term,Collection<AgreementElement>> e: entries){
						s2 += e.getKey().getName() + " => ";
						Collection<AgreementElement> elems = e.getValue();
						for (AgreementElement ae: elems){
							s2 += ae.getName()+",";
						}
						s2+=";";
					}
					writercsv.write(s2);
					writercsv.endRecord();
					writercsv.endRecord();

				} catch (Exception e) {
					throw e;
				}
			}
		}
		/*---------------------------------------------------------------------------------*/
		if (writercsv != null) {
			writercsv.close();
		}
		assert (resultado == true);
	}
	
	
}
