/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.manager.osgi;

import es.us.isa.ada.repository.ADAAgreementRepository;
import es.us.isa.ada.repository.AgreementRepository;
import es.us.isa.ada.users.ADASessionManager;
import es.us.isa.ada.users.SessionManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;

public class Activator implements BundleActivator {

//	private Collection<ServiceReference> references;
	private Hashtable<String,String> t;
	private Collection<ServiceRegistration> registry;
	
	public Activator(){
//		references = new LinkedList<ServiceReference>();
		t = new Hashtable<String, String>();
		registry = new LinkedList<ServiceRegistration>();
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Hola desde el activator de ADAma");
		t = new Hashtable<String,String>();
		t.put("id", "ADASessionManager");
		t.put("type", "ADA");
		SessionManager sm = new ADASessionManager();
		ServiceRegistration sr = context.registerService(SessionManager.class.getCanonicalName(), sm, t);
		registry.add(sr);
		
		t = new Hashtable<String,String>();
		t.put("id", "ADARepository");
		t.put("type", "ADA");
		AgreementRepository ar = new ADAAgreementRepository();
		sr = context.registerService(AgreementRepository.class.getCanonicalName(), ar, t);
		registry.add(sr);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		for (ServiceRegistration sr:registry){
			sr.unregister();
		}

	}

}
