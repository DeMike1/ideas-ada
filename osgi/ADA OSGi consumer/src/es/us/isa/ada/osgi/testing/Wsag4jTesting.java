package es.us.isa.ada.osgi.testing;

import java.io.File;

import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.wsag4j.ADA4Wsag4J;

public class Wsag4jTesting {
	
	public void test1(ADA4Wsag4J ada){
		AbstractDocument doc = ada.loadDocument(
				"Documentos WSAG4J\\test-template.xml",
				"Documentos WSAG4J\\states\\simple1\\state0.xml");

		ConsistencyOperation op = (ConsistencyOperation) ada
				.createOperation("Consistency");
		op.addDocument(doc);
		ada.analyze(op);
		boolean consistent = op.isConsistent();
		System.out.println("Test 1: is the document consistent? "+consistent);
//		assert consistent;
	}

	
}
