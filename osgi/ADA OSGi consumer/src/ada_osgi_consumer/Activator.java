package ada_osgi_consumer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import es.us.isa.ada.ADA;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;
import es.us.isa.ada.osgi.testing.Wsag4jTesting;
import es.us.isa.ada.wsag4j.ADA4Wsag4J;

public class Activator implements BundleActivator {

	private ServiceReference adaReference;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext context) throws Exception {
		testwsag4j(context);
	}

	private void testwsag4j(BundleContext context) {
		adaReference = context.getServiceReference(ADA4Wsag4J.class
				.getCanonicalName());
		if (adaReference != null) {
			ADA4Wsag4J ada = (ADA4Wsag4J) context.getService(adaReference);
			Wsag4jTesting testing = new Wsag4jTesting();
			testing.test1(ada);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		if (adaReference != null){
			context.ungetService(adaReference);
		}
		System.out.println("Goodbye ADA!!");
	}

}
