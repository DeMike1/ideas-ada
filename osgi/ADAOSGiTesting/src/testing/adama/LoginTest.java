package testing.adama;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import es.us.isa.ada.service.ADAManagerService;

@RunWith(JUnit4TestRunner.class)
public class LoginTest {

	@Inject
	private BundleContext bundleContext;
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.0"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAma").version("0.1.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.0"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Test
	public void test(){
		ServiceReference sr = bundleContext.getServiceReference(ADAManagerService.class.getCanonicalName());
		ADAManagerService adama = (ADAManagerService) bundleContext.getService(sr);
		
		String session = null;
		session = adama.login("Antonio", "ajurado");
		Boolean loggedout = adama.logout(session);
		System.out.println("Session: "+session);
		assertNotNull(session);
		assertEquals(Boolean.TRUE, loggedout);
	}
}
