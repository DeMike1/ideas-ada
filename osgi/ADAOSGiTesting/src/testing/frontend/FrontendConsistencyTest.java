package testing.frontend;

import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import testing.utils.Utils;
import es.us.isa.ada.document.AgreementElement;
import es.us.isa.ada.service.ADAServiceV2;
import es.us.isa.ada.wsag10.Term;

@RunWith(JUnit4TestRunner.class)
public class FrontendConsistencyTest {

	@Inject
	private BundleContext bundleContext;
	
	/**
	 * Ruta absoluta donde est� este proyecto para tirar de los documentos a analizar
	 */
	private String urlBase;
	
	/**
	 * Documentos que usaremos para los tests
	 */
	private Collection<Object[]> docs;
	
	@Before
	public void init(){
		urlBase = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
		//Load documents
		docs = new LinkedList<Object[]>();
		File fPath = new File(urlBase);
		File[] subfiles = fPath.listFiles();
		for(File f:subfiles){
			String path = f.getPath();
			if(!path.endsWith(".svn") && path.endsWith(".wsag")){
				Object[] aux = {f.getName(), path};
				docs.add(aux);
			}
		}
	}
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.1"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Test
	public void consistencyTest(){
		ServiceReference sr = bundleContext.getServiceReference(ADAServiceV2.class.getCanonicalName());
		ADAServiceV2 ada = (ADAServiceV2) bundleContext.getService(sr);
		//For every document
		for(Object[] doc:docs){
			String docName = (String)doc[0];
			String docPath = (String)doc[1];

			System.out.println("\nAnalizamos " + docName);
			//Get document content as string
			String docContent = Utils.readFileAsString(docPath);
			//Analyse
			Boolean isConsistent = ada.checkDocumentConsistency(docContent.getBytes());
			System.out.println("�Es consistente? " + isConsistent);
			if (!isConsistent) {
				Map<AgreementElement, Collection<AgreementElement>> explanations = ada.explainInconsistencies(docContent.getBytes());
				System.out.println("Explanations: " + explanations);
			} else {
				Collection<Term> deadTerms = ada.getDeadTerms(docContent.getBytes());
				Collection<Term> ludicrousTerms = ada.getLudicrousTerms(docContent.getBytes());
				if (deadTerms.size() > 0) {
					System.out.println("Tiene dead terms: " + deadTerms);
				}
				if (ludicrousTerms.size() > 0) {
					System.out.println("Tiene ludicrous terms: " + ludicrousTerms);
				}
			}
			System.out.println("\n=================================");
		}
	}
}