package testing.wsag4j.compliance;

import static org.junit.Assert.assertEquals;
import static org.ops4j.pax.exam.CoreOptions.felix;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.waitForFrameworkStartup;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.profile;
import static org.ops4j.pax.exam.container.def.PaxRunnerOptions.vmOption;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import es.us.isa.ada.ADA;
import es.us.isa.ada.document.AbstractDocument;
import es.us.isa.ada.operations.ConsistencyOperation;

@RunWith(JUnit4TestRunner.class)
public class ConsistencyWSAg4JTest {

	@Inject
	private BundleContext bundleContext;
	
	/**
	 * Ruta absoluta donde est� este proyecto para tirar de los documentos a analizar
	 */
	private String urlBase;
	
	private Collection<Object[]> docs;
	
	@Before
	public void init(){
		urlBase = "D:\\My Dropbox\\Escuela\\ADA\\workspace\\ADAOSGiTesting\\resources\\";
		//Load documents
		docs = new LinkedList<Object[]>();
		File[] docsPath = {new File(urlBase+"wsag4j\\templates"), new File(urlBase+"wsag4j\\offers")};
		for(File auxFile:docsPath){
			File[] subfiles = auxFile.listFiles();
			for(File f:subfiles){
				String path = f.getPath();
				if(!path.endsWith(".svn")){
					Object[] aux = {f.getName(), path};
					docs.add(aux);
				}
			}
		}
	}
	
	@Configuration
	public static Option[] configuration(){
		return options(
			felix(),
//			equinox(),
			/*
			 * Borra la cach� de %USER_HOME%/Configuraci�n Local/Temp/paxexam-runner-"username"
			 * Es necesario borrar la cach� cada vez que se hagan cambios en algunos de los bundles que se cargan, para que vuelva a cogerlos del repositorio de maven
			 * Si se borra la cach� con este m�todo, habr� que volver a copiar metrics/metricXXXX.xml en paxexam-runner-"username"
			 * Tambi�n se puede borrar la cach� mediante el explorador del SO. Hay que borrar todo el contenido excepto la carpeta metrics que hab�amos copiado anteriormente
			 */
//			cleanCaches(),
			profile("spring.dm"),
			mavenBundle().groupId("es.us.isa").artifactId("ADACore").version("0.4.1"),
			mavenBundle().groupId("es.us.isa").artifactId("ADAChocoReasoner").version("0.4.1"),
			// this just adds all what you write here to java vm argumenents of the (new) osgi process.
            vmOption( "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006" ),
//            // this is necessary to let junit runner not timout the remote process before attaching debugger
//            // setting timeout to 0 means wait as long as the remote service comes available.
//            // starting with version 0.5.0 of PAx Exam this is no longer required as by default the framework tests
//            // will not be triggered till the framework is not started
            waitForFrameworkStartup()
		);
	}
	
	@Test
	public void test(){
		ServiceReference sr = bundleContext.getServiceReference(ADA.class.getCanonicalName());
		ADA ada = (ADA) bundleContext.getService(sr);
		
		//For every document
		for(Object[] obj:docs){
			System.out.println("\nVamos a analizar el documento "+(String)obj[0]);
			//Create operation
			ConsistencyOperation op = (ConsistencyOperation)ada.createOperation(ADA.CONSISTENCY);
			//Load document
			AbstractDocument doc = ada.loadDocument((String)obj[1]);
			op.addDocument(doc);
			//Analyse
			ada.analyze(op);
			//Get result
			System.out.println("Is consistent: "+op.isConsistent());
			System.out.println("-----------------------------------");
			assertEquals(op.isConsistent(), Boolean.TRUE);
		}
	}
}
