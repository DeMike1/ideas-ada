/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package testsChoco;

import org.junit.Before;
import org.junit.Test;

import choco.Choco;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.ContradictionException;
import choco.kernel.solver.Solver;


public class ChocoDomainsTests {

	private IntegerVariable var;
	
	private Model m;
	
	@Before
	public void setUp(){
		m = new CPModel();
	}
	
	@Test
	public void test1(){
		var = Choco.makeIntVar("var1",0,20);
		m.addVariable(var);
		Constraint cons1 = Choco.gt(var, 10);
		Constraint cons2 = Choco.lt(var, 20);
		m.addConstraint(cons1);
		m.addConstraint(cons2);
		Solver s = new CPSolver();
		s.read(m);
		try {
			s.propagate();
		} catch (ContradictionException e) {
			e.printStackTrace();
		}
		s.solve();
		System.out.println(s.getNbSolutions());
	}
	
}
