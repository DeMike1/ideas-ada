/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */
package testing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Collection;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Document;

import es.us.isa.ada.wsag10.transforms.wsag2wsag4people.XmlToWSAg4People;


@RunWith(Parameterized.class)
public class CommentsTranslationTestSuite {

	private String file;
	
	public CommentsTranslationTestSuite(String s){
		file = s;
	}
	
	@Test
	public void test(){
		XmlToWSAg4People translator = new XmlToWSAg4People();
		BufferedWriter out = null;
//		DocumentBuilder builder;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document agreement = builder.parse(file);
			String result = translator.convertXMLToWSAg4People(agreement);
			System.out.println(result);
			out = new BufferedWriter(new FileWriter(file+".wsag4people"));
			out.write(result);
			out.close();
		} catch (Exception e) {
			System.err.println("Error while opening the agreement file");
		}
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Collection<Object[]> res = new LinkedList<Object[]>();
		
		res.add(new Object[]{"test-cases\\templates\\complete-template-1.wsag"});
		res.add(new Object[]{"test-cases\\offers\\BPMS_compliant_offer_1.wsag"});
		
		//test-cases\offers\BPMS_compliant_offer_1.wsag
		return res;
	}
	
}
