/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.us.isa.ada.loaders;

import es.us.isa.ada.io.IDocumentParser;
import es.us.isa.ada.io.IDocumentWriter;
import es.us.isa.ada.repository.AgreementRepository;
import es.us.isa.ada.selectors.DefaultSelectorCriteria;
import es.us.isa.ada.selectors.SelectorCriteria;
import es.us.isa.ada.subfacades.*;
import es.us.isa.ada.users.SessionManager;
import es.us.isa.ada.wsag10.parsers.DefaultWSAgParser;
import es.us.isa.ada.wsag10.transforms.ITransform;
import es.us.isa.ada.wsag10.transforms.WSAg4PeopleTransform;
import es.us.isa.ada.wsag10.transforms.WSAgPeople2XMLTransform;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * User: resinas
 * Date: 10/06/13
 * Time: 17:13
 */
public class BasicExtensionsLoader implements ExtensionsLoader {

    // These properties represent the return values
    private AnalysisManager am;
    private DocumentsManager dm;
    private TransformManager tm;
    private ADAManager adama;

    // These properties are the configuration
    private SelectorCriteria selectorCriteria = new DefaultSelectorCriteria();
    private Collection<ProxyAnalyzer> proxyAnalyzers;

    private Map<String, IDocumentParser> parsers = loadDefaultParsers();
    private Map<String, IDocumentParser> loadDefaultParsers() {
        Map<String, IDocumentParser> parsers = new HashMap<String, IDocumentParser>();
        parsers.put("wsag", new DefaultWSAgParser());

        return parsers;
    }

    private Map<String, IDocumentWriter> writers = new HashMap<String, IDocumentWriter>();
    private Collection<ITransform> transforms = loadDefaultTransforms();
    private Collection<ITransform> loadDefaultTransforms() {
        Collection<ITransform> transforms = new ArrayList<ITransform>();
        transforms.add(new WSAg4PeopleTransform());
        transforms.add(new WSAgPeople2XMLTransform());

        return transforms;
    }

    private SessionManager sessionManager;
    private AgreementRepository agreementRepository;

    public SelectorCriteria getSelectorCriteria() {
        return selectorCriteria;
    }

    public void setSelectorCriteria(SelectorCriteria selectorCriteria) {
        this.selectorCriteria = selectorCriteria;
    }

    public Collection<ProxyAnalyzer> getProxyAnalyzers() {
        return proxyAnalyzers;
    }

    public void setProxyAnalyzers(Collection<ProxyAnalyzer> proxyAnalyzers) {
        this.proxyAnalyzers = proxyAnalyzers;
    }

    public Map<String, IDocumentParser> getParsers() {
        return parsers;
    }

    public void setParsers(Map<String, IDocumentParser> parsers) {
        this.parsers = parsers;
    }

    public Map<String, IDocumentWriter> getWriters() {
        return writers;
    }

    public void setWriters(Map<String, IDocumentWriter> writers) {
        this.writers = writers;
    }

    public Collection<ITransform> getTransforms() {
        return transforms;
    }

    public void setTransforms(Collection<ITransform> transforms) {
        this.transforms = transforms;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
        adama.setSessionManager(sessionManager);
    }

    public AgreementRepository getAgreementRepository() {
        return agreementRepository;
    }

    public void setAgreementRepository(AgreementRepository agreementRepository) {
        this.agreementRepository = agreementRepository;
        adama.setAgreementRepository(agreementRepository);
    }

    @Override
    public AnalysisManager getAnalysisManager() {
        if (am == null)
            createAnalysisManager();
        return am;
    }

    private void createAnalysisManager() {
        if (proxyAnalyzers == null || selectorCriteria == null)
            throw new RuntimeException("Missing Proxy Analyzers or SelectorCriteria");

        am = new AnalysisManager(proxyAnalyzers,selectorCriteria);
    }

    @Override
    public DocumentsManager getDocumentsManager() {
        if (dm == null)
            createDocumentsManager();
        return dm;
    }

    private void createDocumentsManager() {
        dm = new DocumentsManager();
        for(String parser: parsers.keySet()) {
            dm.addParser(parser, parsers.get(parser));
        }
        for(String writer: writers.keySet()) {
            dm.addWriter(writer, writers.get(writer));
        }
    }

    @Override
    public TransformManager getTransformManager() {
        if (tm == null)
            createTransformManager();
        return tm;
    }

    private void createTransformManager() {
        tm = new TransformManager();
        for (ITransform tr: transforms) {
            tm.addTransform(tr);
        }
    }

    @Override
    public ADAManager getADAManager() {
        if (adama == null)
            createADAManager();
        return adama;
    }

    private void createADAManager() {
        adama = new ADAManager(sessionManager, agreementRepository);
    }
}
