/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.operations;

import java.util.Collection;

import es.us.isa.ada.Operation;
import es.us.isa.ada.wsag10.Term;

/**
 * Determines the set of dead terms of an agreement document
 *
 */
public interface DeadTermsOperation extends Operation{

	/**
	 * Returns true if the document has dead terms.
	 * 
	 * A term is considered as dead when its qualifying condition
	 * is never hold since it collides with other elements
	 * of the document
     * 
	 * @return boolean 
	 */
	public boolean hasDeadTerms();
	
	/**
	 * Returns a collection with the agreement
	 * document's dead terms.
	 * 
	 * A term is considered as dead when its qualifyng condition
	 * is never hold since it collides with other elements
	 * of the document
     * 
	 * @return Collection<Term>
	 */
	public Collection<Term> getDeadTerms();
	
}
