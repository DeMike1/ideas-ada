/**
 * 	This file is part of ADA.
 *
 *     ADA is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ADA is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with ADA.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.us.isa.ada.users;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import es.us.isa.ada.util.bd.ADADBConnection;
import es.us.isa.ada.util.bd.DBConnection;

@RunWith(Parameterized.class)
public class TestDeleteUser {

	private UsersManager um;
	private SessionManager sm;
	
	private int userId;
	private String username;
	private String pass;
	private boolean expectedResult;
	
	public TestDeleteUser(int userId, String username, String pass, boolean expectedResult){
		this.userId = userId;
		this.username = username;
		this.pass = pass;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp(){
		DBConnection db = new ADADBConnection("resources/localDB.properties");
		um = new ADAUsersManager(db);
		sm = new ADASessionManager(db);
	}
	
	@Parameters
	public static Collection<Object[]> getParameters(){
		Object[] validUser = new Object[] {4, "juan", "juanPass", true};
		Object[] invalidSession = new Object[] {4, "juanXXX", "juanPass", false};
		Object[] notExistsUserId = new Object[] {2, "pepe", "pepePass", false};
		Object[] invalidUserId = new Object[] {0, "loQuesea", "loquesea", false};
		
		Collection<Object[]> param = new LinkedList<Object[]>();
		param.add(validUser);
		param.add(invalidSession);
		param.add(notExistsUserId);
		param.add(invalidUserId);
		return param;
	}
	
	@Test
	public void deleteUser(){
		//primero hay que abrir la sesion del usuario que queremos borrar
		String session = sm.login(username, pass);
		boolean result = um.deleteUser(userId, session);
		assertEquals(expectedResult, result);
	}
}